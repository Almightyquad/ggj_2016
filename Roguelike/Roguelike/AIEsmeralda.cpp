#include "AIEsmeralda.h"
#include "ActorFactory.h"
#include "Message.h"
#include "Engine.h"
#include "Actor.h"

AIEsmeralda::~AIEsmeralda()
{
	parentEngine->increaseEnemiesKilled();
}

void AIEsmeralda::update(float deltaTime)
{
	if (player == nullptr)
	{
		player = parentEngine->getPlayer();
	}
	if (scriptPath != "")
	{
		glm::vec3 parentActorPosition = dynamic_cast<PhysicsComponent*>(parentActor->getComponent("PhysicsComponent"))->getPosition();
		lua_getglobal(luaState, "normalizeVector");

		lua_pushnumber(luaState, dynamic_cast<PhysicsComponent*>(player->getComponent("PhysicsComponent"))->getPosition().x);
		lua_pushnumber(luaState, dynamic_cast<PhysicsComponent*>(player->getComponent("PhysicsComponent"))->getPosition().y);
		lua_pushnumber(luaState, parentActorPosition.x);
		lua_pushnumber(luaState, parentActorPosition.y);
		lua_pushnumber(luaState, deltaTime);
		if (lua_pcall(luaState, 5, 2, 0) != 0)
		{
			if (luaDebug)
			{
				printf("error calling luafunction normalizeVector:%s\n", lua_tostring(luaState, -1));
			}
			else
			{
				lua_tostring(luaState, -1);
			}
		}

		std::string newActorPath = lua_tostring(luaState, -2);
		float xComponent = static_cast<float>(lua_tonumber(luaState, -1));
 		float yComponent = 0;

		this->postMessage(Message(dynamic_cast<IAIComponent*>(this), "velocityUpdate", xComponent*deltaTime, 0*deltaTime));
		
		if (newActorPath != "")
		{
			Actor* newActor = parentEngine->getActorFactory()->createActor(newActorPath, glm::vec3{ parentActorPosition.x, parentActorPosition.y + 0.5f, 0.f });
		}
		lua_pop(luaState, lua_gettop(luaState));
	}
}
