#pragma once
#include "IPickUpComponent.h"

class PickUpTuna final : public IPickUpComponent
{
public:
	PickUpTuna(tinyxml2::XMLElement *xmlElement) : IPickUpComponent(xmlElement) {};
	PickUpTuna() : IPickUpComponent(nullptr) {};	
	virtual void onToss() override;
	virtual void onActivation() override;
	virtual void onDeactivation() override;
private:
};