#include "FriendlyProjectilePhysics.h"
#include "Actor.h"
#include "Engine.h"

FriendlyProjectilePhysics::FriendlyProjectilePhysics(tinyxml2::XMLElement * xmlElement)
	:PhysicsComponent(xmlElement),
	combatComponent(nullptr),
	init(false)
{

}

void FriendlyProjectilePhysics::onCollision(int fixtureUserData, Actor * collidedActor)
{
	if (combatComponent == nullptr)
	{
		combatComponent = dynamic_cast<CombatComponent*>(parentActor->getComponent("CombatComponent"));
	}
	glm::vec3 pos = parentActor->getPosition();

	switch (fixtureUserData)
	{
	case CATEGORY_ENEMY:
		if (parentActor->getActorId().actorName == "PlayerShield")
		{
			if (this->player != nullptr)
			{
				if (player->getPosition().x < parentActor->getPosition().x)
				{
					dynamic_cast<PhysicsComponent*>(collidedActor->getComponent("PhysicsComponent"))->setVelocity({ 10.f * 2, 10.f, 0.f });
					//dynamic_cast<PhysicsComponent*>(collidedActor->getComponent("PhysicsComponent"))->applyLinearImpulse({ 200.f, 20.f});
				}
				else
				{
					dynamic_cast<PhysicsComponent*>(collidedActor->getComponent("PhysicsComponent"))->setVelocity({ -10.f * 2, 10.f, 0.f });
					//dynamic_cast<PhysicsComponent*>(collidedActor->getComponent("PhysicsComponent"))->applyLinearImpulse({ -200.f, 20.f });
				}
			}
		}
		dynamic_cast<CombatComponent*>(collidedActor->getComponent("CombatComponent"))->reduceHealth(combatComponent->getDamage());
		break;
	default:
		break;
	}
}

void FriendlyProjectilePhysics::update(float deltaTime)
{
	PhysicsComponent::update(deltaTime);

	if (parentActor->getActorId().actorName == "Halo")
	{
		float newAngle = getRotation() + (glm::two_pi<float>() * deltaTime);
		setRotation(newAngle);
		body->SetLinearVelocity({ body->GetLinearVelocity().x + (body->GetLinearVelocity().x *deltaTime), 0 });
	}


	if (!init)
	{
		if (parentActor->getActorId().actorName == "Spikes")
		{
			this->body->ApplyLinearImpulse({ 0.f, 50.f }, body->GetWorldCenter(), true);
		}
		init = true;
	}

	if (position.y > -7.0f && body->GetLinearVelocity().y > 0.f)
	{
		body->SetLinearVelocity({ 0.f, 0.f });
	}

	float offset = 1.f;
	if (parentActor->getActorId().actorName == "PlayerShield")
	{
		if (this->player == nullptr)
		{
			this->player = parentEngine->getPlayer();
		}
		if (player->getPosition().x < parentActor->getPosition().x)
		{
			parentActor->setPosition({ player->getPosition().x + offset, player->getPosition().y, player->getPosition().z });
		}
		else
		{
			parentActor->setPosition({ player->getPosition().x - offset, player->getPosition().y, player->getPosition().z });
		}
	}
}

void FriendlyProjectilePhysics::onSpawn()
{
	if (parentActor->getActorId().actorName == "flameBlast")
	{
		parentEngine->getParticleSystem()->createRegion(1000.f, parentEngine->getDeltaTime(), glm::vec3({ parentActor->getPosition().x,parentActor->getPosition().y,0 }), glm::vec3({0.8f,2.5f,0.f}),3, glm::vec4(1.f, 1.f, 0.f, 1.0f), glm::vec3(0.6f, 0.8f, 0.8f));
	}
}
