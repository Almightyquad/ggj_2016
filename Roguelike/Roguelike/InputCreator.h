#pragma once
#include "IInputComponent.h"
#include <memory>

class InputFactory;

class InputCreator
{
public:
	InputCreator(const std::string& classname, InputFactory* inputFactory);
	virtual std::unique_ptr<IInputComponent> create(tinyxml2::XMLElement* xmlElement) = 0;
};

template <class T>
class InputCreatorImplementation : public InputCreator
{
public:
	InputCreatorImplementation(const std::string& classname, InputFactory* inputFactory) : InputCreator(classname, inputFactory) {};
	virtual std::unique_ptr<IInputComponent> create(tinyxml2::XMLElement* xmlElement) override
	{
		return std::make_unique<T>(xmlElement);
	}
};