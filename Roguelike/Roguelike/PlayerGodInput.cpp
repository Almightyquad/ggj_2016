#include "PlayerGodInput.h"
#include "Engine.h"
#include "Message.h"

PlayerGodInput::PlayerGodInput(tinyxml2::XMLElement * xmlElement)
	: IInputComponent(xmlElement),
	subscribedToEvents(false),
	movement(0, 0),
	speed(8.f),
	goodPlayerPlaying(true),
	maxJoystickValue(pow(2, 15)),
	abilityOneTimer(1),
	abilityTwoTimer(5),
	freezeInterval(0.2)
{
	freezeCount = 3;

		ControllerInput input;
		for (int i = 0; i < 2; i++)
		{
			std::cout << "Added controller " << i << ".\n";
			this->inputs.emplace_back(input);
		}
}

PlayerGodInput::~PlayerGodInput()
{
	parentEngine->getEventHandler()->removeSubscriber(this);
}

void PlayerGodInput::update(float deltatime)
{
	if (!subscribedToEvents)
	{

		parentEngine->getGuiHandler()->setBarMaxValue("godAbillity1", 1.f);
		parentEngine->getGuiHandler()->setBarMaxValue("godAbillity2", 5.f);

		parentEngine->getEventHandler()->addSubscriber(this);
		subscribedToEvents = true;
		parentActorFactory = parentEngine->getActorFactory();
	}

	glm::vec3 groundActorPos = groundActor->getPosition();
	glm::vec3 parentActorPos = parentActor->getPosition();
	if (glm::distance<float>(parentActorPos.x, groundActorPos.x) > 9)
	{
		if (parentActorPos.x > groundActorPos.x)
		{
			parentActor->setPosition({ groundActorPos.x + 9,parentActorPos.y, parentActorPos.z });
		}
		if (parentActorPos.x < groundActorPos.x)
		{
			parentActor->setPosition({ groundActorPos.x - 9,parentActorPos.y, parentActorPos.z });
		}

	}

	float tempTimer = 1 - abilityOneTimer.getTicks();

	parentEngine->getGuiHandler()->setBarValue("godAbillity1", tempTimer);

	tempTimer = 5 - abilityTwoTimer.getTicks();

	parentEngine->getGuiHandler()->setBarValue("godAbillity2", tempTimer);


	movement = { 0,0 };
	if (goodPlayerPlaying)
	{
		if (upDown)
		{
			movement.y += speed;
		}
		if (downDown)
		{
			movement.y -= speed;
		}
		if (leftDown)
		{
			movement.x -= speed;
		}
		if (rightDown)
		{
			movement.x += speed;
		}
		movement.x += inputs[1].axis.x / maxJoystickValue*speed;
		movement.y -= inputs[1].axis.y / maxJoystickValue*speed;
	}
	if (!goodPlayerPlaying)
	{
		if (wDown)
		{
			movement.y += speed;
		}
		if (sDown)
		{
			movement.y -= speed;
		}
		if (aDown)
		{
			movement.x -= speed;
		}
		if (dDown)
		{
			movement.x += speed;
		}
		movement.x += inputs[0].axis.x / maxJoystickValue*speed;
		movement.y -= inputs[0].axis.y / maxJoystickValue*speed;
	}
	checkAbilityCast();
	parentActor->movePosition({ movement.x, movement.y, 0.f }, deltatime);
	
	if (freezeCount < 3)
	{
		if (freezeInterval.hasEnded())
		{
			freezeInterval.restart();
			parentActorFactory->createActor("res/actors/freeze.xml", glm::vec3(parentActor->getPosition().x, 5.f, -1.5f), glm::vec3({ 0,-5,0 }));
			freezeCount++;
		}
	}
}

void PlayerGodInput::receiveMessage(const Message & message)
{
	if (message.getSenderType() == typeid(EventHandler))
	{
		if (static_cast<const char*>(message.getVariable(0)) == "keyDown")
		{
			int number = message.getVariable(1);
			switch (number)
			{
				//good player
			case SDLK_w:
				wDown = true;
				break;
			case SDLK_s:
				sDown = true;
				break;
			case SDLK_a:
				aDown = true;
				break;
			case SDLK_d:
				dDown = true;
				break;
			case SDLK_e:
				eDown = true;
				break;
			case SDLK_SPACE:
				spaceDown = true;
				break;
			case SDLK_1:
				oneDown = true;
				break;
			case SDLK_2:
				twoDown = true;
				break;
			case SDLK_3:
				threeDown = true;
				break;
			case SDLK_4:
				fourDown = true;
				break;
				//evil player
			case SDLK_UP:
				upDown = true;
				break;
			case SDLK_DOWN:
				downDown = true;
				break;
			case SDLK_LEFT:
				leftDown = true;
				break;
			case SDLK_RIGHT:
				rightDown = true;
				break;
			case SDLK_KP_0:
				numZeroDown = true;
				break;
			case SDLK_KP_1:
				numOneDown = true;
				break;
			case SDLK_KP_7:
				numSevenDown = true;
				break;
			case SDLK_KP_8:
				numEightDown = true;
				break;
			case SDLK_KP_9:
				numNineDown = true;
				break;
			case SDLK_KP_4:
				numFourDown = true;
				break;
			default:
				break;
			}
			return;
		}

		if (static_cast<const char*>(message.getVariable(0)) == "keyUp")
		{
			int number = message.getVariable(1);
			switch (number)
			{
			case SDLK_w:
				wDown = false;
				break;
			case SDLK_s:
				sDown = false;
				break;
			case SDLK_a:
				aDown = false;
				break;
			case SDLK_d:
				dDown = false;
				break;
			case SDLK_e:
				eDown = false;
				break;
			case SDLK_SPACE:
				spaceDown = false;
				break;
			case SDLK_1:
				oneDown = false;
				break;
			case SDLK_2:
				twoDown = false;
				break;
			case SDLK_3:
				threeDown = false;
				break;
			case SDLK_4:
				fourDown = false;
				break;

				//evil player
			case SDLK_UP:
				upDown = false;
				break;
			case SDLK_DOWN:
				downDown = false;
				break;
			case SDLK_LEFT:
				leftDown = false;
				break;
			case SDLK_RIGHT:
				rightDown = false;
				break;
			case SDLK_KP_0:
				numZeroDown = false;
				break;
			case SDLK_KP_1:
				numOneDown = false;
			case SDLK_KP_7:
				numSevenDown = false;
				break;
			case SDLK_KP_8:
				numEightDown = false;
				break;
			case SDLK_KP_9:
				numNineDown = false;
				break;
			case SDLK_KP_4:
				numFourDown = false;
				break;
			default:
				break;
			}
			return;
		}

		if (static_cast<const char*>(message.getVariable(0)) == "joystickNoMotion")
		{
			SDL_JoyAxisEvent axisEvent = message.getVariable(1);
			if (axisEvent.which == 0)
			{
				std::cout << "one axis null\n";
				inputs[0].axis = { 0.f, 0.f };
			}

			if (axisEvent.which == 1)
			{
				std::cout << "two axis null\n";
				inputs[1].axis = { 0.f, 0.f };
			}
		}

		if (static_cast<const char*>(message.getVariable(0)) == "joystickMotion")
		{
			SDL_JoyAxisEvent axisEvent = message.getVariable(1);
			if (axisEvent.which == 0)	//player one
			{
				switch (axisEvent.axis)
				{
				case 0:
					inputs[0].axis.x = axisEvent.value;
					break;
				case 1:
					inputs[0].axis.y = axisEvent.value;
					break;
				}
			}
			if (axisEvent.which == 1)	//player two
			{
				switch (axisEvent.axis)
				{
				case 0:
					inputs[1].axis.x = axisEvent.value;
					break;
				case 1:
					inputs[1].axis.y = axisEvent.value;
					break;
				}
			}
			this->stickID = axisEvent.which;
		}

		if (static_cast<const char*>(message.getVariable(0)) == "joystickButtonUp")
		{
			SDL_JoyButtonEvent buttonUp = message.getVariable(1);
			if (buttonUp.which == 0)
			{
				switch (buttonUp.button)
				{
				case 0: //dpad up

					break;
				case 1: //dpad left

					break;
				case 2: //dpad down

					break;
				case 3: //dpad right

					break;
				case 4: //start

					break;
				case 5: //back or select

					break;
				case 6: //left tumbstick pressed

					break;
				case 7: //right tumbstick pressed

					break;
				case 8: //left bumper or l1
					inputs[0].leftBumperDown = false;
					break;
				case 9: //right bumper or r1
					inputs[0].rightBumperDown = false;
					break;
				case 10: //a or x
					inputs[0].downButtonDown = false;
					break;
				case 11: //b or circle

					break;
				case 12: //x or square

					break;
				case 13: //y or triangle
					inputs[0].upButtonDown = false;
					break;
				}
			}
			if (buttonUp.which == 1)
			{
				switch (buttonUp.button)
				{
				case 0: //dpad up

					break;
				case 1: //dpad left

					break;
				case 2: //dpad down

					break;
				case 3: //dpad right

					break;
				case 4: //start

					break;
				case 5: //back or select

					break;
				case 6: //left tumbstick pressed

					break;
				case 7: //right tumbstick pressed

					break;
				case 8: //left bumper or l1
					inputs[1].leftBumperDown = false;
					break;
				case 9: //right bumper or r1
					inputs[1].rightBumperDown = false;
					break;
				case 10: //a or x
					inputs[1].downButtonDown = false;
					break;
				case 11: //b or circle

					break;
				case 12: //x or square

					break;
				case 13: //y or triangle
					inputs[1].upButtonDown = false;
					break;
				}
			}
		}

		if (static_cast<const char*>(message.getVariable(0)) == "joystickButtonDown")
		{
			SDL_JoyButtonEvent buttonDown = message.getVariable(1);
			if (buttonDown.which == 0)
			{
				switch (buttonDown.button)
				{
				case 0: //dpad up

					break;
				case 1: //dpad left

					break;
				case 2: //dpad down

					break;
				case 3: //dpad right

					break;
				case 4: //start
					parentEngine->setGameState(STATE_MAINMENU);
					break;
				case 5: //back or select

					break;
				case 6: //left tumbstick pressed
					
					break;
				case 7: //right tumbstick pressed

					break;
				case 8: //left bumper or l1
					inputs[0].leftBumperDown = true;
					break;
				case 9: //right bumper or r1
					inputs[0].rightBumperDown = true;
					break;
				case 10: //a or x
					inputs[0].downButtonDown = true;
					break;
				case 11: //b or circle

					break;
				case 12: //x or square

					break;
				case 13: //y or triangle
					inputs[0].upButtonDown = true;
					break;
				}
			}
			if (buttonDown.which == 1)
			{
				switch (buttonDown.button)
				{
				case 0: //dpad up

					break;
				case 1: //dpad left

					break;
				case 2: //dpad down

					break;
				case 3: //dpad right

					break;
				case 4: //start

					break;
				case 5: //back or select

					break;
				case 6: //left tumbstick pressed

					break;
				case 7: //right tumbstick pressed

					break;
				case 8: //left bumper or l1
					inputs[1].leftBumperDown = true;
					break;
				case 9: //right bumper or r1
					inputs[1].rightBumperDown = true;
					break;
				case 10: //a or x
					inputs[1].downButtonDown = true;
					break;
				case 11: //b or circle

					break;
				case 12: //x or square

					break;
				case 13: //y or triangle
					inputs[1].upButtonDown = true;
					break;
				}
			}
		}

		if (static_cast<const char*>(message.getVariable(0)) == "joystickHat")
		{
			SDL_JoyHatEvent hat = message.getVariable(1);
		}
	}
}

void PlayerGodInput::setGroundActor(Actor * actor)
{
	groundActor = actor;
}

void PlayerGodInput::swapCharacters()
{
	Texture* newTexture;
	if (goodPlayerPlaying)
	{
		newTexture = parentEngine->getTextureHandler()->loadTexture("res/textures/aimGood.png");
		dynamic_cast<GraphicsComponent*>(this->parentEngine->getPlayer()->getComponent("GraphicsComponent"))->setTexture(newTexture);
	}
	if (!goodPlayerPlaying)
	{
		newTexture = parentEngine->getTextureHandler()->loadTexture("res/textures/aimBad.png");
		dynamic_cast<GraphicsComponent*>(this->parentEngine->getPlayer()->getComponent("GraphicsComponent"))->setTexture(newTexture);
	}
	goodPlayerPlaying = !goodPlayerPlaying;
}

void PlayerGodInput::checkAbilityCast()
{
	if (!goodPlayerPlaying)
	{
		if (oneDown && abilityOneTimer.hasEnded() || inputs[0].leftBumperDown && abilityOneTimer.hasEnded())
		{
			castGoodAbilityOne();
			abilityOneTimer.restart();
		}
		if (twoDown && abilityTwoTimer.hasEnded() || inputs[0].rightBumperDown && abilityTwoTimer.hasEnded())
		{
			castGoodAbilityTwo();
			abilityTwoTimer.restart();
		}
	}

	if (goodPlayerPlaying)
	{
		if (numSevenDown && abilityOneTimer.hasEnded() || inputs[1].leftBumperDown && abilityOneTimer.hasEnded())
		{
			castEvilAbilityOne();
			abilityOneTimer.restart();
		}
		if (numEightDown && abilityTwoTimer.hasEnded() || inputs[1].rightBumperDown && abilityTwoTimer.hasEnded())
		{
			castEvilAbilityTwo();
			abilityTwoTimer.restart();
		}
	}
}

void PlayerGodInput::castGoodAbilityOne()
{
	parentActorFactory->createActor("res/actors/heal.xml", glm::vec3(parentActor->getPosition().x - 0.5, - 6.f, 0.f));
}

void PlayerGodInput::castGoodAbilityTwo()
{
	freezeInterval.start();
	freezeCount = 0;
}

void PlayerGodInput::castEvilAbilityOne()
{
	parentActorFactory->createActor("res/actors/meteor.xml", glm::vec3(parentActor->getPosition().x + 13.f, parentActor->getPosition().y + 10.f, 0.f), glm::vec3({ -10,0,0 }));
}

void PlayerGodInput::castEvilAbilityTwo()
{
	parentActorFactory->createActor("res/actors/spikes.xml", glm::vec3(parentActor->getPosition().x, -8.f, -.1f));
}