#pragma once
#include "IAIComponent.h"

class AIKappa final : public IAIComponent
{
public:
	AIKappa(tinyxml2::XMLElement *xmlElement);
	~AIKappa();
	//void init();
	void update(float deltaTime) override;
private:
	//void(ActorFactory::*createActor) (const std::string xmlPath, const glm::vec3 position, const glm::vec3 velocity);
};
