#pragma once
#include "ActorFactory.h"
#include "ActorComponent.h"
#include "GraphicsComponent.h"
#include "PhysicsComponent.h"
#include "AnimationComponent.h"
#include "IAIComponent.h"
#include "IPickUpComponent.h"
#include "CombatComponent.h"
#include "AudioComponent.h"
#include "InventoryComponent.h"
#include "ParticleComponent.h"
#include <unordered_map>
#include "Observable.h"

/**
 * @class	Actor
 *
 * @brief	The actor.
 * @details Each object in the game is an actor.
 * 			Actors have many different properties. Which properties they have is decided by the components defined in the xml file.
 * 			All components are independent.
 */
class Actor : public IObservable
{
public:
	Actor(tinyxml2::XMLElement *xmlElement);
	//copy constructor NEEDS TO BE UP TO DATE AT ALL TIMES! if you add a new member variable to actor.h, make sure it is copied. don't want to debug this stupid shit two months in the future
	Actor(const Actor& newActor);
	//copy assignment operator NEEDS TO BE UP TO DATE (after it is implemented)
	Actor& operator=(const Actor& newActor);
	~Actor();
	
	void init(Engine* parentEngine, glm::vec3 position, glm::vec2 tilePosition = glm::vec2{0, 0});
	void update(float deltaTime);
	void onDeath();
	void onHit();
	void onSpawn();

	void setParent(ActorFactory& parentPtr);
	ActorComponent* getComponent(std::string componentId);
		
	actorID getActorId();
	void setActorId(actorID actorId);
	std::string getCategory();
	void setCategory(std::string category);

	void setPosition(glm::vec3 newPosition);
	glm::vec3 getPosition();
	void movePosition(glm::vec3 direction, float deltatime);

	void addComponentSubscriber(const char* messenger, const char* subscriber);
	void removeComponentSubscriber(const char* messenger, const char* subscriber);
	void setActive(bool flag);
	void setPhysicsActive(bool flag);
	bool isActive();

private:
	//krem::Vector3f position;
	glm::vec3 position;
	ActorFactory* parentActorFactory;
	actorID actorId;
	std::string category;
	Actor* equippedWeapon;
	Engine* parentEngine;

	bool active;

	std::unordered_map<std::string, std::unique_ptr<ActorComponent>> actorComponentMap;
	void addChild(ActorComponent* actorComponent);
	tinyxml2::XMLElement *xmlElement;
};