#pragma once
#include "PhysicsComponent.h"

class Actor;
class CombatComponent;

class NeutralProjectilePhysics final : public PhysicsComponent
{
public:
	NeutralProjectilePhysics(tinyxml2::XMLElement *xmlElement);
	void onCollision(int fixtureUserData, Actor* collidedActor);
	void update(float deltaTime) override;
	void onDeath() override;
	void onSpawn() override;
private:
	CombatComponent* combatComponent;
};