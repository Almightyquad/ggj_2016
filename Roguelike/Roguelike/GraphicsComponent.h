#pragma once
#include "IGraphicsComponent.h"
#include "Mesh.h"
#include "Texture.h"
#include "Shader.h"
#include "Transform.h"
/**
 * @class	GraphicsComponent
 *
 * @brief	The graphics component for each actor.
 * @details	Holds pointers to all data needed to draw the actor.
 * 			All pointers are owned by their respective handlers
 */
class GraphicsComponent : public IGraphicsComponent
{
public:
	GraphicsComponent(tinyxml2::XMLElement *xmlElement = nullptr);
	//GraphicsComponent(const GraphicsComponent& newComponent);
	~GraphicsComponent() override;
	void init(Engine* parentEngine) override;
	void draw(const glm::mat4& viewProjection);
	void update(float deltaTime) override;
	void setPosition(glm::vec3 position);
	void setScale(glm::vec3 scale);
	void setTilePosition(glm::vec2 tilePosition);
	void receiveMessage(const Message &message);
	void setTexture(Texture * texture);
	void setRotation(float degrees);
	void setFrames(glm::vec2 xy);
	glm::vec3 getPosition();
	Texture* getTexture();
private:
	bool initB = false; //temp henning fix
	Transform transform;
	Mesh *mesh;
	Shader *shader;
	Texture *texture;
	glm::mat4 modelMatrix;
};