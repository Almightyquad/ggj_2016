#pragma once
#include <vector>
#include <map>
#include <time.h>
#include <thread>

#include "ActorFactory.h"
#include "Console.h"
#include "LevelLoader.h"

#include "GraphicsHandler.h"
#include "PhysicsHandler.h"
#include "AudioHandler.h"
#include "TextureHandler.h"
#include "MeshHandler.h"
#include "GuiHandler.h"
#include "ShaderHandler.h"
#include "EventHandler.h"
#include "FontHandler.h"
#include "TimeRewinder.h"
#include "TerrainHandler.h"
#include "SkyboxHandler.h"

#include "RandomSpawn.h"

#include "common.h"
#include "Camera.h"

#include "Receiver.h"
#include "WeatherParticles.h"

#include "ChunkHandler.h"


#ifdef __ANDROID__
#include <android/asset_manager.h>
#include <android/asset_manager_jni.h>
#include <android/log.h>
#include "android/AssetFile.h"
#include "jni.h"
#include "android/SdlAndroidActivity.h"
#include "android\AssetManager.h"
#else
#include "ParticleSystem.h"
#include "Network.h"
#include "NetworkServerHandler.h"
#include "NetworkClientHandler.h"
//#include "LightHandler.h"
#endif

class Actor;

/**
 * @class	Engine
 *
 * @brief	The engine.
 * @details	Owns all handlers and factories
 * 			Runs the main loop and tracks the gamestate
 * @see		Engine::gameLoop()
 * @see		Handler
 * 			
 */

class Engine : public IReceiver
{ 
public:
	
	void initRunning();
	void initMainmenu();
	void initLevelSelect();
	void initGameover();
	void initSettings();
	void initJoining();
	void initHosting();
	void initQuitting();

	void updateRunning();
	void updateMainmenu();
	void updateLevelSelect();
	void updateGameover();
	void updateSettings();
	void updateJoining();
	void updateHosting();
	void updateQuitting();
public:
	Engine();
	~Engine();
	
	void gameLoop();
	void init();
	void run();
	/**
	 * @fn	void Engine::genericUpdate();
	 *
	 * @brief	Updates generic stuff, including eventhandling
	 */
	void genericUpdate();
	/**
	 * @fn	void Engine::genericDraw();
	 *
	 * @brief	Generic draw.
	 */
	void genericDraw();
	
	ActorFactory* getActorFactory() const;
	PhysicsHandler* getPhysicsHandler() const;
	AudioHandler* getAudioHandler() const;
	GraphicsHandler* getGraphicsHandler() const;
	TextureHandler* getTextureHandler() const;
	MeshHandler* getMeshHandler() const;
	GuiHandler* getGuiHandler() const;
	ShaderHandler* getShaderHandler() const;
	EventHandler* getEventHandler() const;
	TimeRewinder* getTimeKeeper() const;
	FontHandler* getFontHandler() const;
	TerrainHandler* getTerrainHandler() const;
	SkyboxHandler* getSkyboxHandler(const int NUMBER) const;
	ParticleSystem* getParticleSystem() const;
	
#ifdef __ANDROID__
	krem::android::AssetManager* getAssetMgr() const;
#else
	NetworkClientHandler* getClientHandler() const;
	NetworkServerHandler* getServerHandler() const;

#endif
	LevelLoader* getLevelLoader() const;
	XmlHandler* getXmlHandler() const;
	RandomSpawn* getRandomSpawn() const;


	SDL_Event SDLEvent; //primary eventcontainer
	
	
	float getDeltaTime();
	void setPlayer(Actor& player);
	Actor* getPlayer() const;
	int getSelectedTool() const;
	int getSelectedLevel() const;

	void setGameState(int gameState);
	int getGameState();

	void setSeed(unsigned int seed);
	unsigned int getSeed();

	void connectionlistener();

	/** Adds listeners that listens to the gived gui elements */
	void addListeners();

	/**
	 * Receive message from gui elements. This should not be in the engine really
	 *
	 * @param	message	The message.
	 */
	void receiveMessage(const Message &message);
	void increaseEnemiesKilled();
	bool getJoining();

private:
	Actor* player;
	template<typename T>
	void addChild(T t);
	void drawMenuGraphics();
	void updateMenuGraphics();

	float deltaTime = 1.f; //we save deltaTime as seconds
	unsigned int previousTime = 1; //previous time is stored as milliseconds for precision

	std::string playerString;
	int selectedTool;
	int selectedLevel;
	/*
	sf::Thread* server;
	sf::Thread* client;*/
	int enemiesKilled;
	int killsNeededToWin;
	bool levelLoaded = false;
	bool consoleactivated = false;
	
	Console console;

	std::unique_ptr<ActorFactory> actorFactory;
	std::unique_ptr<GraphicsHandler> graphicsHandler;
	std::unique_ptr<PhysicsHandler> physicsHandler;
	std::unique_ptr<TextureHandler> textureHandler;
	std::unique_ptr<AudioHandler> audioHandler;
	std::unique_ptr<MeshHandler> meshHandler;
	std::unique_ptr<GuiHandler> guiHandler;
	std::unique_ptr<ShaderHandler> shaderHandler;
	std::unique_ptr<EventHandler> eventHandler;
	std::unique_ptr<TimeRewinder> timeRewinder;
	std::unique_ptr<FontHandler> fontHandler;
	std::unique_ptr<TerrainHandler> terrainHandler;
	std::vector<std::unique_ptr<SkyboxHandler>> skyboxHandlers;
	std::unique_ptr<WeatherParticles> weatherParticles;
	std::unique_ptr<ParticleSystem> particleSystem;
	std::unique_ptr<XmlHandler> xmlHandler;
	std::unique_ptr<LevelLoader> levelLoader;
	std::unique_ptr<RandomSpawn> randomSpawn;
#ifdef __ANDROID__
	std::unique_ptr<krem::app::SdlAndroidActivity> sdlAndroidActivity;
	std::unique_ptr<krem::android::AssetManager> assetMgr;
	JNIEnv *javaEnv;
	jobject javaAssetManager;
#else
	std::unique_ptr<NetworkClientHandler> networkClientHandler;
	std::unique_ptr<NetworkServerHandler> networkServerHandler;
	//std::unique_ptr<LightHandler> lightHandler;
#endif
	ChunkHandler chunkHandler;

	bool hosting;
	bool joining;
	int gameState;	
	int nextGameState;
	bool running;
	std::string input;
	Timer netUpdateTimer;
	Timer oneSecondTimer;
	int fps;
	float enemySpawnTimer;
	float enemySpawnRate;
	std::vector<std::string> enemyVec;
	unsigned int randomSeed;

	std::string IP;
	int32_t localPort = 0;
	int32_t remotePort = 0;
	std::string message;

};
