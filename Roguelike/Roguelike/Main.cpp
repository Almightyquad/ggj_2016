#include "Engine.h"
#include <memory>

#include "PhysicsScenery.h"
#include "EnemyPhysics.h"
#include "EnemyProjectilePhysics.h"
#include "FriendlyProjectilePhysics.h"
#include "NeutralProjectilePhysics.h"

#include "PlayerGroundPhysics.h"
#include "PlayerGroundInput.h"

#include "PlayerGodInput.h"

#include "MinotaurArtificialIntelligence.h"
#include "CyclopArtificialIntelligence.h"

int main(int argc, char* args[])
{
	std::unique_ptr<Engine> engine = std::make_unique<Engine>();
	engine->init();

	PhysicsCreatorImplementation<PhysicsScenery> PhysicsCreator0("Scenery", engine->getActorFactory()->getPhysicsFactory());
	PhysicsCreatorImplementation<PlayerGroundPhysics> PhysicsCreator1("PlayerGroundPhysics", engine->getActorFactory()->getPhysicsFactory());
	PhysicsCreatorImplementation<EnemyPhysics> PhysicsCreator2("EnemyPhysics", engine->getActorFactory()->getPhysicsFactory());
	PhysicsCreatorImplementation<EnemyProjectilePhysics> PhysicsCreator3("EnemyProjectilePhysics", engine->getActorFactory()->getPhysicsFactory());
	PhysicsCreatorImplementation<FriendlyProjectilePhysics> PhysicsCreator4("FriendlyProjectilePhysics", engine->getActorFactory()->getPhysicsFactory());
	PhysicsCreatorImplementation<NeutralProjectilePhysics> PhysicsCreator5("NeutralProjectilePhysics", engine->getActorFactory()->getPhysicsFactory());
	
	InputCreatorImplementation<PlayerGroundInput> InputCreator0("PlayerGroundInput", engine->getActorFactory()->getInputFactory());
	InputCreatorImplementation<PlayerGodInput> InputCreator1("PlayerGodInput", engine->getActorFactory()->getInputFactory());

	AICreatorImplementation<MinotaurArtificialIntelligence> AICreator0("Minotaur", engine->getActorFactory()->getAIFactory());
	AICreatorImplementation<CyclopArtificialIntelligence> AICreator1("Cyclop", engine->getActorFactory()->getAIFactory());

	engine->run();
	return 0;
}