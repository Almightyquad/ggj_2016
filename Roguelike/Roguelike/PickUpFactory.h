#pragma once
#include <map>
#include <string>
#include "PickUpCreator.h"

class PickUpFactory
{
public:
	std::unique_ptr<IPickUpComponent> create(const std::string& itemName, tinyxml2::XMLElement* xmlElement);

	void registerIt(const std::string& itemName, PickUpCreator* pickUpCreator);
	
private:
	std::map<std::string, PickUpCreator*> table;
};