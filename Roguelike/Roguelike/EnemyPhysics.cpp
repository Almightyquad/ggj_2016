#include "EnemyPhysics.h"
#include "Actor.h"
#include "Engine.h"

EnemyPhysics::EnemyPhysics(tinyxml2::XMLElement * xmlElement)
	: PhysicsComponent(xmlElement)
{
}

void EnemyPhysics::onCollision(int fixtureUserData, Actor * collidedActor)
{
	CombatComponent* combatComponent = dynamic_cast<CombatComponent*>(parentActor->getComponent("CombatComponent"));
	glm::vec2 newImpulse{ 0.f,15.f };
	switch (fixtureUserData)
	{
	case CATEGORY_PLAYERGROUND:
		dynamic_cast<CombatComponent*>(collidedActor->getComponent("CombatComponent"))->reduceHealth(combatComponent->getDamage());
		if (body->GetLinearVelocity().x > 0)
		{
			newImpulse.x = 500.f;
		}
		else
		{
			newImpulse.x = -500.f;
		}
		dynamic_cast<PhysicsComponent*>(collidedActor->getComponent("PhysicsComponent"))->applyLinearImpulse(newImpulse, true);
		break;
	default:
		break;
	}
}

void EnemyPhysics::update(float deltaTime)
{
	PhysicsComponent::update(deltaTime);
}

void EnemyPhysics::onDeath()
{
	parentEngine->getActorFactory()->createActor("res/actors/explosion.xml", parentActor->getPosition());
}

void EnemyPhysics::onSpawn()
{
	activated = false;
	sensorContacts = { 0,0,0 };
}