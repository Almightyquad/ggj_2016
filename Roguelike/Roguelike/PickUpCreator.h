#pragma once
#include "IPickUpComponent.h"
#include <memory>

class PickUpFactory;

class PickUpCreator
{
public:
	PickUpCreator(const std::string& classname, PickUpFactory* pickUpFactory);
	virtual std::unique_ptr<IPickUpComponent> create(tinyxml2::XMLElement* xmlElement) = 0;
	
};

template <class T>
class PickUpCreatorImplementation : public PickUpCreator
{
public:
	PickUpCreatorImplementation(const std::string& classname, PickUpFactory* pickUpFactory) : PickUpCreator(classname, pickUpFactory) {};
	virtual std::unique_ptr<IPickUpComponent> create(tinyxml2::XMLElement* xmlElement) override
	{
		return std::make_unique<T>(xmlElement);
	}
};