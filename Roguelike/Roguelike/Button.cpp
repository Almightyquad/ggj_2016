#include "Button.h"
#include "Engine.h"
#include "GuiState.h"
#include "Message.h"


Button::Button(tinyxml2::XMLElement *xmlElement)
{
	this->originNode = xmlElement;
	currentTexture = 0;
	clicked = false;
	fontDisplay = false;
	text = "";
}

Button::~Button()
{
}

void Button::update()
{
	//checks for mouseover
	// TODO: REMOVE MAGIC NUMBERS
	if (parentEngine->getEventHandler()->mouseInside(transform) && !clicked)
	{
		currentTexture = 2;
	}
	else if (clicked)
	{
		currentTexture = 1;
	}
	else
	{
		currentTexture = 0;
	}
	//updates the button text if it exists
}

bool Button::checkMouseClickLeft()
{
	if (parentEngine->getEventHandler()->mouseInside(transform))
	{
		clicked = true;
		
	}
	else
	{
		clicked = false;
	}
	return clicked;
}

bool Button::checkMouseClickRight()
{
	return false;
}

bool Button::checkMouseRelease()
{
	clicked = false;
	if (parentEngine->getEventHandler()->mouseInside(transform))
	{
		const char * tempName = name.c_str();
		//posts a message with its own name so things subscribed to this get the state the button is in to run functions based on the state
		this->postMessage(Message(this, tempName, true));
		return true;
	}
	return false;
}

void Button::draw(glm::mat4 &viewProjection)
{
	shader->bind();
	shader->loadTransform(transform, viewProjection);
	shader->loadVec2(U_SIZE, glm::vec2(textureAtlasSize, textureAtlasSize));
	shader->loadInt(U_TEXTURE0, 0);
	shader->loadInt(U_SPRITE_NO, currentTexture); //finds what texture to draw
	shader->loadFloat(U_SCALE, 1.5f);
	if (fontDisplay)
	{
		font->update(text, glm::vec3(0, 0, 0));
		shader->loadInt(U_TEXTURE1, 1);
		font->bind(1);
	}
	texture->bind(0);
	mesh->draw();
}

void Button::init(Engine* parentEngine)
{
	this->parentEngine = parentEngine;
	float tempPosX;
	float tempPosY;
	float tempWidth;
	float tempHeight;

	std::string pathTemp;

	if (this->originNode->FirstChildElement("name"))
	{
		name = this->originNode->FirstChildElement("name")->GetText();
	}
	else if (xmlDebug)
	{
		printf("Missing name\n");
	}

	if (this->originNode->FirstChildElement("text"))
	{
		text = this->originNode->FirstChildElement("text")->GetText();
		if (this->originNode->FirstChildElement("fontPath"))
		{
			std::string fontPathTemp = this->originNode->FirstChildElement("fontPath")->GetText();
			font = parentEngine->getFontHandler()->loadFont(fontPathTemp);
			fontDisplay = true;
		}
		else if (xmlDebug)
		{
			printf("Missing fontPath\n");
		}
	}
	else if (xmlDebug)
	{
		printf("Missing text\n");
	}

	if (this->originNode->FirstChildElement("positionX"))
	{
		this->originNode->FirstChildElement("positionX")->QueryFloatText(&tempPosX);
	}
	else if (xmlDebug)
	{
		printf("Missing positionX\n");
	}

	if (this->originNode->FirstChildElement("positionY"))
	{
		this->originNode->FirstChildElement("positionY")->QueryFloatText(&tempPosY);
	}
	else if (xmlDebug)
	{
		printf("Missing positionY\n");
	}

	if (this->originNode->FirstChildElement("widthInPixels"))
	{
		this->originNode->FirstChildElement("widthInPixels")->QueryFloatText(&tempWidth);
	}
	else if (xmlDebug)
	{
		printf("Missing widthInPixels\n");
	}

	if (this->originNode->FirstChildElement("heightInPixels"))
	{
		this->originNode->FirstChildElement("heightInPixels")->QueryFloatText(&tempHeight);
	}
	else if (xmlDebug)
	{
		printf("Missing heightInPixels\n");
	}

	if (this->originNode->FirstChildElement("meshVerticesPath"))
	{
		pathTemp = this->originNode->FirstChildElement("meshVerticesPath")->GetText();
		this->mesh = parentEngine->getMeshHandler()->loadModel(pathTemp);
	}
	else if (xmlDebug)
	{
		printf("Missing meshVericesPath\n");
	}

	if (this->originNode->FirstChildElement("texturePath"))
	{

		pathTemp = this->originNode->FirstChildElement("texturePath")->GetText();
		this->texture = parentEngine->getTextureHandler()->loadTexture(pathTemp);
	}
	else if (xmlDebug)
	{
		printf("Missing texturePath\n");
	}
	
	if (this->originNode->FirstChildElement("shaderPath"))
	{

		pathTemp = this->originNode->FirstChildElement("shaderPath")->GetText();
		this->shader = parentEngine->getShaderHandler()->loadShader(pathTemp);
	}
	else if (xmlDebug)
	{
		printf("Missing shaderPath\n");
	}

	if (this->originNode->FirstChildElement("textureAtlasSize"))
	{

		this->originNode->FirstChildElement("textureAtlasSize")->QueryIntText(&textureAtlasSize);
	}
	else if (xmlDebug)
	{
		printf("Missing textureAtlasSize\n");
	}

	this->transform.setPos(glm::vec3(tempPosX, tempPosY, 1.0f));
	this->transform.setScale(glm::vec3(tempWidth, tempHeight, 1.f));
}

void Button::setParent(GuiState& parentPtr)
{
	this->parentGuiState = &parentPtr;
}

GuiState* Button::getParent()
{
	return parentGuiState;
}

std::string Button::getName()
{
	return name;
}