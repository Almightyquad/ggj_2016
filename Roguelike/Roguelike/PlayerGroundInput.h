#pragma once
#include "IInputComponent.h"
#include "Timer.h"

class Actor;
class PlayerGodInput;
class ActorFactory;

class PlayerGroundInput : public IInputComponent
{
public:
	PlayerGroundInput(tinyxml2::XMLElement * xmlElement);
	~PlayerGroundInput();
	void update(float deltatime) override;
	void receiveMessage(const Message & message) override;
	void init();
	void checkAbilityCast();

	void setFrozen(bool status, int duration);

	void castGoodAbilityOne();
	void castGoodAbilityTwo();
	void castEvilAbilityOne();
	void castEvilAbilityTwo();
	void swapPlayers();
	//void onSpawn() override;
private:
	Timer swapCooldown; //5 sec cooldown before new player can swap characters
	Timer forceSwapTimer; //10 sec? before players are forced to swap characters
	Timer abilityOneTimer; 
	Timer abilityTwoTimer;
	Timer frozenDuration;
	Timer castingSpellTimer;

	
	enum facing
	{
		FACING_LEFT = -1,
		FACING_RIGHT = 1
	};

	enum spellToCast
	{
		CAST_NOSPELL,
		CAST_SPELLONE,
		CAST_SPELLTWO
	};
	int spellToCast;
	int facing;
	bool goodPlayerPlaying;
	bool subscribedToEvents;
	bool frozen;

	ActorFactory* parentActorFactory;
	Actor* godActor;
	PlayerGodInput* godActorInput;
	//input for good player
	bool aDown;
	bool dDown;
	bool eDown;
	bool spaceDown;
	bool oneDown;
	bool twoDown;
	bool threeDown;
	bool fourDown;

	//input for evil player
	bool leftDown;
	bool rightDown;
	bool numZeroDown;
	bool numOneDown;
	bool numSevenDown;
	bool numEightDown;
	bool numNineDown;
	bool numFourDown;

	std::vector<std::string> goodGodSpells;
	std::vector<std::string> goodHumanSpells;
	std::vector<std::string> badGodSpells;
	std::vector<std::string> badHumanSpells;

	SDL_JoystickID stickID = -1;
	struct ControllerInput
	{
		bool upButtonDown = false;
		bool downButtonDown = false;
		bool leftBumperDown = false;
		bool rightBumperDown = false;
		krem::Vector2f axis = { 0.f, 0.f };
	};

	std::vector<ControllerInput> inputs;

};