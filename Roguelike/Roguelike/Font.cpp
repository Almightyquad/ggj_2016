#include "Font.h"
#include <sstream>
#include <typeinfo>
#include <stdio.h>
#ifdef __ANDROID__
#include <android/log.h>
#endif

Font::Font(std::string filePath)
{
	color = { 0, 0, 0 };
	fontSize = 28;

#ifdef __ANDROID__
	SDL_RWops *rw = SDL_RWFromFile(filePath.c_str(), "rb");
	if (rw == NULL)
	{
		__android_log_print(ANDROID_LOG_VERBOSE, "RogueLike : ", "rw is NULL");
	}
	font = TTF_OpenFontRW(rw, 1, fontSize);
#else
	font = TTF_OpenFont(filePath.c_str(), fontSize);
#endif
	glGenTextures(1, &texture);
}


Font::~Font()
{
	TTF_CloseFont(font);
}

//something we found online to make a SDL texture into a openGL texture to use for our fonts
void Font::update(std::string text, glm::vec3 textColor)
{
	color = {static_cast<Uint8>(textColor.x), static_cast<Uint8>(textColor.y), static_cast<Uint8>(textColor.z) };
	SDL_Surface *tempSurface = new SDL_Surface();
	GLenum texture_format;
	Uint8 colors;
	tempSurface = TTF_RenderUTF8_Blended(static_cast<TTF_Font*>(font), text.c_str(), color);
#ifdef __ANDROID__
	if (tempSurface == NULL)
	{
		__android_log_print(ANDROID_LOG_VERBOSE, "RogueLike : ", "tempSurface is NULL");
	}
#endif
	//turn SDL_surfaces into an openGL texture to draw on a mesh
	colors = tempSurface->format->BytesPerPixel;
	if (colors == 4) {   // alpha
		if (tempSurface->format->Rmask == 0x000000ff)
			//texture_format = GL_RGBA;
			texture_format = 0x1908;
		else
			//texture_format = GL_BGRA;
			texture_format = 0x80E1;
	}
	
	else {             // no alpha
		if (tempSurface->format->Rmask == 0x000000ff)
			//texture_format = GL_RGB;
			texture_format = 0x1907;
		else
			//texture_format = GL_BGR;
			texture_format = 0x80E0;
	}
	glBindTexture(GL_TEXTURE_2D, texture);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
#ifdef __ANDROID__
	glTexImage2D(GL_TEXTURE_2D, 0, texture_format, tempSurface->w, tempSurface->h, 0,
		texture_format, GL_UNSIGNED_BYTE, tempSurface->pixels);
#else
	glTexImage2D(GL_TEXTURE_2D, 0, colors, tempSurface->w, tempSurface->h, 0,
		texture_format, GL_UNSIGNED_BYTE, tempSurface->pixels);
#endif
	SDL_FreeSurface(tempSurface);
}

void Font::bind(unsigned int unit)
{
	assert(unit >= 0 && unit <= 31);
	glActiveTexture(GL_TEXTURE0 +unit);
	glBindTexture(GL_TEXTURE_2D, texture);
}
