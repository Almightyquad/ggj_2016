#pragma once
#include "PhysicsComponent.h"

class PhysicsGoal final : public PhysicsComponent
{
public:

	~PhysicsGoal();
	PhysicsGoal(tinyxml2::XMLElement *xmlElement) : PhysicsComponent(xmlElement) {};
	void onCollision(int fixtureUserData, Actor* collidedActor);
	void endCollision(int fixtureUserData, Actor* collidedActor);
private:
};