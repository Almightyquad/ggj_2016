#pragma once
#include "IPickUpComponent.h"

class PickUpHealthPotion final : public IPickUpComponent
{
public:
	PickUpHealthPotion(tinyxml2::XMLElement *xmlElement) : IPickUpComponent(xmlElement) {
	};
	virtual void onPickup(Actor* newOwner) override;
private:
	int healthValue = 50;
};