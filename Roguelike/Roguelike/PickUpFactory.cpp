#include "PickUpFactory.h"

std::unique_ptr<IPickUpComponent> PickUpFactory::create(const std::string & itemName, tinyxml2::XMLElement* xmlElement)
{
	auto it = table.find(itemName);
	if (it != table.end())
	{
		return it->second->create(xmlElement);
	}
	else
	{
		return nullptr;
	}
}

void PickUpFactory::registerIt(const std::string & itemName, PickUpCreator * pickUpCreator)
{
	table[itemName] = pickUpCreator;
}