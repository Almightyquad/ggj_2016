#pragma once
#include "ActorComponent.h"

/**
* @class	InputComponent
*
* @brief	The input component interface. Primarily used by players and items
*/
class IInputComponent : public ActorComponent
{
public:
	IInputComponent(tinyxml2::XMLElement *xmlElement) : ActorComponent(xmlElement) {};
	virtual ~IInputComponent();
	void init(Engine* parentEngine) override;
	virtual void receiveMessage(const Message &message) = 0;
protected:
	glm::vec3 mousePos;
	bool inputChanged = false;
};