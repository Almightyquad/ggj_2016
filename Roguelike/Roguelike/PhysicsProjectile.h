#pragma once
#include "PhysicsComponent.h"

class PhysicsProjectile final : public PhysicsComponent
{
public:
	PhysicsProjectile(tinyxml2::XMLElement *xmlElement) : PhysicsComponent(xmlElement) {};
	void onCollision(int fixtureUserData, Actor* collidedActor);
private:
};