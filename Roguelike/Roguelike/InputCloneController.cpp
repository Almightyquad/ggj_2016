#include "InputCloneController.h"
#include "Engine.h"
#include "Message.h"
#include "Actor.h"
#include "TextureHandler.h"
#include "PhysicsClone.h"

InputCloneController::InputCloneController(tinyxml2::XMLElement * xmlElement)
	: IInputComponent(xmlElement),
	maxNumberOfClones(2000),
	numberOfClones(1),
	singleTargetLengthThreshold(0.35f)
{
	colorPickerTexture = "";
}

void InputCloneController::update(float deltatime)
{	
	currentTool = parentEngine->getSelectedTool();
	if (currentTool != TOOL_COLORPICKER)
	{
		select();
	}
	
	glm::vec3 movement = { 0,0,0 };
	float camSpeed = 10; // worldspace speed of camera
	if (upDown)
	{
		movement.y += camSpeed*deltatime;
	}
	if (downDown)
	{
		movement.y -= camSpeed*deltatime;
	}
	if (leftDown)
	{
		movement.x -= camSpeed*deltatime;
	}
	if (rightDown)
	{
		movement.x += camSpeed*deltatime;
	}
	moveSelected();
	parentActor->movePosition(movement);
}

void InputCloneController::activateTool()
{
	switch (currentTool)
	{
	case TOOL_COPY:
		copy();
		break;
	case TOOL_COLORPICKER:
		colorPicker();
		break;
	case TOOL_ERASE:
		erase();
		break;
	case TOOL_RESIZE:
		resize();
		break;
	default:
		std::cout << "This should not even be possible :SSSS" << std::endl;
		break;
	}
}

void InputCloneController::copy()
{

	bool didCopy = false;
	std::string copySound = "res/sounds/pop.wav";

	if (copyUses > 0 && actorsToBeCloned.size() > 0)
	{
		for (auto it : selectedActors)
		{
			dynamic_cast<GraphicsComponent*>(it.actor->getComponent("GraphicsComponent"))->setTexture(it.texture);
		}
		if (numberOfClones < maxNumberOfClones && actorsToBeCloned.size() > 0)
		{
			didCopy = true;
		}
		for (auto it : actorsToBeCloned)
		{
			if (numberOfClones < maxNumberOfClones)
			{
				dynamic_cast<PhysicsClone*>(it->getComponent("PhysicsComponent"))->setActivated(true);
				it->setPhysicsActive(true);
				numberOfClones++;
				//dynamic_cast<GraphicsComponent*>(it->getComponent("GraphicsComponent"))->setTexture(it);
			}
			else
			{
				parentEngine->getActorFactory()->addToDeleteQueue(it);
			}
		}
	}
	parentEngine->getActorFactory()->addToDeleteQueue(selectionActor);
	selectionActor = nullptr;

	if (didCopy)
	{
		copyUses--;	
		parentEngine->getAudioHandler()->playSound(copySound);
	}
	actorsToBeCloned.clear();
}

void InputCloneController::colorPicker()
{
	
	for (auto it : parentEngine->getActorFactory()->getActors())
	{
		glm::vec3 tempPosition;
		tempPosition = it->getPosition() - mouseWorldPos;

		if (glm::length(tempPosition) < singleTargetLengthThreshold)
		{
			if (it->getActorId().actorName == "Clone")
			{
				if (colorPickerTexture != "")
				{
					if (colorPickerTexture == "Rock")
					{
						Texture* tempTexture = parentEngine->getTextureHandler()->loadTexture("res/textures/characters/jakkheim/walkingStone.png");
						dynamic_cast<GraphicsComponent*>(it->getComponent("GraphicsComponent"))->setTexture(tempTexture);
						dynamic_cast<PhysicsClone*>(it->getComponent("PhysicsComponent"))->setGravityScale(3);
						dynamic_cast<PhysicsClone*>(it->getComponent("PhysicsComponent"))->setAngel(false);
					}
					if (colorPickerTexture == "Goal")
					{
						Texture* tempTexture = parentEngine->getTextureHandler()->loadTexture("res/textures/characters/jakkheim/walkingAngel.png");
						dynamic_cast<GraphicsComponent*>(it->getComponent("GraphicsComponent"))->setTexture(tempTexture);
						dynamic_cast<PhysicsClone*>(it->getComponent("PhysicsComponent"))->setGravityScale(0);
						dynamic_cast<PhysicsClone*>(it->getComponent("PhysicsComponent"))->setAngel(true);
					}
					if (colorPickerTexture == "Seesaw" || colorPickerTexture == "SeesawPlank")
					{
						Texture* tempTexture = parentEngine->getTextureHandler()->loadTexture("res/textures/characters/jakkheim/walkingWood.png");
						dynamic_cast<GraphicsComponent*>(it->getComponent("GraphicsComponent"))->setTexture(tempTexture);
						dynamic_cast<PhysicsClone*>(it->getComponent("PhysicsComponent"))->setGravityScale(0.5);
						dynamic_cast<PhysicsClone*>(it->getComponent("PhysicsComponent"))->setAngel(false);
					}
				}
			}
		}
	}

	
}

void InputCloneController::erase()
{	
	bool didErase = false;

	if (eraseUses > 0 && selectedActors.size() > 0)
	{
		for (auto it : selectedActors)
		{
			if (numberOfClones > 1)
			{
				it.actor->onDeath();
				parentEngine->getActorFactory()->addToDeleteQueue(it.actor);
				numberOfClones--;
				didErase = true;
			}
		}
	}	
	if (didErase)
	{
		eraseUses--;
	}
}

void InputCloneController::select()
{
	bool originalXHigh = false;
	bool originalYHigh = false;

	originalPos.x > mouseWorldPos.x ?
		originalXHigh = true :
		originalXHigh = false;

	originalPos.y > mouseWorldPos.y ?
		originalYHigh = true :
		originalYHigh = false;

	//this is beautiful :'(
	if (mouseDown)
	{
		if (!(shiftDown || ctrlDown))
		{
			for (int i = 0; i < selectedActors.size(); i++)
			{
				if (originalXHigh)
				{
					if (!(selectedActors[i].actor->getPosition().x > mouseWorldPos.x && selectedActors[i].actor->getPosition().x < originalPos.x))
					{
						dynamic_cast<GraphicsComponent*>(selectedActors[i].actor->getComponent("GraphicsComponent"))->setTexture(selectedActors[i].texture);
						selectedActors.erase(selectedActors.begin() + i);
						i--;
						continue;
					}
				}
				else
				{
					if (!(selectedActors[i].actor->getPosition().x < mouseWorldPos.x && selectedActors[i].actor->getPosition().x > originalPos.x))
					{
						dynamic_cast<GraphicsComponent*>(selectedActors[i].actor->getComponent("GraphicsComponent"))->setTexture(selectedActors[i].texture);
						selectedActors.erase(selectedActors.begin() + i);
						i--;
						continue;
					}
				}
				if (originalYHigh)
				{
					if (!(selectedActors[i].actor->getPosition().y > mouseWorldPos.y && selectedActors[i].actor->getPosition().y < originalPos.y))
					{
						dynamic_cast<GraphicsComponent*>(selectedActors[i].actor->getComponent("GraphicsComponent"))->setTexture(selectedActors[i].texture);
						selectedActors.erase(selectedActors.begin() + i);
						i--;
						continue;
					}
				}
				else
				{
					if (!(selectedActors[i].actor->getPosition().y < mouseWorldPos.y && selectedActors[i].actor->getPosition().y > originalPos.y))
					{
						dynamic_cast<GraphicsComponent*>(selectedActors[i].actor->getComponent("GraphicsComponent"))->setTexture(selectedActors[i].texture);
						selectedActors.erase(selectedActors.begin() + i);
						i--;
						continue;
					}
				}
			}
		}

		//this line is particularily beautiful. will optimize if required. it works
		for (auto it : selectedActors)
		{
			dynamic_cast<GraphicsComponent*>(it.actor->getComponent("GraphicsComponent"))->setTexture(it.texture);
		}
		selectedActors.clear();

		for (auto it : parentEngine->getActorFactory()->getActors())
		{
			if (it->getActorId().actorName == "Clone")
			{
				if (it->isActive())
				{
					if (originalXHigh)
					{
						if (!(it->getPosition().x > mouseWorldPos.x && it->getPosition().x < originalPos.x))
						{
							continue;
						}
					}
					else
					{
						if (!(it->getPosition().x < mouseWorldPos.x && it->getPosition().x > originalPos.x))
						{
							continue;
						}
					}
					if (originalYHigh)
					{
						if (!(it->getPosition().y > mouseWorldPos.y && it->getPosition().y < originalPos.y))
						{
							continue;
						}
					}
					else
					{
						if (!(it->getPosition().y < mouseWorldPos.y && it->getPosition().y > originalPos.y))
						{
							continue;
						}
					}

					SelectedActor newSelectedActor; 
					newSelectedActor.actor = it;
					newSelectedActor.texture = dynamic_cast<GraphicsComponent*>(it->getComponent("GraphicsComponent"))->getTexture();
				
					Texture* tempTexture = parentEngine->getTextureHandler()->loadTexture("res/textures/characters/jakkheim/walkingStone.png");
					if (tempTexture == newSelectedActor.texture)
					{
						newSelectedActor.selectedTexture = parentEngine->getTextureHandler()->loadTexture("res/textures/characters/jakkheim/selectedStone.png");
					}

					tempTexture = parentEngine->getTextureHandler()->loadTexture("res/textures/characters/jakkheim/walkingAngel.png");
					if (tempTexture == newSelectedActor.texture)
					{
						newSelectedActor.selectedTexture = parentEngine->getTextureHandler()->loadTexture("res/textures/characters/jakkheim/selectedAngel.png");
					}

					tempTexture = parentEngine->getTextureHandler()->loadTexture("res/textures/characters/jakkheim/walkingWood.png");
					if (tempTexture == newSelectedActor.texture)
					{
						newSelectedActor.selectedTexture = parentEngine->getTextureHandler()->loadTexture("res/textures/characters/jakkheim/selectedWood.png");
					}

					tempTexture = parentEngine->getTextureHandler()->loadTexture("res/textures/characters/jakkheim/walking.png");
					if (tempTexture == newSelectedActor.texture)
					{
						newSelectedActor.selectedTexture = parentEngine->getTextureHandler()->loadTexture("res/textures/characters/jakkheim/selected.png");
					}
					
					dynamic_cast<GraphicsComponent*>(it->getComponent("GraphicsComponent"))->setTexture(newSelectedActor.selectedTexture);

					selectedActors.push_back(newSelectedActor);
				}
			}
		}
	}
}

void InputCloneController::resize()
{
	/*if (newSelectedActor)
	{
		newSelectedActor = false;
	}*/
}

void InputCloneController::moveSelected()
{
	glm::vec2 desiredSpeed{ 0,0 };
	if (aDown)
	{
		desiredSpeed.x -= 2;
	}
	if (dDown)
	{
		desiredSpeed.x += 2;
	}
	for (auto it : selectedActors)
	{
		
		PhysicsComponent* physicsComponent = dynamic_cast<PhysicsComponent*>(it.actor->getComponent("PhysicsComponent"));
		if (!(dynamic_cast<PhysicsClone*>(it.actor->getComponent("PhysicsComponent"))->getAngel()))
		{
			physicsComponent->applySpeed(desiredSpeed);
			if (spaceDown)
			{
				physicsComponent->applyLinearImpulse({ 0.f,3.f });
			}
		}
	}
}

void InputCloneController::receiveMessage(const Message & message)
{
	const char* messageString = static_cast<const char*>(message.getVariable(0));
	
	if (message.getSenderType() == typeid(EventHandler))
	{
		if (messageString == "keyDown")
		{
			int number = message.getVariable(1);
			switch (number)
			{
			case SDLK_UP:
				upDown = true;
				break;
			case SDLK_DOWN:
				downDown = true;
				break;
			case SDLK_LEFT:
				leftDown = true;
				break;
			case SDLK_RIGHT:
				rightDown = true;
				break;
			case SDLK_w:
				wDown = true;
				break;
			case SDLK_s:
				sDown = true;
				break;
			case SDLK_a:
				aDown = true;
				break;
			case SDLK_d:
				dDown = true;
				break;

			case SDLK_SPACE:
				spaceDown = true;
				break;
			case SDLK_LCTRL:
			case SDLK_RCTRL:
				ctrlDown = true;
				break;
			case SDLK_LSHIFT:
			case SDLK_RSHIFT:
				shiftDown = true;
				break;
			default:
				break;
			}
			return;
		}
		if (messageString == "keyUp")
		{
			int number = message.getVariable(1);
			switch (number)
			{
			case SDLK_UP:
				upDown = false;
				break;
			case SDLK_DOWN:
				downDown = false;
				break;
			case SDLK_LEFT:
				leftDown = false;
				break;
			case SDLK_RIGHT:
				rightDown = false;
				break;
			case SDLK_w:
				wDown = false;
				break;
			case SDLK_s:
				sDown = false;
				break;
			case SDLK_a:
				aDown = false;
				break;
			case SDLK_d:
				dDown = false;
				break;
			case SDLK_1:
				parentEngine->getGuiHandler()->setContainerValue("actionBarHuman", 0);
				break;
			case SDLK_2:
				parentEngine->getGuiHandler()->setContainerValue("actionBarHuman", 1);
				break;
			case SDLK_3:
				parentEngine->getGuiHandler()->setContainerValue("actionBarHuman", 2);
				break;
			case SDLK_4:
				parentEngine->getGuiHandler()->setContainerValue("actionBarHuman", 3);
				break;
			case SDLK_SPACE:
				spaceDown = false;
				break;
			case SDLK_LCTRL:
			case SDLK_RCTRL:
				ctrlDown = false;
				break;
			case SDLK_LSHIFT:
			case SDLK_RSHIFT:
				shiftDown = false;
			default:
				break;
			}
			return;
		}
		if (messageString == "mouseLeftDown")
		{
			if (currentTool == TOOL_COLORPICKER)
			{
				return;
			}
			if (selectionActor != nullptr)
			{
				parentEngine->getActorFactory()->addToDeleteQueue(selectionActor);
				selectionActor = nullptr;
			}
			if (selectionActor == nullptr)
			{
				selectionActor = parentEngine->getActorFactory()->createActor("res/actors/selectionBox.xml", glm::vec3(mouseWorldPos.x, mouseWorldPos.y, 0.f));
				//createdSelection = true;
			}
			if (!(shiftDown || ctrlDown))
			{
				for (auto it : actorsToBeCloned)
				{
					parentEngine->getActorFactory()->deleteActor(it);
				}
				actorsToBeCloned.clear();

				for (auto it : selectedActors)
				{
					dynamic_cast<GraphicsComponent*>(it.actor->getComponent("GraphicsComponent"))->setTexture(it.texture);
				}

				selectedActors.clear();
			}
			mouseDown = true;
 			glm::vec3 spawnPos = parentActor->getPosition() + (mousePos*glm::vec3{ parentEngine->getGraphicsHandler()->getUnitsOnScreen()/2.f,0 });
			originalPos = { spawnPos.x,spawnPos.y };
			return;
		}
		if (messageString == "mouseLeftUp")
		{
			mouseDown = false;
			createdSelection = false;

			for (auto it : parentEngine->getActorFactory()->getActors())
			{
				glm::vec3 tempPosition;
				tempPosition = it->getPosition() - mouseWorldPos;

				if (glm::length(tempPosition) < singleTargetLengthThreshold)
				{
					if (it->getActorId().actorName == "Clone" && currentTool != TOOL_COLORPICKER)
					{
						if (!dragged)
						{
							SelectedActor newSelectedActor;
							newSelectedActor.actor = it;
							newSelectedActor.texture = dynamic_cast<GraphicsComponent*>(it->getComponent("GraphicsComponent"))->getTexture();

							Texture* tempTexture = parentEngine->getTextureHandler()->loadTexture("res/textures/characters/jakkheim/walkingStone.png");
							if (tempTexture == newSelectedActor.texture)
							{
								newSelectedActor.selectedTexture = parentEngine->getTextureHandler()->loadTexture("res/textures/characters/jakkheim/selectedStone.png");
							}

							tempTexture = parentEngine->getTextureHandler()->loadTexture("res/textures/characters/jakkheim/walkingAngel.png");
							if (tempTexture == newSelectedActor.texture)
							{
								newSelectedActor.selectedTexture = parentEngine->getTextureHandler()->loadTexture("res/textures/characters/jakkheim/selectedAngel.png");
							}

							tempTexture = parentEngine->getTextureHandler()->loadTexture("res/textures/characters/jakkheim/walkingWood.png");
							if (tempTexture == newSelectedActor.texture)
							{
								newSelectedActor.selectedTexture = parentEngine->getTextureHandler()->loadTexture("res/textures/characters/jakkheim/selectedWood.png");
							}

							tempTexture = parentEngine->getTextureHandler()->loadTexture("res/textures/characters/jakkheim/walking.png");
							if (tempTexture == newSelectedActor.texture)
							{
								newSelectedActor.selectedTexture = parentEngine->getTextureHandler()->loadTexture("res/textures/characters/jakkheim/selected.png");
							}


							dynamic_cast<GraphicsComponent*>(it->getComponent("GraphicsComponent"))->setTexture(newSelectedActor.selectedTexture);

							selectedActors.push_back(newSelectedActor);
						}
					}
					if (currentTool == TOOL_COLORPICKER)
					{
						if (it->getActorId().actorName != "PreventCopyBox")
						{
							colorPickerTexture = it->getActorId().actorName;
						}
					}
				}
			}
			if (selectedActors.size() == 0)
			{

				if (selectionActor != nullptr)
				{
					parentEngine->getActorFactory()->addToDeleteQueue(selectionActor);
					selectionActor = nullptr;
				}
				return;
			}
			if (selectionActor != nullptr)
			{
				if (currentTool != TOOL_COPY)
				{
					parentEngine->getActorFactory()->deleteActor(selectionActor);
					selectionActor = nullptr;
				}
				if (currentTool == TOOL_COPY)
				{
					Actor* newActor;
					for (auto it : selectedActors)
					{				
						newActor = parentEngine->getActorFactory()->copyActor(it.actor, it.texture);
						//newActor->setActive(false);
						newActor->setPhysicsActive(false);
						actorsToBeCloned.push_back(newActor);
					}	
				}		
			}	
			dragged = false;
			return;
		}
		if (messageString == "mouseMotion")
		{
			mousePos = static_cast<glm::vec3>(message.getVariable(1));
			
			glm::vec3 newMouseWorldPos = parentActor->getPosition() + (mousePos*glm::vec3{ parentEngine->getGraphicsHandler()->getUnitsOnScreen() / 2.f,0 });
			glm::vec3 deltaMouseWorldPos = newMouseWorldPos - mouseWorldPos;
			mouseWorldPos = newMouseWorldPos;

			if (mouseDown)
			{
				dragged = true;
				glm::vec3 newScale = {originalPos.x - mouseWorldPos.x, originalPos.y - mouseWorldPos.y, 0.f };
				glm::vec3 newPos = { originalPos.x, originalPos.y, 0.52f };
				newPos.x -= newScale.x / 2.f;
				newPos.y -= newScale.y / 2.f;
				dynamic_cast<GraphicsComponent*>(selectionActor->getComponent("GraphicsComponent"))->setScale(newScale);
				selectionActor->setPosition(newPos);
			}
			if (!mouseDown && selectionActor != nullptr && currentTool == TOOL_COPY)
			{
				glm::vec3 newPos = { deltaMouseWorldPos };
				selectionActor->movePosition(deltaMouseWorldPos);
				for (auto it : actorsToBeCloned)
				{
					it->movePosition(deltaMouseWorldPos);
				}
			}
			return;

		}
		if (messageString == "mouseRightUp")
		{
			activateTool();
			selectedActors.clear();
		}
		if (messageString == "mouseWheel")
		{
			float y;
			SDL_Event SDLEvent = message.getVariable(1);
			y = SDLEvent.wheel.y;
			parentEngine->getGraphicsHandler()->setViewDistance(parentEngine->getGraphicsHandler()->getViewDistance()-(y*1.5));
		}
	}
}