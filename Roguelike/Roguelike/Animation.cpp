#include "Animation.h"


Animation::Animation()
{
}
Animation::~Animation()
{
}

void Animation::config(float animRate, int animFrames, int frameStart)
{
	this->animationSpeed = animRate;
	this->amountOfFrames = animFrames;
	this->firstFrame = frameStart;
}

void Animation::update(float deltaTime)
{
	this->curAnim += deltaTime * this->animationSpeed;
}

void Animation::reset()
{
	this->curAnim = 0.0f;
}

int Animation::getFrame()
{
	return this->firstFrame + (static_cast<int>(this->curAnim) % this->amountOfFrames);
}
