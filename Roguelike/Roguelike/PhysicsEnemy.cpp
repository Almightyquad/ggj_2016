#include "PhysicsEnemy.h"
//#include "Actor.h"
#include "Engine.h"

void PhysicsEnemy::onCollision(int fixtureUserData, Actor * collidedActor)
{
	int damage;
	glm::vec3 pos;
	switch (fixtureUserData)
	{
	case CATEGORY_PROJECTILE:
		parentActor->onHit();
		damage = dynamic_cast<CombatComponent*>(parentActor->getComponent("CombatComponent"))->getDamage();
		dynamic_cast<CombatComponent*>(parentActor->getComponent("CombatComponent"))->reduceHealth(damage);
		pos = dynamic_cast<PhysicsComponent*>(parentActor->getComponent("PhysicsComponent"))->getPosition();
		parentEngine->getParticleSystem()->createPoint(10000.f, parentEngine->getDeltaTime(), pos, 3, glm::vec4(0.9f, 0.1f, 0.1f, 1.0f), glm::vec3(3.f, 3.f, 3.f));
		break;
	case CATEGORY_PLAYER:
		break;
	case CATEGORY_MELEE:
		damage = dynamic_cast<CombatComponent*>(parentActor->getComponent("CombatComponent"))->getDamage();
		dynamic_cast<CombatComponent*>(parentActor->getComponent("CombatComponent"))->reduceHealth(damage);
		pos = dynamic_cast<PhysicsComponent*>(parentActor->getComponent("PhysicsComponent"))->getPosition();
		parentEngine->getParticleSystem()->createPoint(10000.f, parentEngine->getDeltaTime(), pos, 3, glm::vec4(0.9f, 0.1f, 0.1f, 1.0f), glm::vec3(3.f, 3.f, 3.f));
	case CATEGORY_ENEMY:
		/*damage = dynamic_cast<CombatComponent*>(parentActor->getComponent("CombatComponent"))->getDamage();
		dynamic_cast<CombatComponent*>(parentActor->getComponent("CombatComponent"))->reduceHealth(damage);
		pos = dynamic_cast<PhysicsComponent*>(parentActor->getComponent("PhysicsComponent"))->getPosition();
		parentEngine->getParticleSystem()->createPoint(10000.f, parentEngine->getDeltaTime(), pos, 3, glm::vec4(0.9f, 0.1f, 0.1f, 1.0f), glm::vec3(3.f, 3.f, 3.f));*/
		break;
	default:
		break;
	}
}
