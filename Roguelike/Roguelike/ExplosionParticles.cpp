#include "ExplosionParticles.h"

void ExplosionParticles::update(float deltaTime)
{
	for (std::size_t i = 0; i < particles.size(); i++)
	{
		if (this->gravity == true)
		{
			if (particles[i].velocity.y > -this->maxVelocity)
			{
				particles[i].velocity.y -= this->maxVelocity / 4.f;
			}
			else
			{
				particles[i].velocity.y = -this->maxVelocity;
			}
		}
		float color = (rand() % 100) / 100.f;
		colors[i] = glm::vec4(1.f, color, 0.f, 1.f);
		positions[i] += (particles[i].velocity * deltaTime);

		particles[i].life -= (1.f * deltaTime);
		if (particles[i].life <= 0.f)
		{
			particles.erase(particles.begin() + i);
			colors.erase(colors.begin() + i);
			positions.erase(positions.begin() + i);
		}
	}
}
