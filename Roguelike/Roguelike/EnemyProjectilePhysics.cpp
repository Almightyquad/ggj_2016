#include "EnemyProjectilePhysics.h"
#include "Actor.h"
#include "Engine.h"

EnemyProjectilePhysics::EnemyProjectilePhysics(tinyxml2::XMLElement * xmlElement)
	:PhysicsComponent(xmlElement),
	combatComponent(nullptr)
{
}

void EnemyProjectilePhysics::onCollision(int fixtureUserData, Actor * collidedActor)
{
	if (combatComponent == nullptr)
	{
		combatComponent = dynamic_cast<CombatComponent*>(parentActor->getComponent("CombatComponent"));
	}
	glm::vec3 pos = parentActor->getPosition();

	switch (fixtureUserData)
	{
	case CATEGORY_PLAYERGROUND:
		dynamic_cast<CombatComponent*>(collidedActor->getComponent("CombatComponent"))->reduceHealth(combatComponent->getDamage());
		parentEngine->getActorFactory()->addToDeleteQueue(parentActor);
		break;
	case CATEGORY_SCENERY:
		parentEngine->getParticleSystem()->createPoint(1000.f, parentEngine->getDeltaTime(), glm::vec3({ pos.x,pos.y + 0.5f,0 }), 3, glm::vec4(0.4f, 0.4f, 0.4f, 1.0f), glm::vec3(0.7f, 0.7f, 0.7f));
		parentEngine->getActorFactory()->addToDeleteQueue(parentActor);
		break;
	default:
		break;
	}
}