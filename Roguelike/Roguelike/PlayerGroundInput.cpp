#include "PlayerGroundInput.h"
#include "Engine.h"
#include "Message.h"
#include "Actor.h"
#include "PlayerGodInput.h"

PlayerGroundInput::PlayerGroundInput(tinyxml2::XMLElement * xmlElement)
	: IInputComponent(xmlElement),
	swapCooldown(5.f),
	forceSwapTimer(20.f),
	abilityOneTimer(2.5f),
	abilityTwoTimer(4.f),
	castingSpellTimer(0.6f),
	goodPlayerPlaying(true),
	godActorInput(nullptr),
	facing(FACING_RIGHT)
{
	goodGodSpells.emplace_back("heal");
	goodGodSpells.emplace_back("freeze");
	goodHumanSpells.emplace_back("gloryThrow");
	goodHumanSpells.emplace_back("shield");

	badGodSpells.emplace_back("meteor");
	badGodSpells.emplace_back("groundSpike");
	badHumanSpells.emplace_back("flameBlast");
	badHumanSpells.emplace_back("selfDestruct");
	/*if (!controlInit)
	{*/
		ControllerInput input;
		for (int i = 0; i < 2; i++)
		{
			std::cout << "Added controller " << i << ".\n";
			this->inputs.emplace_back(input);
		}

}

PlayerGroundInput::~PlayerGroundInput()
{
	parentEngine->getEventHandler()->removeSubscriber(this);
}

void PlayerGroundInput::update(float deltatime)
{
	if (!subscribedToEvents)
	{

		forceSwapTimer.restart();
		swapCooldown.restart();
		parentEngine->getGuiHandler()->setBarMaxValue("forceSwapTimer", 20.f);
		parentEngine->getGuiHandler()->setBarMaxValue("humanAbillity1", 2.5f);
		parentEngine->getGuiHandler()->setBarMaxValue("humanAbillity2", 4.f);

		parentEngine->getGuiHandler()->setBarMaxValue("healthBar", dynamic_cast<CombatComponent*>(parentActor->getComponent("CombatComponent"))->getHealth());

		parentEngine->getEventHandler()->addSubscriber(this);
		subscribedToEvents = true;
		parentActorFactory = parentEngine->getActorFactory();
		godActor = parentActorFactory->createActor("res/actors/godPlayer.xml", glm::vec3(parentActor->getPosition().x + 8, parentActor->getPosition().y, 0.f));
		godActorInput = dynamic_cast<PlayerGodInput*>(godActor->getComponent("InputComponent"));
		godActorInput->setGroundActor(parentActor);
	}
	
	if (parentActor == parentEngine->getPlayer())
	{
		int tempLife = dynamic_cast<CombatComponent*>(parentActor->getComponent("CombatComponent"))->getHealth();
		parentEngine->getGuiHandler()->setBarValue("healthBar", tempLife);
		if (tempLife <= 0)
		{
			parentEngine->setGameState(STATE_GAMEOVER);
		}
	}

	float tempTimer = 20 - forceSwapTimer.getTicks();
	parentEngine->getGuiHandler()->setBarValue("forceSwapTimer", tempTimer);

	tempTimer = 2.5 - abilityOneTimer.getTicks();

	parentEngine->getGuiHandler()->setBarValue("humanAbillity1", tempTimer);

	tempTimer = 4 - abilityTwoTimer.getTicks();

	parentEngine->getGuiHandler()->setBarValue("humanAbillity2", tempTimer);
		
	if (swapCooldown.hasEnded())
	{
		parentEngine->getGuiHandler()->setBarTexture("forceSwapTimer", "res/gui/textures/DurationBarGood.png");
	}
	else
	{
		parentEngine->getGuiHandler()->setBarTexture("forceSwapTimer", "res/gui/textures/DurationBarBad.png");
	}
	bool swapButtonDown = false;
	if (goodPlayerPlaying && eDown || goodPlayerPlaying & this->inputs[0].upButtonDown)
	{
		swapButtonDown = true;
	}
	else if (!goodPlayerPlaying && eDown || !goodPlayerPlaying & this->inputs[1].upButtonDown)
	{
		swapButtonDown = true;
	}
	if ((swapCooldown.hasEnded() && swapButtonDown) || forceSwapTimer.hasEnded())
	{
		swapPlayers();
	}
	
	float speed = 8.f;
	glm::vec2 desiredSpeed = { 0,0 };
	Texture* texture;

	if (frozenDuration.hasEnded())
	{
		frozen = false;
		if (goodPlayerPlaying)
		{
			Texture* newTexture = parentEngine->getTextureHandler()->loadTexture("res/textures/characters/GroundPlayer/PwsG_01.png");
			dynamic_cast<GraphicsComponent*>(parentActor->getComponent("GraphicsComponent"))->setFrames({ 5,5 });
			dynamic_cast<GraphicsComponent*>(parentActor->getComponent("GraphicsComponent"))->setTexture(newTexture);
		}
		else
		{
			Texture* newTexture = parentEngine->getTextureHandler()->loadTexture("res/textures/characters/GroundPlayer/PwsE_01.png");
			dynamic_cast<GraphicsComponent*>(parentActor->getComponent("GraphicsComponent"))->setFrames({ 5,5 });
			dynamic_cast<GraphicsComponent*>(parentActor->getComponent("GraphicsComponent"))->setTexture(newTexture);
		}
	
	}

	if (!frozen)
	{
		if (goodPlayerPlaying)
		{
			texture = parentEngine->getTextureHandler()->loadTexture("res/textures/characters/GroundPlayer/PisG_01.png");
			dynamic_cast<GraphicsComponent*>(this->parentEngine->getPlayer()->getComponent("GraphicsComponent"))->setTexture(texture);
			dynamic_cast<GraphicsComponent*>(this->parentEngine->getPlayer()->getComponent("GraphicsComponent"))->setFrames({ 5.f, 5.f });
			if (aDown)
			{
				desiredSpeed.x -= speed;
				texture = parentEngine->getTextureHandler()->loadTexture("res/textures/characters/GroundPlayer/PwsG_01.png");
				dynamic_cast<GraphicsComponent*>(this->parentEngine->getPlayer()->getComponent("GraphicsComponent"))->setTexture(texture);
				dynamic_cast<GraphicsComponent*>(this->parentEngine->getPlayer()->getComponent("GraphicsComponent"))->setFrames({ 5.f, 5.f });
			}

			if (dDown)
			{
				desiredSpeed.x += speed;
				texture = parentEngine->getTextureHandler()->loadTexture("res/textures/characters/GroundPlayer/PwsG_01.png");
				dynamic_cast<GraphicsComponent*>(this->parentEngine->getPlayer()->getComponent("GraphicsComponent"))->setTexture(texture);
				dynamic_cast<GraphicsComponent*>(this->parentEngine->getPlayer()->getComponent("GraphicsComponent"))->setFrames({ 5.f, 5.f });
			}

			if (this->stickID != -1)
			{
				if (inputs[0].axis.x != 0)
				{
					texture = parentEngine->getTextureHandler()->loadTexture("res/textures/characters/GroundPlayer/PwsG_01.png");
					dynamic_cast<GraphicsComponent*>(this->parentEngine->getPlayer()->getComponent("GraphicsComponent"))->setTexture(texture);
					dynamic_cast<GraphicsComponent*>(this->parentEngine->getPlayer()->getComponent("GraphicsComponent"))->setFrames({ 5.f, 5.f });
				}

				desiredSpeed.x = (inputs[0].axis.x / 32767.f) * speed;
			}
		}

		if (!goodPlayerPlaying)
		{
			texture = parentEngine->getTextureHandler()->loadTexture("res/textures/characters/GroundPlayer/PisE_01.png");
			dynamic_cast<GraphicsComponent*>(this->parentEngine->getPlayer()->getComponent("GraphicsComponent"))->setTexture(texture);
			dynamic_cast<GraphicsComponent*>(this->parentEngine->getPlayer()->getComponent("GraphicsComponent"))->setFrames({ 5.f, 5.f });

			if (leftDown)
			{
				desiredSpeed.x -= speed;
				texture = parentEngine->getTextureHandler()->loadTexture("res/textures/characters/GroundPlayer/PwsE_01.png");
				dynamic_cast<GraphicsComponent*>(this->parentEngine->getPlayer()->getComponent("GraphicsComponent"))->setTexture(texture);
				dynamic_cast<GraphicsComponent*>(this->parentEngine->getPlayer()->getComponent("GraphicsComponent"))->setFrames({ 5.f, 5.f });
			}

			if (rightDown)
			{
				desiredSpeed.x += speed;
				texture = parentEngine->getTextureHandler()->loadTexture("res/textures/characters/GroundPlayer/PwsE_01.png");
				dynamic_cast<GraphicsComponent*>(this->parentEngine->getPlayer()->getComponent("GraphicsComponent"))->setTexture(texture);
				dynamic_cast<GraphicsComponent*>(this->parentEngine->getPlayer()->getComponent("GraphicsComponent"))->setFrames({ 5.f, 5.f });
			}

			if (this->stickID != -1)
			{
				if (inputs[1].axis.x != 0)
				{
					texture = parentEngine->getTextureHandler()->loadTexture("res/textures/characters/GroundPlayer/PwsE_01.png");
					dynamic_cast<GraphicsComponent*>(this->parentEngine->getPlayer()->getComponent("GraphicsComponent"))->setTexture(texture);
					dynamic_cast<GraphicsComponent*>(this->parentEngine->getPlayer()->getComponent("GraphicsComponent"))->setFrames({ 5.f, 5.f });
				}
				desiredSpeed.x = (inputs[1].axis.x / 32767.f) * speed;
			}
		}
		checkAbilityCast();
		if (spellToCast != CAST_NOSPELL)
		{
			glm::vec2 newVelocity = dynamic_cast<PhysicsComponent*>(parentActor->getComponent("PhysicsComponent"))->getVelocity();
			dynamic_cast<PhysicsComponent*>(parentActor->getComponent("PhysicsComponent"))->setVelocity({ 0.f, newVelocity.y, 0.f });
			if (goodPlayerPlaying)
			{
				if (spellToCast == CAST_SPELLONE)
				{
					texture = parentEngine->getTextureHandler()->loadTexture("res/textures/characters/GroundPlayer/PGsG_01.png");
					dynamic_cast<GraphicsComponent*>(this->parentEngine->getPlayer()->getComponent("GraphicsComponent"))->setFrames({ 5.f, 3.f });
				}
				else
				{
					texture = parentEngine->getTextureHandler()->loadTexture("res/textures/characters/GroundPlayer/PssG_01.png");
					dynamic_cast<GraphicsComponent*>(this->parentEngine->getPlayer()->getComponent("GraphicsComponent"))->setFrames({ 5.f, 3.f });
				}
			}
			else
			{
				texture = parentEngine->getTextureHandler()->loadTexture("res/textures/characters/GroundPlayer/PssE_01.png");
			}
			dynamic_cast<GraphicsComponent*>(this->parentEngine->getPlayer()->getComponent("GraphicsComponent"))->setTexture(texture);
			//JONAS FUNKSJON HER! TODO TODO:
			dynamic_cast<GraphicsComponent*>(this->parentEngine->getPlayer()->getComponent("GraphicsComponent"))->setFrames({ 5.f, 3.f });
		}
		if (spellToCast == CAST_NOSPELL)
		{
			if (desiredSpeed.x > 0)
			{
				facing = FACING_RIGHT;
			}
			if (desiredSpeed.x < 0)
			{
				facing = FACING_LEFT;
			}

			dynamic_cast<PhysicsComponent*>(parentActor->getComponent("PhysicsComponent"))->applySpeed(desiredSpeed);
			glm::vec2 velocity = dynamic_cast<PhysicsComponent*>(parentActor->getComponent("PhysicsComponent"))->getVelocity();
			godActor->movePosition({ velocity.x ,0, 0 }, deltatime);
			if ((spaceDown && goodPlayerPlaying) || (numZeroDown && !goodPlayerPlaying))
			{
				dynamic_cast<PhysicsComponent*>(parentActor->getComponent("PhysicsComponent"))->applySpeed(desiredSpeed);
			}
			bool jumpButtonDown = false;
			if (spaceDown && goodPlayerPlaying || goodPlayerPlaying && inputs[0].downButtonDown)
			{
				jumpButtonDown = true;
			}
			else if (!goodPlayerPlaying && numZeroDown || !goodPlayerPlaying && inputs[1].downButtonDown)
			{
				jumpButtonDown = true;
			}

			if (jumpButtonDown)
			{
				dynamic_cast<PhysicsComponent*>(parentActor->getComponent("PhysicsComponent"))->applyLinearImpulse({ 0.f, 15.f });
			}
		}
	}
}

void PlayerGroundInput::receiveMessage(const Message & message)
{
	if (message.getSenderType() == typeid(EventHandler))
	{
		if (static_cast<const char*>(message.getVariable(0)) == "keyDown")
		{
			int number = message.getVariable(1);
			switch (number)
			{
				//good player
			case SDLK_a:
				aDown = true;
				break;
			case SDLK_d:
				dDown = true;
				break;
			case SDLK_e:
				eDown = true;
				break;
			case SDLK_SPACE:
				spaceDown = true;
				break;
			case SDLK_1:
				oneDown = true;
				break;
			case SDLK_2:
				twoDown = true;
				break;

				//evil player
			case SDLK_LEFT:
				leftDown = true;
				break;
			case SDLK_RIGHT:
				rightDown = true;
				break;
			case SDLK_KP_0:
				numZeroDown = true;
				break;
			case SDLK_KP_1:
				numOneDown = true;
				break;
			case SDLK_KP_7:
				numSevenDown = true;
				break;
			case SDLK_KP_8:
				numEightDown = true;
				break;
			default:
				break;
			}
			return;
		}

		if (static_cast<const char*>(message.getVariable(0)) == "keyUp")
		{
			int number = message.getVariable(1);
			switch (number)
			{
			case SDLK_a:
				aDown = false;
				break;
			case SDLK_d:
				dDown = false;
				break;
			case SDLK_e:
				eDown = false;
				break;
			case SDLK_SPACE:
				spaceDown = false;
				break;
			case SDLK_1:
				oneDown = false;
			case SDLK_2:
				twoDown = false;
				//evil player
			case SDLK_LEFT:
				leftDown = false;
				break;
			case SDLK_RIGHT:
				rightDown = false;
				break;
			case SDLK_KP_0:
				numZeroDown = false;
				break;
			case SDLK_KP_1:
				numOneDown = false;
				break;
			case SDLK_KP_7:
				numSevenDown = false;
				break;
			case SDLK_KP_8:
				numEightDown = false;
				break;
			default:
				break;
			}
			return;
		}

		if (static_cast<const char*>(message.getVariable(0)) == "joystickNoMotion")
		{
			SDL_JoyAxisEvent axisEvent = message.getVariable(1);
			if (axisEvent.which == 0)
			{
				std::cout << "one axis null\n";
				inputs[0].axis = { 0.f, 0.f };
			}

			if (axisEvent.which == 1)
			{
				std::cout << "two axis null\n";
				inputs[1].axis = { 0.f, 0.f };
			}
		}

		if (static_cast<const char*>(message.getVariable(0)) == "joystickMotion")
		{
			SDL_JoyAxisEvent axisEvent = message.getVariable(1);
			if (axisEvent.which == 0)	//player one
			{
				switch (axisEvent.axis)
				{
				case 0:
					inputs[0].axis.x = axisEvent.value;
					break;
				case 1:
					inputs[0].axis.y = axisEvent.value;
					break;
				}
			}
			if (axisEvent.which == 1)	//player two
			{
				switch (axisEvent.axis)
				{
				case 0:
					inputs[1].axis.x = axisEvent.value;
					break;
				case 1:
					inputs[1].axis.y = axisEvent.value;
					break;
				}
			}
			this->stickID = axisEvent.which;
		}

		if (static_cast<const char*>(message.getVariable(0)) == "joystickButtonUp")
		{
			SDL_JoyButtonEvent buttonUp = message.getVariable(1);
			if (buttonUp.which == 0)
			{
				switch (buttonUp.button)
				{
				case 0: //dpad up

					break;
				case 1: //dpad left

					break;
				case 2: //dpad down
					break;
				case 3: //dpad right

					break;
				case 4: //start

					break;
				case 5: //back or select

					break;
				case 6: //left tumbstick pressed

					break;
				case 7: //right tumbstick pressed

					break;
				case 8: //left bumper or l1
					inputs[0].leftBumperDown = false;
					break;
				case 9: //right bumper or r1
					inputs[0].rightBumperDown = false;
					break;
				case 10: //a or x
					inputs[0].downButtonDown = false;
					break;
				case 11: //b or circle

					break;
				case 12: //x or square

					break;
				case 13: //y or triangle
					inputs[0].upButtonDown = false;
					break;
				}
			}
			if (buttonUp.which == 1)
			{
				switch (buttonUp.button)
				{
				case 0: //dpad up

					break;
				case 1: //dpad left

					break;
				case 2: //dpad down

					break;
				case 3: //dpad right

					break;
				case 4: //start

					break;
				case 5: //back or select

					break;
				case 6: //left tumbstick pressed

					break;
				case 7: //right tumbstick pressed

					break;
				case 8: //left bumper or l1
					inputs[1].leftBumperDown = false;
					break;
				case 9: //right bumper or r1
					inputs[1].rightBumperDown = false;
					break;
				case 10: //a or x
					inputs[1].downButtonDown = false;
					break;
				case 11: //b or circle

					break;
				case 12: //x or square

					break;
				case 13: //y or triangle
					inputs[1].upButtonDown = false;
					break;
				}
			}

		}

		if (static_cast<const char*>(message.getVariable(0)) == "joystickButtonDown")
		{
			SDL_JoyButtonEvent buttonDown = message.getVariable(1);
			if (buttonDown.which == 0)
			{
				switch (buttonDown.button)
				{
				case 0: //dpad up

					break;
				case 1: //dpad left

					break;
				case 2: //dpad down

					break;
				case 3: //dpad right

					break;
				case 4: //start
					parentEngine->setGameState(STATE_MAINMENU);
					break;
				case 5: //back or select

					break;
				case 6: //left tumbstick pressed

					break;
				case 7: //right tumbstick pressed

					break;
				case 8: //left bumper or l1
					inputs[0].leftBumperDown = true;
					break;
				case 9: //right bumper or r1
					inputs[0].rightBumperDown = true;
					break;
				case 10: //a or x
					inputs[0].downButtonDown = true;
					break;
				case 11: //b or circle

					break;
				case 12: //x or square

					break;
				case 13: //y or triangle
					inputs[0].upButtonDown = true;
					break;
				}
			}
			if (buttonDown.which == 1)
			{
				switch (buttonDown.button)
				{
				case 0: //dpad up

					break;
				case 1: //dpad left

					break;
				case 2: //dpad down

					break;
				case 3: //dpad right

					break;
				case 4: //start
					parentEngine->setGameState(STATE_MAINMENU);
					break;
				case 5: //back or select

					break;
				case 6: //left tumbstick pressed

					break;
				case 7: //right tumbstick pressed

					break;
				case 8: //left bumper or l1
					inputs[1].leftBumperDown = true;
					break;
				case 9: //right bumper or r1
					inputs[1].rightBumperDown = true;
					break;
				case 10: //a or x
					inputs[1].downButtonDown = true;
					break;
				case 11: //b or circle

					break;
				case 12: //x or square

					break;
				case 13: //y or triangle
					inputs[1].upButtonDown = true;
					break;
				}
			}

		}

		if (static_cast<const char*>(message.getVariable(0)) == "joystickHat")
		{
			SDL_JoyHatEvent hat = message.getVariable(1);
		}
	}
}

void PlayerGroundInput::checkAbilityCast()
{
	if (castingSpellTimer.hasEnded() && (spellToCast == CAST_NOSPELL))
	{
		if (goodPlayerPlaying)
		{
			if (oneDown && abilityOneTimer.hasEnded() || inputs[0].leftBumperDown && abilityOneTimer.hasEnded())
			{
				spellToCast = CAST_SPELLONE;
				castingSpellTimer.restart();
			}
			if (twoDown && abilityTwoTimer.hasEnded() || inputs[0].rightBumperDown && abilityTwoTimer.hasEnded())
			{
				spellToCast = CAST_SPELLTWO;
				castingSpellTimer.restart();
			}
		}

		if (!goodPlayerPlaying)
		{
			if (numSevenDown && abilityOneTimer.hasEnded() || inputs[1].leftBumperDown && abilityOneTimer.hasEnded())
			{
				spellToCast = CAST_SPELLONE;
				castingSpellTimer.restart();
			}
			if (numEightDown && abilityTwoTimer.hasEnded() || inputs[1].rightBumperDown && abilityTwoTimer.hasEnded())
			{
				spellToCast = CAST_SPELLTWO;
				castingSpellTimer.restart();
			}
		}
	}
	if (castingSpellTimer.hasEnded() && spellToCast != CAST_NOSPELL)
	{
		
		if (goodPlayerPlaying)
		{
			if (spellToCast == CAST_SPELLONE && abilityOneTimer.hasEnded())
			{
				castGoodAbilityOne();
				abilityOneTimer.restart();
			}
			if (spellToCast == CAST_SPELLTWO && abilityTwoTimer.hasEnded())
			{
				castGoodAbilityTwo();
				abilityTwoTimer.restart();
			}

		}

		if (!goodPlayerPlaying)
		{
			if (spellToCast == CAST_SPELLONE && abilityOneTimer.hasEnded())
			{
				castEvilAbilityOne();
				abilityOneTimer.restart();
			}
			if (spellToCast == CAST_SPELLTWO && abilityTwoTimer.hasEnded())
			{
				castEvilAbilityTwo();
				abilityTwoTimer.restart();
			}
		}
		spellToCast = CAST_NOSPELL;
	}
}

void PlayerGroundInput::setFrozen(bool status, int duration)
{
	frozenDuration.restart();
	frozen = true;
	frozenDuration = duration;
	frozenDuration.start();

	if (goodPlayerPlaying)
	{
		Texture* newTexture = parentEngine->getTextureHandler()->loadTexture("res/textures/characters/GroundPlayer/frozenGood.png");
		dynamic_cast<GraphicsComponent*>(parentActor->getComponent("GraphicsComponent"))->setFrames({ 1,1 });
		dynamic_cast<GraphicsComponent*>(parentActor->getComponent("GraphicsComponent"))->setTexture(newTexture);
	}
	else
	{
		Texture* newTexture = parentEngine->getTextureHandler()->loadTexture("res/textures/characters/GroundPlayer/frozenBad.png");
		dynamic_cast<GraphicsComponent*>(parentActor->getComponent("GraphicsComponent"))->setFrames({ 1,1 });
		dynamic_cast<GraphicsComponent*>(parentActor->getComponent("GraphicsComponent"))->setTexture(newTexture);
	}
}

void PlayerGroundInput::castGoodAbilityOne()
{
	parentActorFactory->createActor("res/actors/halo.xml", glm::vec3(parentActor->getPosition().x + 0.5f*facing, parentActor->getPosition().y, 0.5f), glm::vec3{ facing*8.f,0.f,0.f });
}

void PlayerGroundInput::castGoodAbilityTwo()
{
	parentActorFactory->createActor("res/actors/shield.xml", glm::vec3(parentActor->getPosition().x + 1.f*facing, parentActor->getPosition().y, 0.f));
}

void PlayerGroundInput::castEvilAbilityOne()
{
	parentActorFactory->createActor("res/actors/flameBlast.xml", glm::vec3(parentActor->getPosition().x + 1.5f*facing, parentActor->getPosition().y + 0.5f, 0.f));
}

void PlayerGroundInput::castEvilAbilityTwo()
{
	
	if (swapCooldown.hasEnded())
	{
		parentActorFactory->createActor("res/actors/selfdestruct.xml", glm::vec3(parentActor->getPosition().x, parentActor->getPosition().y + 2, 0.f));
		
		if (dynamic_cast<CombatComponent*>(parentActor->getComponent("CombatComponent"))->getHealth() > 40.f)
		{
			dynamic_cast<CombatComponent*>(parentActor->getComponent("CombatComponent"))->reduceHealth(40.f);
		}
		swapPlayers();
	}
	
}

void PlayerGroundInput::swapPlayers()
{
	swapCooldown.restart();
	forceSwapTimer.restart();
	goodPlayerPlaying = !goodPlayerPlaying;

	int numberOfSpells = 2;
	if (goodPlayerPlaying)
	{
		//GOD is now BAD
		Texture* newTexture = parentEngine->getTextureHandler()->loadTexture("res/textures/characters/GroundPlayer/PwsG_01.png");
		parentEngine->getGuiHandler()->setBarBackground("healthBar", "res/gui/textures/barBackgroundGood.png");
		parentEngine->getGuiHandler()->setPanelTexture("godPortrait", "res/gui/textures/badGodPortrait.png");
		parentEngine->getGuiHandler()->setTextureContainerBox("actionBarGod", "res/gui/textures/spellFrameBad.png");
		parentEngine->getGuiHandler()->setTextureContainerBox("actionBarHuman", "res/gui/textures/spellFrameGood.png");
		parentEngine->getGuiHandler()->setBarBackground("forceSwapTimer", "res/gui/textures/DurationBackgroundBad.png");

		dynamic_cast<GraphicsComponent*>(parentActor->getComponent("GraphicsComponent"))->setTexture(newTexture);

		for (int i = 0; i < numberOfSpells; i++)
		{
			parentEngine->getGuiHandler()->setTextureContainer(i, "actionBarHuman", "res/gui/textures/" + goodHumanSpells[i] + ".png");
			parentEngine->getGuiHandler()->setTextureContainer(i, "actionBarGod", "res/gui/textures/" + badGodSpells[i] + ".png");
		}
	}

	if (!goodPlayerPlaying)
	{
		//GOD is now GOOD
		Texture* newTexture = parentEngine->getTextureHandler()->loadTexture("res/textures/characters/GroundPlayer/PwsE_01.png");
		parentEngine->getGuiHandler()->setBarBackground("healthBar", "res/gui/textures/barBackgroundBad.png");
		parentEngine->getGuiHandler()->setPanelTexture("godPortrait", "res/gui/textures/goodGodPortrait.png");
		parentEngine->getGuiHandler()->setTextureContainerBox("actionBarGod", "res/gui/textures/spellFrameGood.png");
		parentEngine->getGuiHandler()->setTextureContainerBox("actionBarHuman", "res/gui/textures/spellFrameBad.png");
		parentEngine->getGuiHandler()->setBarBackground("forceSwapTimer", "res/gui/textures/DurationBackgroundGood.png");

		dynamic_cast<GraphicsComponent*>(parentActor->getComponent("GraphicsComponent"))->setTexture(newTexture);

		for (int i = 0; i < numberOfSpells; i++)
		{
			parentEngine->getGuiHandler()->setTextureContainer(i, "actionBarHuman", "res/gui/textures/" + badHumanSpells[i] + ".png");
			parentEngine->getGuiHandler()->setTextureContainer(i, "actionBarGod", "res/gui/textures/" + goodGodSpells[i] + ".png");
		}
	}
	godActorInput->swapCharacters();
}
