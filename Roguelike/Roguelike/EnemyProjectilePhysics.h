#pragma once
#include "PhysicsComponent.h"

class Actor;
class CombatComponent;

class EnemyProjectilePhysics final : public PhysicsComponent
{
public:
	EnemyProjectilePhysics(tinyxml2::XMLElement *xmlElement);
	void onCollision(int fixtureUserData, Actor* collidedActor);
	//void update(float deltaTime);
	//void onDeath() override;
	//void onSpawn() override;
private:
	CombatComponent* combatComponent;
};