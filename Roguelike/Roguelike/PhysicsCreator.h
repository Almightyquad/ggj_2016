#pragma once
#include "IPhysicsComponent.h"
#include <memory>

class PhysicsFactory;

class PhysicsCreator
{
public:
	PhysicsCreator(const std::string& classname, PhysicsFactory* physicsFactory);
	virtual std::unique_ptr<IPhysicsComponent> create(tinyxml2::XMLElement* xmlElement) = 0;

};

template <class T>
class PhysicsCreatorImplementation : public PhysicsCreator
{
public:
	PhysicsCreatorImplementation(const std::string& classname, PhysicsFactory* physicsFactory) : PhysicsCreator(classname, physicsFactory) {};
	virtual std::unique_ptr<IPhysicsComponent> create(tinyxml2::XMLElement* xmlElement) override
	{
		return std::make_unique<T>(xmlElement);
	}
};