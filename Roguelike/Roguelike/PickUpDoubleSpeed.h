#pragma once
#include "IPickUpComponent.h"

class PickUpDoubleSpeed final : public IPickUpComponent
{
public:
	PickUpDoubleSpeed(tinyxml2::XMLElement *xmlElement) : IPickUpComponent(xmlElement)
	{ 
	};
	virtual void onToss() override;
	virtual void onActivation() override;
	virtual void onDeactivation() override;
private:
};