#pragma once
#include "PhysicsComponent.h"

class EnemyPhysics final : public PhysicsComponent
{
public:
	EnemyPhysics(tinyxml2::XMLElement *xmlElement);
	void onCollision(int fixtureUserData, Actor* collidedActor);
	void update(float deltaTime);
	void onDeath() override;
	void onSpawn() override;
private:
	bool activated;
};