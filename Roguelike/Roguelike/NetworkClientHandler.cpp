#include "NetworkClientHandler.h"
#include "Engine.h"
#include "IInputComponent.h"

NetworkClientHandler::NetworkClientHandler()
{
}

NetworkClientHandler::~NetworkClientHandler()
{
	SDLNet_TCP_Close(socket);
	SDLNet_Quit();
}

void NetworkClientHandler::init()
{
	if (SDLNet_Init() < 0)
	{
		printf("SDLNet_Init: %s\n", SDLNet_GetError());
	}

	if (SDLNet_ResolveHost(&serverIP, "127.0.0.1", 2000))
	{
		printf("SDLNet_ResolveHost: %s\n", SDLNet_GetError());
	}
	while (!socket)
	{
		if (!(socket = SDLNet_TCP_Open(&serverIP)))
		{
			printf("\nSDLNet_TCP_Open: %s\n", SDLNet_GetError());
		}
		else
		{


			//receive random seed for world generation
			unsigned int randomSeed;
			if ((SDLNet_TCP_Recv(socket, &randomSeed, sizeof(unsigned int))) <= 0)
			{
				printf("SDLNet_TCP_Recv game start error: %s\n", SDLNet_GetError());
			}
			else
			{
				parentEngine->setSeed(randomSeed);
				parentEngine->setGameState(STATE_RUNNING);
				//if (networkDebug)
				{
					printf("succeeded receiving packet. Seed: %u \n", randomSeed);
				}
			}
		
			//send selected character
			
			int selectedTool = parentEngine->getSelectedTool();


			if (SDLNet_TCP_Send(socket, &selectedTool, sizeof(int)) < sizeof(int))
			{
				printf("SDLNet_TCP_Send: %s\n", SDLNet_GetError());
			}
			else if (networkDebug)
			{
				printf("succeeded sending actorId from client\n");
			}
			//allocates memory for socketset and adds socket to the set. only sockets in a set can be checked for readyness
			socketSet = SDLNet_AllocSocketSet(1);
			SDLNet_TCP_AddSocket(socketSet, socket);

			//receive actorID based on selected character
			if (SDLNet_TCP_Recv(socket, &playerActorID, sizeof(actorID)) <= 0)
			{
				printf("SDLNet_TCP_Send actorID: %s\n", SDLNet_GetError());
			}
			else if (networkDebug)
			{
				printf("succeeded receiving -player actorID- packet \n");
			}

		}



	}

	
}

void NetworkClientHandler::sendPacket(int gameState)
{
	if (networkDebug)
	{
		printf("starting to send packet from client\n");
	}
	
	switch (gameState)
	{
	case STATE_JOINING:
		sendJoining();
		break;
	case STATE_RUNNING:
		sendRunning();
		break;
	default:
		printf("invalid gamestate for network play");
		break;
	}
}


void NetworkClientHandler::receivePacket(int gameState)
{

	if (networkDebug)
	{
		printf("starting to receive packet on client\n");
	}
	
	switch (gameState)
	{
	case STATE_JOINING:
		receiveJoining();
		break;
	case STATE_RUNNING:
		receiveRunning();
		
	//	parentEngine->getActorFactory()->loadMovingObjects(actorFromServer);
		break;
	default:
		printf("invalid gamestate for network play");
		break;
	}
}


void NetworkClientHandler::receiveActorStructure()
{
	if ((SDLNet_TCP_Recv(socket, &numberOfHostActors, sizeof(int))) <= 0)
	{
		printf("SDLNet_TCP_Recv1: %s\n", SDLNet_GetError());
	}
	else
	{
		if (networkDebug)
		{
			printf("succeeded receiving packet1\n");
		}
	}

	for (int i = 0; i < numberOfHostActors; i++)
	{
		if (SDLNet_TCP_Recv(socket, &actorFromServer[i], sizeof(NetworkActorData)) <= 0)
		{
			printf("SDLNet_TCP_Recv2: %s\n", SDLNet_GetError());
		}
		else
		{
			actorID receivedActorId;
			unpackActorId(&receivedActorId, actorFromServer[i]);

			std::string category = actorFromServer[i].category;
			category = category.substr(0, actorFromServer[i].categoryLength);

			Actor* actorToSync = parentEngine->getActorFactory()->getActor(receivedActorId);
			if (actorToSync == nullptr)
			{
				std::string xmlPath;
				if (category != "")
				{
					xmlPath = "res/actors/" + category + "/" + receivedActorId.actorName + ".xml";
				}
				else
				{
					xmlPath = "res/actors/" + receivedActorId.actorName + ".xml";
				}
				parentEngine->getActorFactory()->insertActorFromNetwork(xmlPath, glm::vec3(actorFromServer[i].actorPosition.x, actorFromServer[i].actorPosition.y, .0f),receivedActorId , glm::vec3(actorFromServer[i].actorVelocity.x, actorFromServer[i].actorVelocity.y, .0f));
			}
			else
			{
				PhysicsComponent* actorToSyncPhysicsComponent = dynamic_cast<PhysicsComponent*>(actorToSync->getComponent("PhysicsComponent"));
				glm::vec3 oldPosition = actorToSyncPhysicsComponent->getPosition();
				glm::vec2 oldVelocity = actorToSyncPhysicsComponent->getVelocity();
				if (glm::distance(oldPosition, glm::vec3{ actorFromServer[i].actorPosition.x,actorFromServer[i].actorPosition.y,0 }) > 0.1f)
				{
					actorToSyncPhysicsComponent->setPosition(glm::vec3(actorFromServer[i].actorPosition.x, actorFromServer[i].actorPosition.y, .0f));
				}
				actorToSyncPhysicsComponent->setVelocity(glm::vec3(actorFromServer[i].actorVelocity.x, actorFromServer[i].actorVelocity.y, 0.f));
			}
		}
	}
}

void NetworkClientHandler::receiveItemStructure()
{
	//if (SDLNet_TCP_Recv(socket, &numberOfitems, sizeof(int)) <= 0)
	//{
	//	printf("SDLNet_TCP_Recv2: %s\n", SDLNet_GetError());
	//}
	//else
	//{
	//	for (int i = 0; i < numberOfitems; i++)
	//	{
			//if (SDLNet_TCP_Recv(socket, &actorFromServer[i], sizeof(NetworkActorData)) <= 0)
			//{
			//	actorID receivedItemId;
			//	unpackActorId(&receivedItemId, actorFromServer[i]);

			//	Actor* itemActor = (parentEngine->getActorFactory()->getActor(receivedItemId));
			//	IPickUpComponent* itemFromServer = dynamic_cast<IPickUpComponent*>(itemActor->getComponent("PickUpComponent"));


			//	if (itemFromServer->checkSavable())
			//	{
			//		if (itemFromServer->getOwner() == nullptr)
			//		{
			//			dynamic_cast<InventoryComponent*>(parentEngine->getPlayer()->getComponent("InventoryComponent"))->addItem(itemActor);

			//			//dynamic_cast<PhysicsComponent*>(itemActor->getComponent("PhysicsComponent"))->onCollision(CATEGORY_PLAYER, parentEngine->getPlayer());
			//			//dynamic_cast<PhysicsComponent*>(parentEngine->getPlayer()->getComponent("PhysicsComponent"))->onCollision(CATEGORY_PICKUP, itemActor);
			//		}
			//	}
			//	else
			//	{
			//		itemFromServer->onDeath();
			//		itemFromServer->onPickup(parentEngine->getPlayer());
			//		parentEngine->getActorFactory()->addToDeleteQueue(itemActor);
			//	}

			//	
			//}
	//	}
	//}
}

actorID NetworkClientHandler::getPlayerActorID()
{
	return playerActorID;
}

void NetworkClientHandler::sendJoining()
{
	
}

void NetworkClientHandler::receiveJoining()
{
	receiveActorStructure();
}

void NetworkClientHandler::receiveRunning()
{
	if (SDLNet_CheckSockets(socketSet, 1) > 0)
	{
		if (SDLNet_SocketReady(socket))
		{
			receiveActorStructure();
			receiveItemStructure();
		}
	}
}

void NetworkClientHandler::unpackActorId(actorID * actorIdToUnpackTo, NetworkActorData actorData)
{
	actorIdToUnpackTo->actorId = actorData.actorIdNumber;
	actorIdToUnpackTo->actorName = actorData.actorIdName;
	actorIdToUnpackTo->actorName = actorIdToUnpackTo->actorName.substr(0, actorData.actorIdNameLength);


}


void NetworkClientHandler::sendRunning()
{
	InputValues InputToSend;
	IInputComponent* inputComponent;

	inputComponent = dynamic_cast<IInputComponent*>(parentEngine->getPlayer()->getComponent("InputComponent"));
	//InputToSend = inputComponent->getInputValues(); //fix when we fix network
	if (SDLNet_TCP_Send(socket, &InputToSend, sizeof(InputValues)) < sizeof(InputValues))
	{
		printf("SDLNet_TCP_Send: %s\n", SDLNet_GetError());
	}
	else if (networkDebug)
	{
		printf("succeeded sending input from client\n");
	}
	
}

