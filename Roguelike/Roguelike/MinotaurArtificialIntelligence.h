#pragma once
#include "IAIComponent.h"
#include "Timer.h"

class PhysicsComponent;

class MinotaurArtificialIntelligence : public IAIComponent
{
public:
	MinotaurArtificialIntelligence(tinyxml2::XMLElement *xmlElement);
	void update(float deltaTime) override;
	void setFrozen(bool status, int duration);
private:
	Actor* player;
	Timer chargeCooldown;
	Timer walkCooldown;
	Timer frozenDuration;
	PhysicsComponent* physicsComponent;
	bool frozen;
	bool charge;
	bool walk;
};