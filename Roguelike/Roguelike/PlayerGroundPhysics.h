#pragma once
#include "PhysicsComponent.h"

class PlayerGroundPhysics final : public PhysicsComponent
{
public:
	PlayerGroundPhysics(tinyxml2::XMLElement *xmlElement);
	void onCollision(int fixtureUserData, Actor* collidedActor);
	void onSpawn() override;
	void update(float deltaTime);
	void setActivated(bool flag);
	void setGravityScale(float scale);
	void onDeath() override;
	void setAngel(bool status);

	bool getAngel();
private:
	bool activated;
	bool angel = false;
	//this is important. 0 means that it is created, but not activated by right click (using copy tool). 
	//1 means that it is created AND activated, but NOT checked if it is spawned inside a unspawnable zone!
	//2 means that is is created, activated, and checked if it spawned inside a unspawnable zone, so it is free to do whatever.
	int spawnphase;
};