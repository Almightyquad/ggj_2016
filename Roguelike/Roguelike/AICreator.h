#pragma once
#include "IAIComponent.h"
#include <memory>

class AIFactory;

class AICreator
{
public:
	AICreator(const std::string& classname, AIFactory* aIFactory);
	virtual std::unique_ptr<IAIComponent> create(tinyxml2::XMLElement* xmlElement) = 0;
};

template <class T>
class AICreatorImplementation : public AICreator
{
public:
	AICreatorImplementation(const std::string& classname, AIFactory* aIFactory) : AICreator(classname, aIFactory) {};
	virtual std::unique_ptr<IAIComponent> create(tinyxml2::XMLElement* xmlElement) override
	{
		return std::make_unique<T>(xmlElement);
	}
};