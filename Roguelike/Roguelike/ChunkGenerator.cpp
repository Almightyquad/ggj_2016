#include "ChunkGenerator.h"



ChunkGenerator::ChunkGenerator(int chunkSizeX, int chunkSizeY)
{
	this->chunkSizeX = chunkSizeX;
	this->chunkSizeY = chunkSizeY;
}


ChunkGenerator::~ChunkGenerator()
{
}




void ChunkGenerator::addElement(std::string name, int id, std::string layer)
{
	checkElementMapForKey(foreground, layer, id, name, "foreground");
	checkElementMapForKey(middleground, layer, id, name, "middleground");
	checkElementMapForKey(background, layer, id, name, "background");
}

void ChunkGenerator::addPrefab(std::vector<std::vector<int>> prefab)
{
	prefabVec.emplace_back(prefab);
}

//Load the boat <- not a helpful comment
bool ChunkGenerator::loadPrefab(std::string filepath)
{
	std::ifstream in(filepath);
	std::string tempReadStr;
	std::string tempStr = "";
	std::vector<int> tempIntVec;
	std::vector<std::vector<int>> tempPrefabVec;
	while (std::getline(in, tempReadStr))
	{
		for (int i = 0; i < tempReadStr.size(); i++)
		{
			if (tempReadStr.at(i) != ',')
			{
				tempStr += tempReadStr.at(i);
			}
			if (tempReadStr.at(i) == ',')
			{
				tempIntVec.emplace_back(std::stoi(tempStr));
				tempStr.clear();
			}
		}
		tempIntVec.emplace_back(std::stoi(tempStr));
		tempStr.clear();
		tempPrefabVec.emplace_back(tempIntVec);
		tempIntVec.clear();
	}
	std::reverse(tempPrefabVec.begin(), tempPrefabVec.end());
	prefabVec.emplace_back(tempPrefabVec);
	
	if (tempPrefabVec.size() > 0)
	{
		return true;
	}
	return false;
}

std::vector<std::vector<int>> ChunkGenerator::generateChunk()
{
	std::vector<std::vector<int>> tempIntVecChunk;
	std::vector<int> tempIntVec;
	for (int i = 0; i < chunkSizeX; i++)
	{
		tempIntVec.emplace_back(0);
	}
	//filling the chunk with zeros with the exception of the bottom 2 layers
	for (int i = 0; i < chunkSizeY-2; i++)
	{
		tempIntVecChunk.emplace_back(tempIntVec);
	}
	tempIntVec.clear();
	//adding layer above ground
	for (int i = 0; i < chunkSizeX; i++)
	{
		tempIntVec.emplace_back(-1);
	}
	tempIntVecChunk.emplace_back(tempIntVec);
	tempIntVec.clear();
	//adding ground layer
	for (int i = 0; i < chunkSizeX; i++)
	{
		tempIntVec.emplace_back(3);
	}
	tempIntVecChunk.emplace_back(tempIntVec);
	tempIntVec.clear();

	std::vector<int> tempVecPosX;
	std::vector<int> tempVecPosY;

	//This chunk of code is really sketchy
	findIdsInChunk(tempIntVecChunk, -1, tempVecPosX, tempVecPosY);
	
	int tempIntPrefabActualXPos = -1;
	int	randPrefabArrayLoc = 0;
	int tempRand = rand() % 100;
	if (tempRand < 50)
	{
		tempIntPrefabActualXPos = checkPrefabSpace(tempVecPosX, tempVecPosY, randPrefabArrayLoc);
	}

	
	if (tempIntPrefabActualXPos > -1)
	{
		for (int i = 0; i < prefabVec[randPrefabArrayLoc].size(); i++)
		{
			for (int j = 0; j < prefabVec[randPrefabArrayLoc][0].size(); j++)
			{
				//Dont worry, this one confuses me aswell.
				tempIntVecChunk[tempVecPosY[0] - i][tempIntPrefabActualXPos + j] = prefabVec[randPrefabArrayLoc][i][j];
			}
		}
	}
		

	//Clear the -1 from the vector
	for (int i = 0; i < chunkSizeY; i++)
	{
		for (int j = 0; j < chunkSizeX; j++)
		{
			if (tempIntVecChunk[i][j] == -1)
			{
				tempIntVecChunk[i][j] = 0;
			}
		}
	}
	//Debug chunk
	/*
	for (int i = 0; i < chunkSizeY; i++)
	{
		for (int j = 0; j < chunkSizeX; j++)
		{
			std::cout << tempIntVecChunk[i][j] << " ";
		}
		std::cout << std::endl;
	}*/

	return tempIntVecChunk;

}

std::unordered_map<int, std::string> ChunkGenerator::getElements(std::string layer)
{
	if (layer == "background")
	{
		return background;
	}
	if (layer == "middleground")
	{
		return middleground;
	}
	if (layer == "foreground")
	{
		return foreground;
	}
}

void ChunkGenerator::findIdsInChunk(std::vector<std::vector<int>> chunkVec, int idToFind, std::vector<int> & vecPosX, std::vector<int> & vecPosY)
{
	for (int i = 0; i < chunkSizeY; i++)
	{
		for (int j = 0; j < chunkSizeX; j++)
		{
			if (chunkVec[i][j] == idToFind)
			{
				vecPosY.emplace_back(i);
				vecPosX.emplace_back(j);
			}
		}
	}
}

//This code is never fucking going to work. So check here first.
int ChunkGenerator::checkPrefabSpace(std::vector<int> vecPosX, std::vector<int> vecPosY, int & randPrefab)
{
	int runs = 0;
	int randomLoc;
	if (!prefabVec.empty())
	{
		int tempRandPrefab = rand() % static_cast<int>(prefabVec.size());
		std::vector<std::vector<int>> tempPrefab;
		while (runs != 10)
		{
			randomLoc = rand() % static_cast<int>(vecPosX.size() - 1);
			if (prefabVec.size() != 0)
			{
				tempPrefab = prefabVec[tempRandPrefab];
				if (prefabVec[tempRandPrefab][0].size() + randomLoc < static_cast<int>(vecPosX.size()))
				{
					randPrefab = tempRandPrefab;
					return randomLoc;
				}
			}
			runs++;
		}
	}
	return -1;
}

void ChunkGenerator::checkElementMapForKey(std::unordered_map<int, std::string>& elementMap, std::string layer, int id, std::string name, std::string desiredLayer)
{
	if (layer == desiredLayer)
	{
		if (elementMap.find(id) == elementMap.end())
		{
			elementMap.insert(std::make_pair(id, name));
		}
		else
		{
			std::cout << "\nKeyID: " << id << " already exists, it is mapped to " << elementMap[id] << "\n";
		}
	}
}