#pragma once
#include <vector>
#include <string>
#include <glm\glm.hpp>
#include "Handler.h"

class Engine;

class RandomSpawn : public Handler
{
public:
	RandomSpawn();
	~RandomSpawn();
	bool spawnRandomEnemy(std::string actorToSpawn, glm::vec2 spawnPos);
	bool spawnRandomItem(std::string actorToSpawn);
};

