#pragma once
#include "IAIComponent.h"

class AIEsmeralda final : public IAIComponent
{
public:
	AIEsmeralda(tinyxml2::XMLElement *xmlElement)
		: IAIComponent(xmlElement)
	{};
	~AIEsmeralda();
	void update(float deltaTime) override;
private:

	//void(ActorFactory::*createActor) (const std::string xmlPath, const glm::vec3 position, const glm::vec3 velocity);
};
