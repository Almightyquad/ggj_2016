#pragma once
#include "IInputComponent.h"
//#include "Message.h"

class InputPlayer final : public IInputComponent
{
public:
	InputPlayer(tinyxml2::XMLElement *xmlElement);
	virtual void update(float deltatime) override;
	virtual void receiveMessage(const Message &message) override;
	virtual void receiveNetworkInput(InputValues newInput) override;
	InputValues getInputValues();
private:
	bool flipInventory;
	bool inventoryOpen;
	bool inputChanged = false;
	InputValues input;
};