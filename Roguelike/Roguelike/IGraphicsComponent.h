#pragma once
#include <string>
#include "ActorComponent.h"
#include "tinyxml2-master\tinyxml2.h"
#include "Animation.h"

/**
* @class	GraphicsComponent
*
* @brief	The graphics component interface. everything that needs to be drawn needs this
* @details	Holds pointers to all data needed to draw the actor.
* 			All pointers are owned by their respective handlers
*/
class IGraphicsComponent : public ActorComponent
{
public:
	IGraphicsComponent(tinyxml2::XMLElement *xmlElement = nullptr) 
		: ActorComponent(xmlElement),
		xFrames(1),
		yFrames(1) {};
	virtual ~IGraphicsComponent(){};
	/*virtual void loadTexture(std::string filepath) = 0;
	virtual Texture getTexture(std::string textureID) = 0;
	virtual std::string getComponentName() = 0;*/
protected:
	bool animated = false;
	bool flipped = false;
	glm::vec2 tilePosition;
	int xFrames;
	int yFrames;
	std::string texturePath;
	std::string meshPath;
	std::string category;
	Animation anim;
};