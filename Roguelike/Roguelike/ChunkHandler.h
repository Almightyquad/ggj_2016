#pragma once
#include <vector>
#include <string>
#include "ChunkGenerator.h"
#include "Handler.h"
#include "Actor.h"
#include "ActorFactory.h"
#include "common.h"
#include "PhysicsComponent.h"

class Engine;

class ChunkHandler : public Handler
{
public:
	ChunkHandler();
	~ChunkHandler();
	void setActorFactory(ActorFactory* actorFactory);
	void setPhysicsHandler(PhysicsHandler * physicsHandler);
	void generateChunk(int numberOfChunks);
	void loadPrefabList(std::vector<std::string> paths);
	void addElementsToList(std::vector<int> ids, std::vector<std::string> names, std::vector<std::string> layers);
	void loadChunks();
	std::vector<std::vector<Actor*>> getChunkActors();
	void deleteChunkActors();
private:
	ActorFactory * actorFactory;
	PhysicsHandler * physicsHandler;
	std::vector<std::vector<std::vector<int>>> chunks;
	ChunkGenerator * chunkGenerator;
	std::vector<std::vector<Actor*>> chunkActors;
	Actor * addActorToMap(std::string path, int x, int y, glm::vec2 positionInTileMap);
	

	int chunkNumberX;
	int chunkNumberY;
	int chunkWidth;
	int chunkHeight;

	int groundIterator;
};

