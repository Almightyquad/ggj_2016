#pragma once
#include "tinyxml2-master\tinyxml2.h"
#include <memory>
#include <glm\glm.hpp>
#include "common.h"
#include "Receiver.h"
#include "Observable.h"
class Actor;
class Engine;
/**
 * @class	ActorComponent
 *
 * @brief	The base class for all components. Can't be instantiated.
 */
class ActorComponent : public IReceiver, public IObservable
{
public:
	
	ActorComponent();
	ActorComponent(tinyxml2::XMLElement *xmlElement = nullptr);
	
	virtual ~ActorComponent();
	virtual void init(Engine* parentEngine) = 0;
	virtual void update(float deltaTime = 0) {};
	virtual void receiveMessage(const Message &message) {};
	virtual void onHit() {};
	virtual void onDeath() {};
	virtual void onSpawn() {};

	void setParent(Actor& parentPtr);
	bool isAlive();
protected:
	bool alive;
	Actor* parentActor;
	Engine* parentEngine;
	tinyxml2::XMLElement *originNode;
private:
};