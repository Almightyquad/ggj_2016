#include "MinotaurArtificialIntelligence.h"
#include "Engine.h"
#include "PhysicsComponent.h"

MinotaurArtificialIntelligence::MinotaurArtificialIntelligence(tinyxml2::XMLElement * xmlElement)
	: IAIComponent(xmlElement),
	player(nullptr),
	charge(false),
	physicsComponent(nullptr),
	chargeCooldown(3.2f),
	walkCooldown(0.5f)
{
}

void MinotaurArtificialIntelligence::update(float deltaTime)
{
	if (frozenDuration.hasEnded())
	{
		frozen = false;
		Texture* newTexture = parentEngine->getTextureHandler()->loadTexture("res/textures/characters/Minotaur/Minotaur.png");
		dynamic_cast<GraphicsComponent*>(parentActor->getComponent("GraphicsComponent"))->setTexture(newTexture);
	}
	if (!frozen)
	{
		if (player == nullptr)
		{
			player = parentEngine->getPlayer();
			physicsComponent = dynamic_cast<PhysicsComponent*>(parentActor->getComponent("PhysicsComponent"));
			chargeCooldown.start();
		}
		float distanceToPlayer = glm::length<float>(player->getPosition() - parentActor->getPosition());
		if (distanceToPlayer < 5 && chargeCooldown.hasEnded())
		{
			charge = true;
			walk = false;
			chargeCooldown.restart();
			walk = false;
			walkCooldown.restart();
		}

		if (!walk && walkCooldown.hasEnded())
		{
			walk = true;
		}

		if ((player->getPosition().x - parentActor->getPosition().x) > 0)
		{
			if (charge)
			{
				physicsComponent->applyLinearImpulse({ 4.f, 1.5f });
				charge = false;
			}
			if (walk)
			{
				physicsComponent->applySpeed({ 1.5f,0.f });
			}
		}
		else
		{
			if (charge)
			{
				physicsComponent->applyLinearImpulse({ -4.f, 1.5f });
				charge = false;
			}
			if (walk)
			{
				physicsComponent->applySpeed({ -1.5f,0.f });
			}
		}

	}
}

void MinotaurArtificialIntelligence::setFrozen(bool status, int duration)
{
	frozen = true;
	frozenDuration.setDuration(duration);
	frozenDuration.restart();
}
