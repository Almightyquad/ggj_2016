#include "EventHandler.h"
#include "Engine.h"
#include "common.h"
#include <memory>
#include "Actor.h"
#include "Message.h"

#ifdef __ANDROID__
#define SDL_MOUSEMOTION SDL_FINGERMOTION
#define SDL_MOUSEBUTTONDOWN SDL_FINGERDOWN
#define SDL_MOUSEBUTTONUP SDL_FINGERUP
#endif

EventHandler::EventHandler()
{
	consoleActive = false;
	inputActive = false;
	inputText = "";
	consoleInputText = "";
	mouseX = mouseY = 1;
	prevMouseX = prevMouseY = -1;
}

EventHandler::~EventHandler()
{

}

void EventHandler::handleEvents()
{
	SDL_Event SDLEvent;
	int inputhandling = -1;
	//switch (SDLevent);
	
	while (SDL_PollEvent(&SDLEvent))
	{
		switch (SDLEvent.type)
		{
		case SDL_QUIT:
			parentEngine->setGameState(STATE_QUITTING);
			break;
		case SDL_KEYDOWN:
			//Console doesn't work, so this pretty much does nothing
			if (SDLEvent.key.keysym.sym == SDLK_BACKQUOTE)
			{
				swapConsoleStatus();
				consoleInputText = "";
			}
			if (!consoleActive && !inputActive)
			{
				//Sends messages to InputPlayer
				this->postMessage(Message(this, "keyDown", static_cast<int>(SDLEvent.key.keysym.sym)));	
			}
			if (consoleActive)
			{
				handleConsoleTextActions(SDLEvent);
			}
			if (inputActive)
			{
				handleTextActions(SDLEvent);
			}
			break;
		case SDL_KEYUP:
			this->postMessage(Message(this, "keyUp", static_cast<int>(SDLEvent.key.keysym.sym)));
			break;
		case SDL_TEXTINPUT:
			if (inputActive)
			{
				handleInputText(SDLEvent);
			}
			else if (consoleActive)
			{
				handleConsoleInputText(SDLEvent);
			}
			break;
		case SDL_MOUSEMOTION:
			handleMouseMotion(SDLEvent);
			break;
		case SDL_MOUSEBUTTONDOWN:
			handleMouseDown(SDLEvent);
			break;
		case SDL_MOUSEBUTTONUP:
			handleMouseUp(SDLEvent);
			break;
		case SDL_MOUSEWHEEL:
			handleMouseWheel(SDLEvent);
			break;
		case SDL_WINDOWEVENT:
			handleWindowEvents(SDLEvent);
			break;
		case SDL_JOYAXISMOTION:
			handleJoystickMotion(SDLEvent);
			break;
		case SDL_JOYBUTTONDOWN:
			handleJoystickDown(SDLEvent);
			break;
		case SDL_JOYBUTTONUP:
			handleJoystickUp(SDLEvent);
			break;
		case SDL_JOYHATMOTION:


		default:
			if (SDLEvent.type == 4352)
			{
				std::cout << "We don't handle event: "
						  << "SDL_AudioDeviceAdded (4352)\n";
			}
			else if (SDLEvent.type == 770)
			{
				std::cout << "We don't handle event: "
						  << "SDL_TextEditing (770)\n";
			}
			else
			{
				std::cout << "We don't handle this type of events: " << SDLEvent.type << std::endl;
			}
			
			break;
		}	
	}
}

void EventHandler::init()
{
#ifdef __ANDROID__
	touchLocation.x = 1;
	touchLocation.y = 1;
#endif
	screenResolutionX = static_cast<int>(parentEngine->getGraphicsHandler()->getWindowResolution().x);
	screenResolutionY = static_cast<int>(parentEngine->getGraphicsHandler()->getWindowResolution().y);

	//add two gamepads
	SDL_Joystick *joystick;
	SDL_JoystickEventState(SDL_ENABLE);
	joystick = SDL_JoystickOpen(0);
	joystick = SDL_JoystickOpen(1);
}

void EventHandler::handleWindowEvents(SDL_Event & SDLEvent)
{
	switch (SDLEvent.window.event)
	{
	case SDL_WINDOWEVENT_RESIZED:
		glViewport(0, 0, SDLEvent.window.data1, SDLEvent.window.data2);
		screenResolutionX = static_cast<int>(parentEngine->getGraphicsHandler()->getWindowResolution().x);
		screenResolutionY = static_cast<int>(parentEngine->getGraphicsHandler()->getWindowResolution().y);
		break;
	default:
		break;
	}
}

void EventHandler::updateMousePosition(SDL_Event & SDLEvent)
{
	//This thing is a clusterfuck
	//TODO: Fix this clusterfuck
	//transform to OPENGL space
#ifdef __ANDROID__
	if (((SDLEvent.tfinger.x * screenResolutionX) != mouseX) || ((SDLEvent.tfinger.y * screenResolutionY) != mouseY))
	{	
		touchLocation.x = (SDLEvent.tfinger.x * screenResolutionX);
		touchLocation.y = (SDLEvent.tfinger.y * screenResolutionY);
		mousePos.x = static_cast<float>(touchLocation.x) / screenResolutionX * 2 - 1;
		mousePos.y = static_cast<float>(touchLocation.y) / screenResolutionY * 2 - 1;
		//Because androids y is different than opengl, so we just flip the Y
		mousePos.y = -mousePos.y;
		prevMouseX = touchLocation.x;
		prevMouseY = touchLocation.y;
	}
#endif
	SDL_GetMouseState(&mouseX, &mouseY);
	mousePos.x = static_cast<float>(mouseX) / screenResolutionX * 2 - 1;
	mousePos.y = -(static_cast<float>(mouseY) / screenResolutionY * 2 - 1);
	prevMouseX = mouseX;
	prevMouseY = mouseY;
}

void EventHandler::handleMouseMotion(SDL_Event & SDLEvent)
{
	updateMousePosition(SDLEvent);
	//Sends a message to InputPlayer
	this->postMessage(Message(this, "mouseMotion", mousePos));
	//parentEngine->getGraphicsHandler()->setCameraDirection(glm::vec2(SDLEvent.motion.xrel, SDLEvent.motion.yrel), parentEngine->getDeltaTime());
}

void EventHandler::handleMouseDown(SDL_Event & SDLEvent)
{
	bool guiClick;
#ifndef __ANDROID__
	switch (SDLEvent.button.button)
	{
	case SDL_BUTTON_LEFT:
#endif
		guiClick = parentEngine->getGuiHandler()->checkMouseClickLeft();

		if (!guiClick) //dont want player to shoot if he is pressing gui elements
		{
			//sends message to all inputcomponents
			this->postMessage(Message(this, "mouseLeftDown", mousePos));
		}
		guiClick = false;
#ifndef __ANDROID__
		break;
	case SDL_BUTTON_RIGHT:
		guiClick = parentEngine->getGuiHandler()->checkMouseClickRight();

		if (!guiClick)
		{
			//sends message to all inputcomponents
			this->postMessage(Message(this, "mouseRightDown", mousePos));
		}
		guiClick = false;
		break;
	}
#endif

}

void EventHandler::handleMouseUp(SDL_Event & SDLEvent)
{
	bool guiRelease;
	guiRelease = parentEngine->getGuiHandler()->checkMouseRelease();
										
	switch (static_cast<int>(SDLEvent.button.button))
	{
	case SDL_BUTTON_LEFT:
		this->postMessage(Message(this, "mouseLeftUp"));
		break;
	case SDL_BUTTON_RIGHT:
		this->postMessage(Message(this, "mouseRightUp"));
		break;
	}
}

void EventHandler::handleMouseWheel(SDL_Event & SDLEvent)
{
	this->postMessage(Message(this, "mouseWheel", SDLEvent));
}

void EventHandler::handleJoystickMotion(SDL_Event & SDLEvent)
{
	if ((SDLEvent.jaxis.value < -JOYSTICKDEADZONE) || (SDLEvent.jaxis.value > JOYSTICKDEADZONE))	//outside deadzone
	{
		this->postMessage(Message(this, "joystickMotion", SDLEvent.jaxis));
	}
	if (SDLEvent.jaxis.axis == 0 && (SDLEvent.jaxis.value >= -JOYSTICKDEADZONE) && (SDLEvent.jaxis.value <= JOYSTICKDEADZONE))
	{
		this->postMessage(Message(this, "joystickNoMotion", SDLEvent.jaxis));
	}
}

void EventHandler::handleJoystickUp(SDL_Event & SDLEvent)
{
	this->postMessage(Message(this, "joystickButtonUp", SDLEvent.jbutton));
}

void EventHandler::handleJoystickDown(SDL_Event & SDLEvent)
{
	this->postMessage(Message(this, "joystickButtonDown", SDLEvent.jbutton));
}

void EventHandler::handleJoystickHat(SDL_Event & SDLEvent)
{
	this->postMessage(Message(this, "joystickHat", SDLEvent.jhat));
}



bool EventHandler::mouseInside(Transform trans)		//used very deeply inside UI some place
{
	Transform temp = trans;
	temp.getPos() -= temp.getScale() / 2.f;
	bool inside = true;
	//Mouse is left of the button
	if (mousePos.x < temp.getPos().x)
	{
		inside = false;
	}
	//Mouse is right of the button
	else if (mousePos.x > temp.getPos().x + temp.getScale().x)
	{
		inside = false;
	}
	//Mouse above the button
	else if (mousePos.y < temp.getPos().y)
	{
		inside = false;
	}
	//Mouse below the button
	else if (mousePos.y > temp.getPos().y + temp.getScale().y)
	{
		inside = false;
	}
	return inside;
}

void EventHandler::handleConsoleTextActions(SDL_Event & SDLevent)
{
	//This is for all the useful commands when you do text editing, like backspace, ctrl+c, ctrl+v and enter.
	if (SDLevent.type == SDL_KEYDOWN)
	{
		//Handle backspace
		if (SDLevent.key.keysym.sym == SDLK_BACKSPACE && inputText.length() > 0)
		{
			//lop off character
			consoleInputText.pop_back();
		}
		//Handle copy
		else if (SDLevent.key.keysym.sym == SDLK_c && SDL_GetModState() & KMOD_CTRL)
		{
			SDL_SetClipboardText(consoleInputText.c_str());
		}
		//Handle paste
		else if (SDLevent.key.keysym.sym == SDLK_v && SDL_GetModState() & KMOD_CTRL)
		{
			consoleInputText = SDL_GetClipboardText();
		}
		else if (SDLevent.key.keysym.sym == SDLK_RETURN)
		{
			inputText = "";
			consoleInputText;
		}

	}
}

//Slight duplicate of the one over.
//TODO, fix a general function for these keyboard commands and functions
void EventHandler::handleTextActions(SDL_Event & SDLevent)
{
	//This is for all the useful commands when you do text editing, like backspace, ctrl+c, ctrl+v and enter.
	if (SDLevent.type == SDL_KEYDOWN)
	{
		//Handle backspace
		if (SDLevent.key.keysym.sym == SDLK_BACKSPACE && inputText.length() > 0)
		{
			//lop off character
			inputText.pop_back();
		}
		//Handle copy
		else if (SDLevent.key.keysym.sym == SDLK_c && SDL_GetModState() & KMOD_CTRL)
		{
			SDL_SetClipboardText(inputText.c_str());

		}
		//Handle paste
		else if (SDLevent.key.keysym.sym == SDLK_v && SDL_GetModState() & KMOD_CTRL)
		{
			inputText = SDL_GetClipboardText();

		}
		else if (SDLevent.key.keysym.sym == SDLK_RETURN)
		{
			//inputText = "";
		}
	}
}

//TODO fix a general function for the function below and the one below that
void EventHandler::handleConsoleInputText(SDL_Event &SDLevent)
{
	if (SDLevent.type == SDL_TEXTINPUT)
	{
		//Not copy or pasting
		
		if (!((SDLevent.text.text[0] == 'c' || SDLevent.text.text[0] == 'C') && (SDLevent.text.text[0] == 'v' || SDLevent.text.text[0] == 'V') && SDL_GetModState() & KMOD_CTRL))
		{
			//Append character
			if (!(SDLevent.text.text[0] == '|'))
			{
				consoleInputText += SDLevent.text.text;
			}
		}
	
	}
}

void EventHandler::handleInputText(SDL_Event &SDLevent)
{
	if (SDLevent.type == SDL_TEXTINPUT)
	{
		//Not copy or pasting

		if (!((SDLevent.text.text[0] == 'c' || SDLevent.text.text[0] == 'C') && (SDLevent.text.text[0] == 'v' || SDLevent.text.text[0] == 'V') && SDL_GetModState() & KMOD_CTRL))
		{
			//Append character
			if (!(SDLevent.text.text[0] == '|'))
			{
				inputText += SDLevent.text.text;			
			}
		}
	}
}

void EventHandler::swapConsoleStatus()
{
	consoleActive = !consoleActive;
}

void EventHandler::setInputStatus(bool status)
{
	inputActive = status;
	inputText = "";
}

glm::ivec2 EventHandler::getMousePos()
{
	return glm::ivec2(mouseX, mouseY);
}

glm::vec3 EventHandler::getMouseOpenGLPos()
{
	return mousePos;
}

std::string EventHandler::getInputText()
{		 
	return inputText;
}
