#include "PlayerGroundPhysics.h"
#include "Actor.h"
#include "Engine.h"

PlayerGroundPhysics::PlayerGroundPhysics(tinyxml2::XMLElement * xmlElement)
	: PhysicsComponent(xmlElement)
{
}

void PlayerGroundPhysics::onCollision(int fixtureUserData, Actor * collidedActor)
{
	switch (fixtureUserData)
	{
	case CATEGORY_ENEMYPROJECTILE:
	case CATEGORY_ENEMY:
		
		break;
	default:
		break;
	}
}

void PlayerGroundPhysics::update(float deltaTime)
{
	PhysicsComponent::update(deltaTime);
}

void PlayerGroundPhysics::setActivated(bool flag)
{
	activated = flag;
}

void PlayerGroundPhysics::setGravityScale(float scale)
{
	body->SetGravityScale(scale);
	physicsDefinition.gravity = scale;
}

void PlayerGroundPhysics::onDeath()
{
	parentEngine->getActorFactory()->createActor("res/actors/explosion.xml", parentActor->getPosition());
}

void PlayerGroundPhysics::onSpawn()
{
	spawnphase = 0;
	activated = false;
	sensorContacts = { 0,0,0 };
}