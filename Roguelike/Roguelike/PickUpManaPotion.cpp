#include "PickUpManaPotion.h"
#include "Engine.h"

void PickUpManaPotion::onPickup(Actor * newOwner)
{
	dynamic_cast<CombatComponent*>(newOwner->getComponent("CombatComponent"))->increaseMana(manaValue);
	if (newOwner == parentEngine->getPlayer())
	{
		parentEngine->getGuiHandler()->setBarValue("manaBar", dynamic_cast<CombatComponent*>(newOwner->getComponent("CombatComponent"))->getMana());
	}
}
