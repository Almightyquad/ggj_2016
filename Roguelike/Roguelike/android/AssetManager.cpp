#include "AssetManager.h"

#include <cassert>

namespace krem { namespace android {

class AssetManager::Impl
{
public:
	Impl(JNIEnv* javaEnv, jobject javaAssetManager);

	jmethodID getJavaMethodId(const char* name, const char* signature);

	JNIEnv* javaEnv;
	jobject javaAssetManager;
	jclass javaAssetManagerClass;
	jclass javaIoExceptionClass;

	jmethodID listMethodId;
	jmethodID openMethodId;
};

AssetManager::AssetManager(JNIEnv* javaEnv, jobject javaAssetManager):
	pimpl(std::make_unique<Impl>(javaEnv, javaAssetManager))
{
}

AssetManager::AssetManager():
	AssetManager(nullptr, nullptr)
{
}

AssetManager::~AssetManager() = default;

AssetManager::AssetManager(AssetManager&& other):
	pimpl(std::move(other.pimpl))
{
}

AssetManager& AssetManager::operator=(AssetManager&& other)
{
	this->pimpl = std::move(other.pimpl);

	return *this;
}

std::vector<std::string> AssetManager::list(const std::string& path)
{
	auto pathJavaString = this->pimpl->javaEnv->NewStringUTF(path.c_str());
	auto javaListArray = static_cast<jobjectArray>(this->pimpl->javaEnv->CallObjectMethod(this->pimpl->javaAssetManager, this->pimpl->listMethodId, pathJavaString));

	auto numElements = this->pimpl->javaEnv->GetArrayLength(javaListArray);
	auto fileList = std::vector<std::string>();
	fileList.reserve(numElements);

	for (jsize i = 0; i < numElements; ++i)
	{
		auto elementJavaString = static_cast<jstring>(this->pimpl->javaEnv->GetObjectArrayElement(javaListArray, i));

		auto elementCString = this->pimpl->javaEnv->GetStringUTFChars(elementJavaString, nullptr);
		auto elementString = std::string(elementCString);
		this->pimpl->javaEnv->ReleaseStringUTFChars(elementJavaString, elementCString);

		this->pimpl->javaEnv->DeleteLocalRef(elementJavaString);

		fileList.push_back(std::move(elementString));
	}

	this->pimpl->javaEnv->DeleteLocalRef(javaListArray);
	this->pimpl->javaEnv->DeleteLocalRef(pathJavaString);

	return fileList;
}
#include <android\log.h>
AssetFile AssetManager::open(const std::string& path)
{
	__android_log_print(ANDROID_LOG_VERBOSE, "Roguelike: ","%s", path.c_str());
	auto pathJavaString = this->pimpl->javaEnv->NewStringUTF(path.c_str());
	auto inputStreamJavaObject = this->pimpl->javaEnv->CallObjectMethod(this->pimpl->javaAssetManager, this->pimpl->openMethodId, pathJavaString);
	this->pimpl->javaEnv->DeleteLocalRef(pathJavaString);
	if (this->pimpl->javaEnv->ExceptionCheck())
	{
		auto exception = this->pimpl->javaEnv->ExceptionOccurred();
		this->pimpl->javaEnv->ExceptionClear();
		// File not found (could be a directory).
		if (this->pimpl->javaEnv->IsInstanceOf(exception, this->pimpl->javaIoExceptionClass))
		{
			this->pimpl->javaEnv->DeleteLocalRef(exception);
			return AssetFile(path);
		}
		else
		{
			this->pimpl->javaEnv->DeleteLocalRef(exception);
			throw std::runtime_error("Exception caught while opening Android asset.");
		}
	}
	else
	{
		return AssetFile(path, this->pimpl->javaEnv, inputStreamJavaObject);
	}
}

AssetManager::Impl::Impl(JNIEnv* javaEnv, jobject javaAssetManager):
	javaEnv(javaEnv),
	javaAssetManager(javaAssetManager),
	javaAssetManagerClass(nullptr)
{
	if (this->javaEnv && this->javaAssetManager)
	{
		this->javaAssetManagerClass = javaEnv->GetObjectClass(javaAssetManager);
		this->javaIoExceptionClass = javaEnv->FindClass("java/io/IOException");

		this->listMethodId = this->getJavaMethodId("list", "(Ljava/lang/String;)[Ljava/lang/String;");
		this->openMethodId = this->getJavaMethodId("open", "(Ljava/lang/String;)Ljava/io/InputStream;");
	}
}

jmethodID AssetManager::Impl::getJavaMethodId(const char* name, const char* signature)
{
	return this->javaEnv->GetMethodID(this->javaAssetManagerClass, name, signature);
}

} }
