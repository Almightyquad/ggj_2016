#pragma once
#include <string>
#include <unordered_map>
#include "ActorComponent.h"
#include "tinyxml2-master\tinyxml2.h"

/**
* @class	AudioComponent
*
* @brief	The audio component interface.
*/
class IAudioComponent : public ActorComponent
{
public:
	IAudioComponent(tinyxml2::XMLElement *xmlElement) : ActorComponent(xmlElement) {};
	virtual ~IAudioComponent(){};

protected:
	std::unordered_map<std::string, std::string> soundMap;
	std::unordered_map<std::string, std::string> musicMap;
};