#include "NeutralProjectilePhysics.h"
#include "Actor.h"
#include "Engine.h"
#include "PlayerGroundInput.h"
#include "MinotaurArtificialIntelligence.h"


NeutralProjectilePhysics::NeutralProjectilePhysics(tinyxml2::XMLElement * xmlElement)
	:PhysicsComponent(xmlElement),
	combatComponent(nullptr)
{

}

void NeutralProjectilePhysics::onCollision(int fixtureUserData, Actor * collidedActor)
{
	if (parentActor->getActorId().actorName == "Heal")
	{
		return;
	}

	if (combatComponent == nullptr)
	{
		combatComponent = dynamic_cast<CombatComponent*>(parentActor->getComponent("CombatComponent"));
	}
	glm::vec3 pos = parentActor->getPosition();

	switch (fixtureUserData)
	{
	case CATEGORY_PLAYERGROUND:
		dynamic_cast<CombatComponent*>(collidedActor->getComponent("CombatComponent"))->reduceHealth(combatComponent->getDamage());
		parentEngine->getActorFactory()->addToDeleteQueue(parentActor);
		if (parentActor->getActorId().actorName == "Freeze")
		{
			dynamic_cast<PlayerGroundInput*>(collidedActor->getComponent("InputComponent"))->setFrozen(true, 3);
		}
		onDeath();
		break;
	case CATEGORY_ENEMY:
		dynamic_cast<CombatComponent*>(collidedActor->getComponent("CombatComponent"))->reduceHealth(combatComponent->getDamage());
		parentEngine->getActorFactory()->addToDeleteQueue(parentActor);

		if (parentActor->getActorId().actorName == "Freeze")
		{
			if (collidedActor->getActorId().actorName == "Minotaur")
			{
				Texture* newTexture = parentEngine->getTextureHandler()->loadTexture("res/textures/characters/Minotaur/minotaurFrozen.png");
				dynamic_cast<GraphicsComponent*>(collidedActor->getComponent("GraphicsComponent"))->setTexture(newTexture);
				if (dynamic_cast<MinotaurArtificialIntelligence*>(collidedActor->getComponent("AIComponent")))
				{
					dynamic_cast<MinotaurArtificialIntelligence*>(collidedActor->getComponent("AIComponent"))->setFrozen(true, 5);
				}
			}
			
		}

		onDeath();
		break;
	case CATEGORY_SCENERY:
		parentEngine->getActorFactory()->addToDeleteQueue(parentActor);
		onDeath();
		break;
	default:
		break;
	}
}

void NeutralProjectilePhysics::update(float deltaTime)
{
	if (parentActor->getActorId().actorName == "Heal")
	{
		for (b2ContactEdge* contact = body->GetContactList();contact;contact = contact->next)
		{
				dynamic_cast<CombatComponent*>(static_cast<PhysicsComponent*>(contact->contact->GetFixtureA()->GetBody()->GetUserData())->getParentActor()->getComponent("CombatComponent"))->increaseHealth(10.f*deltaTime);			
		}
	}
	
	if (parentActor->getActorId().actorName == "Meteor")
	{
		float newAngle = getRotation() + (glm::two_pi<float>() * deltaTime);
		setRotation(newAngle);
	}
	PhysicsComponent::update(deltaTime);
}

void NeutralProjectilePhysics::onDeath()
{
	if (parentActor->getActorId().actorName == "Meteor")
	{
		for (int i = 0; i < 5; i++)
		{
			float offsetX = (((rand() % 200) / 25.f) - 1.f) * static_cast<float>(i);
			float offsetY = (((rand() % 200) / 25.f) - 1.f) * static_cast<float>(i);
			parentEngine->getActorFactory()->createActor("res/actors/explosion.xml", { parentActor->getPosition().x + offsetX, parentActor->getPosition().y + offsetY, parentActor->getPosition().z });
		}
	}
	
}

void NeutralProjectilePhysics::onSpawn()
{
	//parentEngine->getParticleSystem()->createPoint(10000.f, parentEngine->getDeltaTime(), glm::vec3({ parentActor->getPosition().x,parentActor->getPosition().y,0 }), 3, glm::vec4(1.f, 0.4f, 0.3f, 1.0f), glm::vec3(0.6f, 0.8f, 0.8f));
}

