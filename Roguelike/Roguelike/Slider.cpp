#include "Slider.h"
#include "Engine.h"
#include "GuiState.h"
#include "Message.h"

Slider::Slider(tinyxml2::XMLElement *xmlElement)
{
	this->originNode = xmlElement;
	//HARDCODED LIKE FUCK
	slider = false;
	left = false;
	right = false;
	fontDisplay = false;
	currentSliderTexture = 1;
	currentRightTexture = 0;
	currentLeftTexture = 2;
}

Slider::~Slider()
{
}

void Slider::update()
{ 
	transform[0].getPos().x = values[currentValue];
	//HARDCODED LIKE FUCK
	if (slider)
	{
		//checks if the slider is inbetween the left and right arrow while using the mouse
		if (parentEngine->getEventHandler()->getMouseOpenGLPos().y < transform[0].getPos().y + transform[0].getScale().y * 1.1
			&&parentEngine->getEventHandler()->getMouseOpenGLPos().y > transform[0].getPos().y - transform[0].getScale().y * 1.1)
		{		//decreases the value of the slider in the left direction
			if (parentEngine->getEventHandler()->getMouseOpenGLPos().x <= transform[0].getPos().x)
			{
				if (currentValue > minValue)
				{
					currentValue -= valueInterval;
					transform[0].getPos().x = values[currentValue];
				}
			}//increases the value of the slider in the right direction
			if (parentEngine->getEventHandler()->getMouseOpenGLPos().x >= transform[0].getPos().x)
			{
				if (currentValue < maxValue)
				{
					currentValue += valueInterval;
					transform[0].getPos().x = values[currentValue];
				}
			}
		}
		else
		{
			slider = false;
		}
	}

	if (left)
	{	//checks if the slider is higher than the left arrow when clicking the left arrow
		if (transform[0].getPos().x > (transform[1].getPos().x + transform[1].getScale().x))
		{ //decreases the value on the slider
			if (currentValue > minValue)
			{
				currentValue -= valueInterval;
				transform[0].getPos().x = values[currentValue];
			}
		}
		
	}

	if (right)
	{	//checks if the slider is lower than the right arrow when clicking the right arrow
		if (transform[0].getPos().x < (transform[2].getPos().x - transform[2].getScale().x))
		{ //increases the value of the slider
			if (currentValue < maxValue)
			{
				currentValue += valueInterval;
				transform[0].getPos().x = values[currentValue];
			}
			
		}
	}
	checkMouseOver();
	
	const char * tempName = name.c_str();
	//sends a message with the slider name and the current value displayed to be used in functions
	this->postMessage(Message(this, tempName, currentValue));
}

void Slider::checkMouseOver()
{
	//TODO: REMOVE MAGIC NUMBERS
	if (parentEngine->getEventHandler()->mouseInside(transform[0]) && !slider)
	{
		currentSliderTexture = 4;
	}
	else if (!slider)
	{
		currentSliderTexture = 1;
	}
	if (parentEngine->getEventHandler()->mouseInside(transform[1]) && !left)
	{
		currentLeftTexture = 3;
	}
	else if (!left)
	{
		currentLeftTexture = 0;
	}
	if (parentEngine->getEventHandler()->mouseInside(transform[2]) && !right)
	{
		currentRightTexture = 5;
	}
	else if (!right)
	{
		currentRightTexture = 2;
	}
}

bool Slider::checkMouseClickLeft()
{
	//TODO: REMOVE MAGIC NUMBERS
	if (parentEngine->getEventHandler()->mouseInside(transform[0]))
	{
		slider = true;
		currentSliderTexture = 7;
		return slider;
	}
	if (parentEngine->getEventHandler()->mouseInside(transform[1]))
	{
		left = true;
		currentLeftTexture = 6;

		return left;
	}

	if (parentEngine->getEventHandler()->mouseInside(transform[2]))
	{
		right = true;
		currentRightTexture = 8;

		return right;
	}
	return false;
}

bool Slider::checkMouseClickRight()
{
	return false;
}

bool Slider::checkMouseRelease()
{
	slider = false;
	left = false;
	right = false;
	currentSliderTexture = 1;
	currentLeftTexture = 0;
	currentRightTexture = 2;

	return false;
}

void Slider::draw(glm::mat4 &viewProjection)
{
	float SCALE = 1.5f;
	//TODO: rewrite this shit aswell
	for (int i = 0; i < 3; i++)
	{
		//HARDCODED LIKE FUCK
		shader->bind();
		if (i == 0)
		{
			shader->loadTransform(transform[i], viewProjection);
			shader->loadVec2(U_SIZE, glm::vec2(textureAtlasSize, textureAtlasSize));
			shader->loadInt(U_TEXTURE0, 0);
			shader->loadInt(U_SPRITE_NO, currentSliderTexture);
			shader->loadFloat(U_SCALE, SCALE);
			if (fontDisplay)
			{
				font->update(std::to_string(currentValue), glm::vec3(0, 0, 0));
				shader->loadInt(U_TEXTURE1, 1);
				font->bind(1);
			}
		
		}
		else if (i == 1)
		{
			shader->loadTransform(transform[i], viewProjection);
			shader->loadVec2(U_SIZE, glm::vec2(textureAtlasSize, textureAtlasSize));
			shader->loadInt(U_TEXTURE0, 0);	
			shader->loadInt(U_SPRITE_NO, currentLeftTexture);
			shader->loadFloat(U_SCALE, SCALE);
			if (fontDisplay)
			{
				font->update(" ", glm::vec3(0, 0, 0));
				shader->loadInt(U_TEXTURE1, -1);
				font->bind(1);
			}
		}
		else
		{
			shader->loadTransform(transform[i], viewProjection);
			shader->loadVec2(U_SIZE, glm::vec2(textureAtlasSize, textureAtlasSize));
			shader->loadInt(U_TEXTURE0, 0);
			shader->loadInt(U_SPRITE_NO, currentRightTexture);
			shader->loadFloat(U_SCALE, SCALE);
			if (fontDisplay)
			{
				font->update(" ", glm::vec3(0, 0, 0));
				shader->loadInt(U_TEXTURE1, -1);
				font->bind(1);
			}
		} 
		texture->bind(0);
		mesh->draw();
	}
}

void Slider::init(Engine* parentEngine)
{
	this->parentEngine = parentEngine;
	float tempPosX;
	float tempPosY;
	float tempWidth;
	float tempHeight;

	std::string pathTemp;

	int tempValues;
	int tempAtlasSize;

	//TODO: rewrite
	if (this->originNode->FirstChildElement("name"))
	{
		name = this->originNode->FirstChildElement("name")->GetText();
	}
	else if (xmlDebug)
	{
		printf("Missing name\n");
	}

	if (this->originNode->FirstChildElement("fontPath"))
	{
		pathTemp = this->originNode->FirstChildElement("fontPath")->GetText();
		font = parentEngine->getFontHandler()->loadFont(pathTemp);
		fontDisplay = true;
	}
	else if (xmlDebug)
	{
		printf("Missing fontPath\n");
	}

	if (this->originNode->FirstChildElement("positionY"))
	{
		this->originNode->FirstChildElement("positionY")->QueryFloatText(&tempPosY);
	}
	else if (xmlDebug)
	{
		printf("Missing positionY\n");
	}

	if (this->originNode->FirstChildElement("positionSliderX"))
	{
		this->originNode->FirstChildElement("positionSliderX")->QueryFloatText(&tempPosX);
		transform[0].setPos(glm::vec3(tempPosX, tempPosY, 1.0f));
	}
	else if (xmlDebug)
	{
		printf("Missing positionSliderX\n");
	}

	if (this->originNode->FirstChildElement("positionLeftX"))
	{
		this->originNode->FirstChildElement("positionLeftX")->QueryFloatText(&tempPosX);
		transform[1].setPos(glm::vec3(tempPosX, tempPosY, 1.0f));
	}
	else if (xmlDebug)
	{
		printf("Missing positionLeftX\n");
	}

	if (this->originNode->FirstChildElement("positionRightX"))
	{
		this->originNode->FirstChildElement("positionRightX")->QueryFloatText(&tempPosX);
		transform[2].setPos(glm::vec3(tempPosX, tempPosY, 1.0f));
	}
	else if (xmlDebug)
	{
		printf("Missing positionRightX\n");
	}

	if (this->originNode->FirstChildElement("widthInPixels"))
	{
		this->originNode->FirstChildElement("widthInPixels")->QueryFloatText(&tempWidth);
	}
	else if (xmlDebug)
	{
		printf("Missing widthInPixels\n");
	}

	if (this->originNode->FirstChildElement("heightInPixels"))
	{
		this->originNode->FirstChildElement("heightInPixels")->QueryFloatText(&tempHeight);
	}
	else if (xmlDebug)
	{
		printf("Missing heightInPixels\n");
	}

	if (this->originNode->FirstChildElement("minValue"))
	{
		this->originNode->FirstChildElement("minValue")->QueryIntText(&tempValues);
		minValue = tempValues;
	}
	else if (xmlDebug)
	{
		printf("Missing minValue\n");
	}

	if (this->originNode->FirstChildElement("maxValue"))
	{
		this->originNode->FirstChildElement("maxValue")->QueryIntText(&tempValues);
		maxValue = tempValues;
	}
	else if (xmlDebug)
	{
		printf("Missing maxValue\n");
	}

	if (this->originNode->FirstChildElement("defaultValue"))
	{
		this->originNode->FirstChildElement("defaultValue")->QueryIntText(&tempValues);
		currentValue = tempValues;
	}
	else if (xmlDebug)
	{
		printf("Missing defaultValue\n");
	}

	if (this->originNode->FirstChildElement("valueInterval"))
	{
		this->originNode->FirstChildElement("valueInterval")->QueryIntText(&tempValues);
		valueInterval = tempValues;
	}
	else if (xmlDebug)
	{
		printf("Missing valueInterval\n");
	}

	if (this->originNode->FirstChildElement("meshVerticesPath"))
	{
		pathTemp = this->originNode->FirstChildElement("meshVerticesPath")->GetText();
		this->mesh = parentEngine->getMeshHandler()->loadModel(pathTemp);

	}
	else if (xmlDebug)
	{
		printf("Missing meshVerticesPath\n");
	}

	if (this->originNode->FirstChildElement("texturePath"))
	{
		pathTemp = this->originNode->FirstChildElement("texturePath")->GetText();
		this->texture = parentEngine->getTextureHandler()->loadTexture(pathTemp);
	}
	else if (xmlDebug)
	{
		printf("Missing texturePath\n");
	}

	if (this->originNode->FirstChildElement("shaderPath"))
	{
		pathTemp = this->originNode->FirstChildElement("shaderPath")->GetText();
		this->shader = parentEngine->getShaderHandler()->loadShader(pathTemp);
	}
	else if (xmlDebug)
	{
		printf("Missing shaderPath\n");
	}

	if (this->originNode->FirstChildElement("textureAtlasSize"))
	{
		this->originNode->FirstChildElement("textureAtlasSize")->QueryIntText(&tempAtlasSize);
		textureAtlasSize = tempAtlasSize;
	}
	else if (xmlDebug)
	{
		printf("Missing textureAtlasSize\n");
	}

	//rewrite this ugly thing aswell
	for (int i = 0; i < 3; i++)
		transform[i].setScale(glm::vec3(tempWidth, tempHeight, 1.f));

	float tempDistance = transform[2].getPos().x - transform[1].getPos().x - transform[1].getScale().x;
	distance = (tempDistance - transform[1].getScale().x) / maxValue;

	float tempValuePos = transform[1].getPos().x + transform[1].getScale().x;
	int numberOfValue = maxValue - minValue;
	for (int i = 0; i <= numberOfValue; i++)
	{
		values.emplace_back(tempValuePos);
		tempValuePos += distance;
	}
}

void Slider::setParent(GuiState& parentPtr)
{
	this->parentGuiState = &parentPtr;
}

std::string Slider::getName()
{
	return name;
}

GuiState* Slider::getParent()
{
	return parentGuiState;
}
