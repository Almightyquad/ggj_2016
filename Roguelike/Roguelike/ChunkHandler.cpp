#include "ChunkHandler.h"
#include "Engine.h"

ChunkHandler::ChunkHandler()
{
	chunkWidth = 16;
	chunkHeight = 9;
	chunkGenerator = new ChunkGenerator(chunkWidth, chunkHeight);
	chunkNumberX = 0;
	chunkNumberY = 0;
	groundIterator = 0;
}


ChunkHandler::~ChunkHandler()
{
}

void ChunkHandler::setActorFactory(ActorFactory* actorFactory)
{
	this->actorFactory = actorFactory;
}

void ChunkHandler::setPhysicsHandler(PhysicsHandler * physicsHandler)
{
	this->physicsHandler = physicsHandler;
}

void ChunkHandler::generateChunk(int numberOfChunks)
{
	for (int i = 0; i < numberOfChunks; i++)
	{
		chunks.emplace_back(chunkGenerator->generateChunk());
	}
}

void ChunkHandler::loadPrefabList(std::vector<std::string> paths)
{
	for (auto it : paths)
	{
		chunkGenerator->loadPrefab(it);
	}
}

void ChunkHandler::addElementsToList(std::vector<int> ids, std::vector<std::string> names, std::vector<std::string> layers)
{
	for (int i = 0; i < static_cast<int>(ids.size()); i++)
	{
		chunkGenerator->addElement(names[i], ids[i], layers[i]);
	}
}

void ChunkHandler::loadChunks()
{
	std::vector<CollisionData> collisionDataVector;
	generateChunk(1);
	std::vector<std::vector<int>> chunkVec = chunks[chunkNumberX];
	
	for (int i = 0; i < chunkHeight + 2; i++)
	{
		collisionDataVector.push_back(CollisionData{ { chunkWidth * chunkNumberX, -i }, 0.f });
	}

	for (int i = 1; i < chunkWidth + 1; i++)
	{
		collisionDataVector.push_back(CollisionData{ { i + chunkWidth * chunkNumberX, 0 }, 0.f });
	}

	std::vector<Actor*> tempActors;

	for (int y = 0; y < 9; y++)
	{
		for (int x = 0; x < 16; x++)
		{
			if (chunkVec[y][x] == 1)
			{
				tempActors.emplace_back(actorFactory->createActor("res/actors/tileAtlases/tile.xml", glm::vec3(x + chunkNumberX * chunkWidth, -y + chunkNumberY * chunkHeight, 0.0f), glm::vec2(0, 0)));
				collisionDataVector.push_back(CollisionData{ { x + chunkNumberX * chunkWidth + 1, -y - chunkNumberY * chunkHeight - 1 }, 1.f });
			}
			if (chunkVec[y][x] == 3)
			{
				tempActors.emplace_back(actorFactory->createActor("res/actors/tileAtlases/ground.xml", glm::vec3(x + chunkNumberX * chunkWidth, -y + chunkNumberY * chunkHeight, -0.001f), glm::vec2(groundIterator, 1)));
				tempActors.emplace_back(actorFactory->createActor("res/actors/tileAtlases/ground.xml", glm::vec3(x + chunkNumberX * chunkWidth, -y + 1 + chunkNumberY * chunkHeight, -0.001f), glm::vec2(groundIterator, 0)));
				groundIterator++;
				if (groundIterator >= 38)
				{
					groundIterator = 0;
				}
				collisionDataVector.push_back(CollisionData{ { x + chunkNumberX * chunkWidth + 1, -y - chunkNumberY * chunkHeight - 1 }, 1.f });
			}
			if (chunkVec[y][x] == 2)
			{
				tempActors.emplace_back(addActorToMap("res/actors/tileAtlases/castleFg.xml", x, y, glm::vec2(14, 5)));
			}
			if (chunkVec[y][x] == 4)
			{
				tempActors.emplace_back(addActorToMap("res/actors/tileAtlases/column.xml", x, y, glm::vec2(0, 2)));
			}
			if (chunkVec[y][x] == 5)
			{
				tempActors.emplace_back(addActorToMap("res/actors/tileAtlases/column.xml", x, y, glm::vec2(0, 1)));
			}
			if (chunkVec[y][x] == 6)
			{
				tempActors.emplace_back(addActorToMap("res/actors/tileAtlases/column.xml", x, y, glm::vec2(0, 0)));
			}
			if (chunkVec[y][x] != 1 && chunkVec[y][x] != 3)
			{
				collisionDataVector.push_back(CollisionData{ { x + chunkNumberX * chunkWidth + 1, -y - chunkNumberY * chunkHeight - 1 }, 0.f });
			}
		}
	}
	chunkActors.emplace_back(tempActors);
	for (int i = 1; i < chunkWidth + 1; i++)
	{
		collisionDataVector.push_back(CollisionData{ { i + chunkWidth * chunkNumberX, -chunkHeight - 1 }, 0.f });
	}

	for (int i = 0; i < chunkHeight + 2; i++)
	{
		collisionDataVector.push_back(CollisionData{ { chunkWidth + 1 + chunkWidth * chunkNumberX, -i }, 0.f });
	}
	physicsHandler->applyTerrainCollision(collisionDataVector);
	chunkNumberX++;
}

std::vector<std::vector<Actor*>> ChunkHandler::getChunkActors()
{
	return chunkActors;
}

void ChunkHandler::deleteChunkActors()
{
	chunkActors.begin()->clear();
}

Actor * ChunkHandler::addActorToMap(std::string path, int x, int y, glm::vec2 positionInTileMap)
{
	return actorFactory->createActor(path, glm::vec3(x + chunkNumberX * chunkWidth, -y + chunkNumberY * chunkHeight, 0.f), positionInTileMap);
}
