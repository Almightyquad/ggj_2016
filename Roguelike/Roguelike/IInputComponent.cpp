#include "IInputComponent.h"
#include "Engine.h"
IInputComponent::~IInputComponent()
{
	parentEngine->getEventHandler()->removeSubscriber(this);
}

void IInputComponent::init(Engine * parentEngine)
{
	this->parentEngine = parentEngine;
	parentEngine->getEventHandler()->addSubscriber(this);
}
