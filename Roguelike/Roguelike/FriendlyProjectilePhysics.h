#pragma once
#include "PhysicsComponent.h"

class Actor;
class CombatComponent;

class FriendlyProjectilePhysics final : public PhysicsComponent
{
public:
	FriendlyProjectilePhysics(tinyxml2::XMLElement *xmlElement);
	void onCollision(int fixtureUserData, Actor* collidedActor);
	void update(float deltaTime) override;
	//void onDeath() override;
	void onSpawn() override;
private:
	CombatComponent* combatComponent;
	Actor* player = nullptr;
	bool init;
};