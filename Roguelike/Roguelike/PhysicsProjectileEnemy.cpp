#include "PhysicsProjectileEnemy.h"
#include "Actor.h"
#include "Engine.h"

void PhysicsProjectileEnemy::onCollision(int fixtureUserData, Actor * collidedActor)
{
	switch (fixtureUserData)
	{
	case CATEGORY_PLAYER:
		dynamic_cast<CombatComponent*>(parentActor->getComponent("CombatComponent"))->kill();		
		parentActor->onHit();
		break;
	case CATEGORY_SCENERY:
		if (parentActor->getActorId().actorName == "Chucked")
		{
			glm::vec3 pos = dynamic_cast<PhysicsComponent*>(parentActor->getComponent("PhysicsComponent"))->getPosition();
			parentEngine->getParticleSystem()->createPoint(1000.f, parentEngine->getDeltaTime(), pos, 3, glm::vec4(0.5f, 0.5f, 0.5f, 1.0f), glm::vec3(1.0, 1.0, 1.0));
		}
		break;
	default:
		break;
	}
}
