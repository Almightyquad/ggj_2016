#include "AudioComponent.h"
#include "Engine.h"

AudioComponent::AudioComponent(tinyxml2::XMLElement * xmlElement) 
	: IAudioComponent::IAudioComponent( xmlElement)
{

}

AudioComponent::~AudioComponent()
{	
}


void AudioComponent::init(Engine* parentEngine)
{
	std::string tempSoundPaths;
	this->parentEngine = parentEngine;
	XMLElement *itemElement;
	if (originNode->FirstChildElement("sounds"))
	{
		itemElement = this->originNode->FirstChildElement("sounds");
		if (itemElement->FirstChildElement("jumping"))
		{
			tempSoundPaths = itemElement->FirstChildElement("jumping")->GetText();
			soundMap.insert(std::make_pair("jumping", tempSoundPaths));
			parentEngine->getAudioHandler()->loadSoundEffect(tempSoundPaths);
		}
		else if (xmlDebug)
		{
			printf("Missing jumping\n");
		}

		if (itemElement->FirstChildElement("spawn"))
		{
			tempSoundPaths = itemElement->FirstChildElement("spawn")->GetText();
			soundMap.insert(std::make_pair("spawn", tempSoundPaths));
			parentEngine->getAudioHandler()->loadSoundEffect(tempSoundPaths);
		}
		else if (xmlDebug)
		{
			printf("Missing create\n");
		}
		if (itemElement->FirstChildElement("death"))
		{
			tempSoundPaths = itemElement->FirstChildElement("death")->GetText();
			soundMap.insert(std::make_pair("death", tempSoundPaths));
			parentEngine->getAudioHandler()->loadSoundEffect(tempSoundPaths);
		}
		else if (xmlDebug)
		{
			printf("Missing create\n");
		}
		if (itemElement->FirstChildElement("hit"))
		{
			tempSoundPaths = itemElement->FirstChildElement("hit")->GetText();
			soundMap.insert(std::make_pair("hit", tempSoundPaths));
			parentEngine->getAudioHandler()->loadSoundEffect(tempSoundPaths);
		}
		else if (xmlDebug)
		{
			printf("Missing create\n");
		}
	}
	else if (xmlDebug)
	{
		printf("Missing sounds\n");
	}

	if (originNode->FirstChildElement("music"))
	{
		itemElement = this->originNode->FirstChildElement("music");
	}
	else if (xmlDebug)
	{
		printf("Missing Music\n");
	}
	
}

void AudioComponent::receiveMessage(const Message & message)
{
}

void AudioComponent::onSpawn()
{
	auto it = soundMap.find("spawn");
	if (it != soundMap.end())
	{
		parentEngine->getAudioHandler()->playSound(it->second);
	}
}

void AudioComponent::onDeath()
{
	auto it = soundMap.find("death");
	if (it != soundMap.end())
	{
		parentEngine->getAudioHandler()->playSound(it->second);
	}
}

void AudioComponent::onHit()
{
	auto it = soundMap.find("hit");
	if (it != soundMap.end())
	{
		parentEngine->getAudioHandler()->playSound(it->second);
	}
}

