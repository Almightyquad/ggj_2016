#pragma once
#include <glm\glm.hpp>
#ifdef __ANDROID__
#include <GLES3/gl31.h>
#else
#include <GL\glew.h>
#endif
#include <vector>
#include "Texture.h"
#include "Shader.h"
#include "common.h"
#include "obj_loader.h"
#include "Handler.h"

class Engine;

class TerrainHandler : public Handler
{
public:

	/**
	 * @fn	TerrainHandler::TerrainHandler();
	 *
	 * @brief	Default constructor.
	 */
	TerrainHandler();

	/**
	 * @fn	TerrainHandler::~TerrainHandler();
	 *
	 * @brief	Destructor.
	 */
	~TerrainHandler();

	/**
	 * @fn	void TerrainHandler::init();
	 *
	 * @brief	Initialises this object.
	 */
	void init();

	/**
	 * @fn	void TerrainHandler::generateTerrain(float gridX, float gridZ);
	 *
	 * @brief	Generates a terrain.
	 *
	 * @param	gridX	The grid x coordinate.
	 * @param	gridZ	The grid z coordinate.
	 */
	void generateTerrain(float gridX, float gridZ);

	/**
	 * @fn	void TerrainHandler::update();
	 *
	 * @brief	Updates this object.
	 */
	void update();

	/**
	 * @fn	void TerrainHandler::draw(glm::mat4 & viewProjection);
	 *
	 * @brief	Draws the given view projection.
	 *
	 * @param [in,out]	viewProjection	The view projection.
	 */
	void draw(glm::mat4 & viewProjection);

private:
	GLuint vertexArrayObject;
	GLuint vertexArrayBuffer[NUM_BUFFERS];
	glm::vec4 sunPosition;

	unsigned int drawCount;

	const int SIZE;
	const int VERTEX_COUNT;
	const float LOWEST;

	float sunDistance = 100.f;
	float x = 0.f;	//in world grid/chunk
	float z = 0.f;	//in world grid/chunk

	Texture *surface;
	Texture *slopes;
	Shader *shader;
	Transform transform;
};

