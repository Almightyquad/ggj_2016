#include "PhysicsCreator.h"
#include "PhysicsFactory.h"

PhysicsCreator::PhysicsCreator(const std::string & classname, PhysicsFactory* aIFactory)
{
	aIFactory->registerIt(classname, this);
}
