#pragma once
#include "IAnimationComponent.h"
#include <iostream>

class AnimationComponent : public IAnimationComponent
{
public:
	AnimationComponent(tinyxml2::XMLElement *xmlElement);
	~AnimationComponent() override;
	void init(Engine* parentEngine) override;
	void loadFromXML(Animation &anim, char* firstChild);
	void receiveMessage(const Message &message);
	Animation* getAnim(int STATE);
private:

};