#pragma once
#include <map>
#include <string>
#include "PhysicsCreator.h"

class PhysicsFactory
{
public:
	std::unique_ptr<IPhysicsComponent> create(const std::string& itemName, tinyxml2::XMLElement* xmlElement);

	void registerIt(const std::string& itemName, PhysicsCreator* physicsCreator);

private:
	std::map<std::string, PhysicsCreator*> table;
};