#include "CustomCursor.h"
#include "Engine.h"


CustomCursor::CustomCursor()
{
	currentTexture = "";
}


CustomCursor::~CustomCursor()
{
}

bool CustomCursor::addCursorToMap(std::string filePath, Engine * parentEngine)
{
	/*if (cursorMap.count(filePath) == 0)
	{
		SDL_Surface *tempCursor = new SDL_Surface;
		tempCursor = SDL_LoadBMP(filePath.c_str());
		if (tempCursor != NULL)
		{
			printf("Loading %s \n", filePath);
			cursorMap.emplace(filePath, tempCursor);
			SDL_FreeSurface(tempCursor);
			return true;
		}
		SDL_FreeSurface(tempCursor);
		return false;
	}
	return true;*/
	if (cursorMap.count(filePath) == 0)
	{
		cursorMap.emplace(filePath, parentEngine->getTextureHandler()->loadTexture(filePath));
	}
	return true;
}

void CustomCursor::toggleCursorOnOff()
{
	//Possibly rewrite this later so it doesn't have an else if
	int cursorState = SDL_ShowCursor(SDL_QUERY);
	if (cursorState == 0)
	{
		SDL_ShowCursor(SDL_ENABLE);
	}
	else if (cursorState == 1)
	{
		SDL_ShowCursor(SDL_DISABLE);
	}
}

bool CustomCursor::toggleCursorTexture(std::string filePath, Engine * parentEngine)
{
	if (addCursorToMap(filePath, parentEngine))
	{
		currentTexture = filePath;
		return true;
	}
	printf("An error has occured loading %s \n", filePath.c_str());
	return false;
}

Texture * CustomCursor::getCursorTexture()
{
	if (cursorMap.count(currentTexture) == 0)
	{
		return NULL;
	}
	return cursorMap[currentTexture];
}

void CustomCursor::updateCursor(Engine * parentEngine)
{
	glm::vec3 tempVec = parentEngine->getEventHandler()->getMouseOpenGLPos();
	tempVec.x += this->transform.getScale().x / 2.f;
	tempVec.y -= this->transform.getScale().y / 2.f;
	tempVec.z = 1.0f;
	this->transform.setPos(tempVec);
}

void CustomCursor::draw()
{
	glm::mat4 staticCamera =	//don't want UI to float in 3D space but locked to view projection
	{
		1.f, 0, 0, 0,
		0, 1.f, 0, 0,
		0, 0, -1.f, 0,
		0, 0, 0, 1.f
	};
	glDisable(GL_DEPTH);
	shader->bind();
	shader->loadTransform(this->transform, staticCamera);
	shader->loadInt(U_TEXTURE0, 0);
	getCursorTexture()->bind(0);
	
	mesh->draw();
	glEnable(GL_DEPTH);
}

void CustomCursor::initCursor(Engine * parentEngine)
{
	this->mesh = parentEngine->getMeshHandler()->loadModel("res/models/quad.obj");
	this->shader = parentEngine->getShaderHandler()->loadShader("res/shaders/basicShader");
	transform.setPos(glm::vec3(0.f, 0.f, 1.0f));
	transform.setScale(glm::vec3(0.8f/16.f, 0.8f/9.f, 1.f));
}
