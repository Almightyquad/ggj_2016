#include "Panel.h"
#include "Engine.h"
#include "GuiState.h"
#include "Message.h"


Panel::Panel(tinyxml2::XMLElement *xmlElement)
{
	this->originNode = xmlElement;
}

Panel::~Panel()
{
}

void Panel::draw(glm::mat4 &viewProjection)
{
	shader->bind();
	shader->loadTransform(transform, viewProjection);
	shader->loadInt(U_TEXTURE0, 0);
	shader->loadFloat(U_SCALE, 1.5f);
	texture->bind(0);

	mesh->draw();
}

void Panel::update()
{
}

void Panel::init(Engine* parentEngine)
{
	this->parentEngine = parentEngine;
	float tempPosX;
	float tempPosY;
	float tempWidth;
	float tempHeight;

	std::string pathTemp;

	if (this->originNode->FirstChildElement("name"))
	{
		name = this->originNode->FirstChildElement("name")->GetText();
	}
	else if (xmlDebug)
	{
		printf("Missing name\n");
	}

	if (this->originNode->FirstChildElement("positionX"))
	{
		this->originNode->FirstChildElement("positionX")->QueryFloatText(&tempPosX);
	}
	else if (xmlDebug)
	{
		printf("Missing positionX\n");
	}

	if (this->originNode->FirstChildElement("positionY"))
	{
		this->originNode->FirstChildElement("positionY")->QueryFloatText(&tempPosY);
	}
	else if (xmlDebug)
	{
		printf("Missing positionY\n");
	}

	if (this->originNode->FirstChildElement("widthInPixels"))
	{
		this->originNode->FirstChildElement("widthInPixels")->QueryFloatText(&tempWidth);
	}
	else if (xmlDebug)
	{
		printf("Missing widthInPixels\n");
	}

	if (this->originNode->FirstChildElement("heightInPixels"))
	{
		this->originNode->FirstChildElement("heightInPixels")->QueryFloatText(&tempHeight);
	}
	else if (xmlDebug)
	{
		printf("Missing heightInPixels\n");
	}

	if (this->originNode->FirstChildElement("meshVerticesPath"))
	{
		pathTemp = this->originNode->FirstChildElement("meshVerticesPath")->GetText();
		this->mesh = parentEngine->getMeshHandler()->loadModel(pathTemp);
	}
	else if (xmlDebug)
	{
		printf("Missing meshVerticesPath\n");
	}

	if (this->originNode->FirstChildElement("texturePath"))
	{
		pathTemp = this->originNode->FirstChildElement("texturePath")->GetText();
		this->texture = parentEngine->getTextureHandler()->loadTexture(pathTemp);
	}
	else if (xmlDebug)
	{
		printf("Missing texturePath\n");
	}

	if (this->originNode->FirstChildElement("shaderPath"))
	{
		pathTemp = this->originNode->FirstChildElement("shaderPath")->GetText();
		this->shader = parentEngine->getShaderHandler()->loadShader(pathTemp);
	}
	else if (xmlDebug)
	{
		printf("Missing shaderPath\n");
	}

	transform.setPos(glm::vec3(tempPosX, tempPosY, 1.0f));
	transform.setScale(glm::vec3(tempWidth, tempHeight, 1.f));
}

void Panel::setParent(GuiState& parentPtr)
{
	this->parentGuiState = &parentPtr;
}

void Panel::changeTexture(std::string texturePath)
{
	texture = parentEngine->getTextureHandler()->loadTexture(texturePath);
}

GuiState* Panel::getParent()
{
	return parentGuiState;
}

std::string Panel::getName()
{
	return name;
}
