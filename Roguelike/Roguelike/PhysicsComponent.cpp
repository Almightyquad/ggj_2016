#include "PhysicsComponent.h"
#include "Actor.h"
#include "Engine.h"
#include "PhysicsHandler.h"
#include "Message.h"


PhysicsComponent::PhysicsComponent(tinyxml2::XMLElement *xmlElement) 
: IPhysicsComponent(xmlElement)
{
	sensorContacts.jump = 0;
	sensorContacts.wallLeft = 0;
	sensorContacts.wallRight = 0;
	//forceScale is the scaling for the force exerted. Currently at 60hz because that feels best for now
	forceScale = 1.f/60.f;
}

PhysicsComponent::~PhysicsComponent()
{
	this->body->GetWorld()->DestroyBody(this->body);
}


void PhysicsComponent::init(Engine* parentEngine)
{
	this->parentEngine = parentEngine;
	if (originNode->FirstChildElement("category"))
	{
		physicsDefinition.category = originNode->FirstChildElement("category")->GetText();
	}
	else if (xmlDebug)
	{
		printf("Missing category\n");
	}
	if (originNode->FirstChildElement("bodyType"))
	{
		originNode->FirstChildElement("bodyType")->QueryIntText(&physicsDefinition.bodyType);
	}
	else if (xmlDebug)
	{
		printf("Missing bodyType\n");
	}

	if (originNode->FirstChildElement("sensor"))
	{
		originNode->FirstChildElement("sensor")->QueryBoolText(&physicsDefinition.isSensor);
	}
	else if (xmlDebug)
	{
		printf("Missing sensor.\n");
	}

	if (originNode->FirstChildElement("density"))
	{
		originNode->FirstChildElement("density")->QueryFloatText(&physicsDefinition.density);
	}
	else if (xmlDebug)
	{
		printf("Missing density\n");
	}

	if (originNode->FirstChildElement("xInPixels"))
	{
		originNode->FirstChildElement("xInPixels")->QueryFloatText(&physicsDefinition.size.x);
	}
	else if (xmlDebug)
	{
		printf("Missing xInPixels\n");
	}

	if (originNode->FirstChildElement("yInPixels"))
	{
		originNode->FirstChildElement("yInPixels")->QueryFloatText(&physicsDefinition.size.y);
	}
	else if (xmlDebug)
	{
		printf("Missing yInPixels\n");
	}
	if (originNode->FirstChildElement("jumpSensor"))
	{
		originNode->FirstChildElement("jumpSensor")->QueryBoolText(&physicsDefinition.jumpSensor);
	}
	else if (xmlDebug)
	{
		printf("Missing jumpSensor.\n");
	}

	if (originNode->FirstChildElement("wallSensor"))
	{
		originNode->FirstChildElement("wallSensor")->QueryBoolText(&physicsDefinition.wallSensor);
	}
	else if (xmlDebug)
	{
		printf("Missing wallSensor.\n");
	}

	if (originNode->FirstChildElement("restitution"))
	{
		originNode->FirstChildElement("restitution")->QueryFloatText(&physicsDefinition.restitution);
	}
	else if (xmlDebug)
	{
		printf("Missing restitution\n");
	}

	if (originNode->FirstChildElement("friction"))
	{
		originNode->FirstChildElement("friction")->QueryFloatText(&physicsDefinition.friction);
	}
	else if (xmlDebug)
	{
		printf("Missing friction\n");
	}
	if (originNode->FirstChildElement("fixedRotation"))
	{
		originNode->FirstChildElement("fixedRotation")->QueryBoolText(&physicsDefinition.fixedRotation);
	}
	else if (xmlDebug)
	{
		printf("Missing fixedRotation\n");
	}

	if (originNode->FirstChildElement("gravity"))
	{
		originNode->FirstChildElement("gravity")->QueryFloatText(&physicsDefinition.gravity);
	}
	else if (xmlDebug)
	{
		printf("Missing gravity\n");
	}

	if (originNode->FirstChildElement("defaultForce"))
	{
		originNode->FirstChildElement("defaultForce")->QueryFloatText(&defaultForce);
	}
	else if (xmlDebug)
	{
		printf("Missing defaultForce\n");
	}

	if (originNode->FirstChildElement("shape"))
	{
		std::string temp;
		temp = originNode->FirstChildElement("shape")->GetText();

		if (temp == "box")
		{
			physicsDefinition.shape = physicsDefinition.BOX;
		}
		else if (temp == "circle")
		{
			physicsDefinition.shape = physicsDefinition.CIRCLE;
		}
		else if (temp == "vertices")
		{
			//do more fancy stuff here when needed. just defaults to box for now
			physicsDefinition.shape = physicsDefinition.BOX;
		}
		else
		{
			printf("Typo in <shape> in .xml file\n");
		}
	}
	else if (xmlDebug)
	{
		printf("Missing shape\n");
	}
	this->body = this->parentEngine->getPhysicsHandler()->createBox(glm::vec2(position.x, position.y), physicsDefinition);
	this->body->SetUserData(this);
}

void PhysicsComponent::update(float deltaTime)
{
	this->newPosition = { body->GetPosition().x, body->GetPosition().y, parentActor->getPosition().z };
	this->newAngle = body->GetAngle();

	bool changed = false;
	if (angle > glm::two_pi<float>() || angle < -glm::two_pi<float>())
	{
		body->SetTransform(body->GetPosition(), 0.f);	//sets body angle to 0 if above or under pi*2 / -pi*2 since transformation sets rotation to 0 if above or below.
		angle = 0.f;									//sets angle to 0
	}

	if (newPosition != position)
	{
		changed = true;
		position = newPosition;
	}
	if (newAngle != angle)
	{
		changed = true;
		angle = newAngle;
	}
	if (changed)
	{
		this->postMessage(Message(this, this->body));
		parentActor->setPosition(newPosition);
		//if (dynamic_cast<GraphicsComponent*>(parentActor->getComponent("GraphicsComponent"))->tran
	}
}


void PhysicsComponent::setPosition(glm::vec3 newPosition)
{
	body->SetTransform(b2Vec2(newPosition.x, newPosition.y), body->GetAngle());
	this->position = newPosition;
}

glm::vec3 PhysicsComponent::getPosition()
{
	return glm::vec3(body->GetPosition().x, body->GetPosition().y, 0.f);
	
}

void PhysicsComponent::setRotation(float angle)
{
	body->SetTransform(body->GetPosition(), angle);	
}

void PhysicsComponent::setVelocity(glm::vec3 velocity)
{
	b2Vec2 temp(velocity.x, velocity.y);
	body->SetLinearVelocity(temp);
}

float PhysicsComponent::getRotation()
{
	return body->GetAngle();
}


glm::vec2 PhysicsComponent::getVelocity()
{
	b2Vec2 vel = body->GetLinearVelocity();
	return{ vel.x, vel.y };
}

void PhysicsComponent::applySpeed(glm::vec2 desiredSpeed)
{
	b2Vec2 currentVelocity = body->GetLinearVelocity();
	float desiredXVelocity = desiredSpeed.x;
	float velocityChange = -currentVelocity.x;
	float impulse;

	if (desiredSpeed.x < 0.f)
	{
		if (sensorContacts.wallLeft == 0)
		{
			//desiredXVelocity = -4.f;
			velocityChange += desiredXVelocity;
			impulse = body->GetMass() * velocityChange;
			body->ApplyLinearImpulse(b2Vec2(impulse, 0.f), body->GetWorldCenter(), true);
		}
		if (sensorContacts.wallLeft > 0)
		{
			body->SetLinearVelocity((b2Vec2(0, currentVelocity.y)));
		}
	}
	if (desiredSpeed.x > 0.f)
	{
		if (sensorContacts.wallRight == 0)
		{
			//desiredXVelocity = 4.f;
			velocityChange += desiredXVelocity;
			impulse = body->GetMass() * velocityChange;
			body->ApplyLinearImpulse(b2Vec2(impulse, 0.f), body->GetWorldCenter(), true);
		}
		else
		{
			body->SetLinearVelocity((b2Vec2(0.f, currentVelocity.y)));
		}
	}
	if (desiredSpeed.x == 0.f)
	{
		if (sensorContacts.jump > 0) //if on ground
		{
			desiredXVelocity = currentVelocity.x*0.5f;
		}
		if (sensorContacts.jump == 0) //if airborne
		{
			desiredXVelocity = currentVelocity.x*0.5f;
		}
		velocityChange += desiredXVelocity;
		impulse = body->GetMass() * velocityChange;
		body->ApplyLinearImpulse(b2Vec2(impulse, 0.f), body->GetWorldCenter(), true);
	}
}

void PhysicsComponent::applyLinearImpulse(glm::vec2 impulse, bool knockback)
{
	if (!knockback)
	{
		if ((body->GetLinearVelocity().y <= 0) && (sensorContacts.jump > 0))
		{
			this->body->ApplyLinearImpulse({ impulse.x,impulse.y }, body->GetWorldCenter(), true);
		}
	}
	if (knockback)
	{
		this->body->ApplyLinearImpulse({ impulse.x,impulse.y }, body->GetWorldCenter(), true);
	}
}

void PhysicsComponent::receiveMessage(const Message &message)
{
	//if (message.getSenderType() == typeid(IInputComponent))
	{
		if (static_cast<const char*>(message.getVariable(0)) == "keyboardEvent")
		{
			b2Vec2 currentVelocity = body->GetLinearVelocity();
			float desiredXVelocity = 0.f;
			float velocityChange = -currentVelocity.x;
			float impulse;

			switch (static_cast<int>(message.getVariable(1)))
			{
			case -1: //move left
				if (sensorContacts.wallLeft == 0)
				{
					desiredXVelocity = -4.f;
					velocityChange += desiredXVelocity;
					impulse = body->GetMass() * velocityChange;
					body->ApplyLinearImpulse(b2Vec2(impulse, 0.f), body->GetWorldCenter(), true);

					//this->body->SetLinearVelocity(b2Vec2(-this->defaultForce*forceScale, currentVelocity.y));
				}
				if (sensorContacts.wallLeft > 0)
				{
					body->SetLinearVelocity((b2Vec2(0, currentVelocity.y)));
				}
				break;
			case 0: //don't move
				if (sensorContacts.jump > 0) //if on ground
				{
					desiredXVelocity = currentVelocity.x*0.5f;
				}
				if (sensorContacts.jump == 0) //if airborne
				{
					desiredXVelocity = currentVelocity.x*0.5f;
				}
				velocityChange += desiredXVelocity;
				impulse = body->GetMass() * velocityChange;
				body->ApplyLinearImpulse(b2Vec2(impulse, 0.f), body->GetWorldCenter(), true);
				break;
			case 1: //move right! someone can rewrite this if you want
				if (sensorContacts.wallRight == 0)
				{
					desiredXVelocity = 4.f;
					velocityChange += desiredXVelocity;
					impulse = body->GetMass() * velocityChange;
					body->ApplyLinearImpulse(b2Vec2(impulse, 0.f), body->GetWorldCenter(), true);
				}
				else
				{
					body->SetLinearVelocity((b2Vec2(0.f, currentVelocity.y)));
				}
				break;
			default:
				printf("invalid input to physicscomponent");
				break;
			}
			

			if (static_cast<bool>(message.getVariable(2)) == true)
			{
				if ((currentVelocity.y <= 0) && (sensorContacts.jump > 0))
				{
  					this->body->ApplyLinearImpulse(b2Vec2(0, body->GetMass() * 8), body->GetWorldCenter(), true);
				}
				if (debug)
				{
					printf("yvelocity: %f\n", currentVelocity.y);
				}
			}
		}
	}
	if (message.getSenderType() == typeid(IAIComponent))
	{
		if (static_cast<const char*>(message.getVariable(0)) == "velocityUpdate")
		{

			b2Vec2 newVelocity = body->GetLinearVelocity();
			if (body->GetLinearVelocity().x == 0)
			{
				newVelocity.x = defaultForce*parentEngine->getDeltaTime();
			}
			
			//b2Vec2 newVelocity = { message.getVariable(1), message.getVariable(2) };
			if (body->GetLinearVelocity().x < 0 && sensorContacts.wallLeft > 0)
			{
				newVelocity.x = defaultForce*parentEngine->getDeltaTime();
			}
			if (body->GetLinearVelocity().x >= 0 && sensorContacts.wallRight > 0)
			{
				newVelocity.x = -defaultForce*parentEngine->getDeltaTime();
			}
			newVelocity.y = body->GetLinearVelocity().y;
			
			this->body->SetLinearVelocity(newVelocity);
		}

		if (static_cast<const char*>(message.getVariable(0)) == "jump")
		{
			float impulse = body->GetMass() * 10.f;
			if ((this->body->GetLinearVelocity().y <= 0) && (sensorContacts.jump > 0))
			{
 				b2Vec2 temp = b2Vec2(0, impulse);
				this->body->SetLinearVelocity({body->GetLinearVelocity().x, 0.f});
				this->body->ApplyLinearImpulse(temp, body->GetWorldCenter(), true);
			}
		}
	}
}

std::string PhysicsComponent::getActorName()
{
	return parentActor->getActorId().actorName;
}

bool PhysicsComponent::wallCollision()
{
	if (body->GetLinearVelocity().x < 0 && sensorContacts.wallLeft > 0)
	{
		return true;
	}
	if (body->GetLinearVelocity().x > 0 && sensorContacts.wallRight > 0)
	{
		return true;
	}
	return false;
}

//flips x velocity, not y
void PhysicsComponent::flipVelocity()
{
	body->SetLinearVelocity({ -body->GetLinearVelocity().x, body->GetLinearVelocity().y });
}

Actor * PhysicsComponent::getParentActor()
{
	return parentActor;
}

b2Body * PhysicsComponent::getB2Body()
{
	return body;
}

void PhysicsComponent::setB2Body(b2Body* body)
{
	this->body = body;
	body->SetUserData(this);
}

PhysicsDefinition PhysicsComponent::getPhysicsDefinition()
{
	return physicsDefinition;
}

b2Fixture* PhysicsComponent::applyFixture(krem::Vector2f size, krem::Vector2f relativePosition, void * fixturename, float angle, bool sensor)
{
	b2PolygonShape newFixtureShape;
	newFixtureShape.SetAsBox(size.x, size.y, b2Vec2({ relativePosition.x,relativePosition.y }), angle);
	
	b2FixtureDef newFixtureDefinition;
	newFixtureDefinition.shape = &newFixtureShape;
	newFixtureDefinition.isSensor = sensor;
	
	b2Fixture* newFixture = body->CreateFixture(&newFixtureDefinition);
	newFixture->SetUserData((void*)fixturename);
	return newFixture;
}

void PhysicsComponent::startContact(void* sensorType)
{
	if (sensorType == "jump")
	{
		sensorContacts.jump++;
	}
	if (sensorType == "wallLeft")
	{
		sensorContacts.wallLeft++;
	}
	if (sensorType == "wallRight")
	{
		sensorContacts.wallRight++;
	}
}

void PhysicsComponent::endContact(void* sensorType)
{
	if (sensorType == "jump")
	{
		sensorContacts.jump--;
	}
	if (sensorType == "wallLeft")
	{
		sensorContacts.wallLeft--;
	}
	if (sensorType == "wallRight")
	{
		sensorContacts.wallRight--;
	}
}
