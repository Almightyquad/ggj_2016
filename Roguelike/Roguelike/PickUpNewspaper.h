#pragma once
#include "IPickUpComponent.h"

class PickUpNewspaper final : public IPickUpComponent
{
public:
	PickUpNewspaper(tinyxml2::XMLElement *xmlElement) : IPickUpComponent(xmlElement) {
	};
	virtual void onToss() override;
	virtual void onActivation() override;
	virtual void onDeactivation() override;
private:
};