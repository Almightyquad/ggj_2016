#pragma once
#include <vector>
#include <map>
#include <SDL.h>
#include <glm\glm.hpp>
#include "Texture.h"
#include "Shader.h"
#include "Transform.h"
#include "Mesh.h"

class Engine;

class CustomCursor
{
public:
	CustomCursor();
	~CustomCursor();
	//Toggles cursor on or off
	void toggleCursorOnOff();
	bool toggleCursorTexture(std::string filePath, Engine * parentEngine);
	Texture * getCursorTexture();
	void updateCursor(Engine * parentEngine);
	void draw();
	void initCursor(Engine * parentEngine);

private:
	std::map<std::string, Texture *> cursorMap;
	std::string currentTexture;
	bool addCursorToMap(std::string filePath, Engine * parentEngine);

	Transform transform;
	Mesh *mesh;
	Shader *shader;
	Texture *texture;
};

