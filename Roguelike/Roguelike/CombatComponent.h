#pragma once
#include "ICombatComponent.h"
#include "Timer.h"
/**
* @class	ILifeComponent
*
* @brief	A life component.
* @defails	Tracks whether the actor is alive.
* 			Dies whenever health or timer reacher zero(whichever occurs first).	
*/
class CombatComponent : public ICombatComponent
{
public:
	CombatComponent(tinyxml2::XMLElement *xmlElement);
	virtual ~CombatComponent();
	void init(Engine* parentEngine);
	void update(float deltaTime) override;
	void reduceHealth(float damage);
	void reduceMana(int manaCost);
	void increaseHealth(float healthValue);
	void increaseMana(int manaValue);
	
	int getMana();
	int getHealth();
	int getDamage();
	void kill();
private:
	Timer invincibilityTimer;
};

