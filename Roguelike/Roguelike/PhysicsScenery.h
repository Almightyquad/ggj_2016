#pragma once
#include "PhysicsComponent.h"

class PhysicsScenery final : public PhysicsComponent
{
public:
	PhysicsScenery(tinyxml2::XMLElement *xmlElement) : PhysicsComponent(xmlElement) {};
	void onCollision(int fixtureUserData, Actor* collidedActor);
private:
};