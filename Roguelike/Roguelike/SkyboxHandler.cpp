#include "SkyboxHandler.h"
#include "Engine.h"

SkyboxHandler::SkyboxHandler()
	: rotation(glm::half_pi<float>())
{

}

SkyboxHandler::~SkyboxHandler()
{
	for (int i = 0; i < NO_OF_SKYBOX_TEXTURES; i++)
	{
		glDeleteTextures(1, &texture[i]);
	}
}

void SkyboxHandler::init()
{
	shader = parentEngine->getShaderHandler()->loadShader("res/shaders/skyboxShader");

	loadCube();
	glGenVertexArrays(1, &vertexArrayObject);
	glBindVertexArray(vertexArrayObject);

	glGenBuffers(1, vertexArrayBuffer);
	glBindBuffer(GL_ARRAY_BUFFER, vertexArrayBuffer[0]);
	glBufferData(GL_ARRAY_BUFFER, positions.size()*sizeof(positions[0]), &positions[0], GL_STATIC_DRAW);

	glEnableVertexAttribArray(0);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, 0);

	glBindVertexArray(0);
}

unsigned char* SkyboxHandler::loadSTBTexture(const std::string filename, int &width, int &height, int &numComponents)
{
/*#ifdef __ANDROID__
	unsigned char* imageData;
	auto tempCharVec = getSTBString(filename, parentEngine->getAssetMgr());
	unsigned char* tempUnChar = reinterpret_cast<unsigned char*>(tempCharVec.data());
	imageData = stbi_load_from_memory(tempUnChar, sizeof(tempUnChar) * tempCharVec.size(), &width, &height, &numComponents, 4);
	return imageData;
#else
#endif*/
	return stbi_load(filename.c_str(), &width, &height, &numComponents, 4);
}

#ifdef __ANDROID__
const std::vector<char> SkyboxHandler::getSTBString(std::string path, krem::android::AssetManager * assetmgr)
{
	krem::android::AssetFile AF = assetmgr->open(path);
	auto tempBuffer = AF.readAll();
	return tempBuffer;
}
#endif

void SkyboxHandler::loadTexture(const std::string & filename, int type)
{
	int width, height, numComponents;
	unsigned char* imageData[SIDES];

	imageData[RIGHT] = loadSTBTexture((filename + "right.png").c_str(), width, height, numComponents);
	
	imageData[LEFT] = loadSTBTexture((filename + "left.png").c_str(), width, height, numComponents);

	imageData[TOP] = loadSTBTexture((filename + "top.png").c_str(), width, height, numComponents);

	imageData[BOTTOM] = loadSTBTexture((filename + "bottom.png").c_str(), width, height, numComponents);

	imageData[BACK] = loadSTBTexture((filename + "back.png").c_str(), width, height, numComponents);

	imageData[FRONT] = loadSTBTexture((filename + "front.png").c_str(), width, height, numComponents);

	for (int i = 0; i < SIDES; i++)
	{
		if (imageData[i] == NULL)
		{
			std::cerr << "Texture loading failed for texture: " << filename << " " << i << std::endl;
		}
	}

	glEnable(GL_TEXTURE_CUBE_MAP);
	glGenTextures(1, &texture[type]);
	glBindTexture(GL_TEXTURE_CUBE_MAP, texture[type]);

	for (int i = 0; i < SIDES; i++)
	{
		glTexImage2D(GL_TEXTURE_CUBE_MAP_POSITIVE_X + i, 0, GL_RGBA, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, imageData[i]);
	}

	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MIN_FILTER, GL_LINEAR);

	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_R, GL_CLAMP_TO_EDGE);

	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, imageData);
	
	for (int i = 0; i < SIDES; i++)
	{
		stbi_image_free(imageData[i]);
	}
}

void SkyboxHandler::update(glm::mat4& viewProjection, float deltaTime, float speed)
{
	this->rotation += speed * deltaTime;	//5min/day 0.0209f
	if (this->rotation >= glm::two_pi<float>() || this->rotation <= -glm::two_pi<float>())
	{
		rotation = 0;
	}
}

void SkyboxHandler::bind(unsigned int unit)
{
	glActiveTexture(GL_TEXTURE0 + unit);
	glBindTexture(GL_TEXTURE_CUBE_MAP, texture[unit]);
}

void SkyboxHandler::draw(glm::mat4 & viewProjection)
{
	glm::mat4 view = viewProjection;
	glDepthMask(0);
	Transform transform;
	view[3][0] = 0;
	view[3][1] = 0;
	view[3][2] = 0;
	view[3][3] = 1.f;	//lock skybox around camera
	view *= glm::rotate(rotation, glm::vec3(0, 1, 0));	//rotate "rotation" per second around y-axis

	shader->bind();
	shader->loadTransform(transform, view);
	shader->loadInt(U_TEXTURE0, 0);
	shader->loadInt(U_TEXTURE1, 1);
	//shader->loadFloat(U_SCALE, glm::two_pi<float>());	//always night
	shader->loadFloat(U_SCALE, (cos(rotation) + 1.f) * 0.5f);	//one full rotation = one full day (pi = day (0) //// two_pi/0 = night (1))

	bind(SKYBOX_DAY);
	bind(SKYBOX_NIGHT);

	glBindVertexArray(vertexArrayObject);
	glDrawArrays(GL_TRIANGLES, 0, positions.size());
	glBindVertexArray(0);
	glDepthMask(1);
}

float SkyboxHandler::getRotation()
{
	return this->rotation;
}

void SkyboxHandler::loadCube()
{
	const float SIZE = 1000.f;	//SIZE of Quad
	positions.push_back({ -SIZE,  SIZE, -SIZE });
	positions.push_back({ -SIZE, -SIZE, -SIZE });
	positions.push_back({ SIZE, -SIZE, -SIZE });
	positions.push_back({ SIZE, -SIZE, -SIZE });
	positions.push_back({ SIZE,  SIZE, -SIZE });
	positions.push_back({ -SIZE,  SIZE, -SIZE });

	positions.push_back({ -SIZE, -SIZE,  SIZE });
	positions.push_back({ -SIZE, -SIZE, -SIZE });
	positions.push_back({ -SIZE,  SIZE, -SIZE });
	positions.push_back({ -SIZE,  SIZE, -SIZE });
	positions.push_back({ -SIZE,  SIZE,  SIZE });
	positions.push_back({ -SIZE, -SIZE,  SIZE });

	positions.push_back({ SIZE, -SIZE, -SIZE });
	positions.push_back({ SIZE, -SIZE,  SIZE });
	positions.push_back({ SIZE,  SIZE,  SIZE });
	positions.push_back({ SIZE,  SIZE,  SIZE });
	positions.push_back({ SIZE,  SIZE, -SIZE });
	positions.push_back({ SIZE, -SIZE, -SIZE });

	positions.push_back({ -SIZE, -SIZE,  SIZE });
	positions.push_back({ -SIZE,  SIZE,  SIZE });
	positions.push_back({ SIZE,  SIZE,  SIZE });
	positions.push_back({ SIZE,  SIZE,  SIZE });
	positions.push_back({ SIZE, -SIZE,  SIZE });
	positions.push_back({ -SIZE, -SIZE,  SIZE });

	positions.push_back({ -SIZE,  SIZE, -SIZE });
	positions.push_back({ SIZE,  SIZE, -SIZE });
	positions.push_back({ SIZE,  SIZE,  SIZE });
	positions.push_back({ SIZE,  SIZE,  SIZE });
	positions.push_back({ -SIZE,  SIZE,  SIZE });
	positions.push_back({ -SIZE,  SIZE, -SIZE });

	positions.push_back({ -SIZE, -SIZE, -SIZE });
	positions.push_back({ -SIZE, -SIZE,  SIZE });
	positions.push_back({ SIZE, -SIZE, -SIZE });
	positions.push_back({ SIZE, -SIZE, -SIZE });
	positions.push_back({ -SIZE, -SIZE,  SIZE });
	positions.push_back({ SIZE, -SIZE,  SIZE });
}
