#include "TerrainHandler.h"
#include "Engine.h"



TerrainHandler::TerrainHandler()
	:SIZE(1000), VERTEX_COUNT(128), LOWEST(-75.f)
{
}


TerrainHandler::~TerrainHandler()
{

}

void TerrainHandler::init()
{
	this->surface = parentEngine->getTextureHandler()->loadTexture("res/textures/grass.png");
	this->slopes = parentEngine->getTextureHandler()->loadTexture("res/textures/rock2.jpg");
	this->shader = parentEngine->getShaderHandler()->loadShader("res/shaders/terrainShader");
}

void TerrainHandler::generateTerrain(float gridX, float gridZ)
{
	std::cout << "Generating terrain at (" << gridX << ", " << gridZ << ")" << std::endl;
	x = gridX * SIZE;
	z = gridZ * SIZE;

	int count = VERTEX_COUNT * VERTEX_COUNT;

	std::vector<glm::vec3> positions;
	std::vector<glm::vec2> textureCoords;
	std::vector<glm::vec3> normals;
	std::vector<unsigned int> indices;

	glm::vec3 position;
	glm::vec2 texture;
	glm::vec3 normal;
	unsigned int index;
	for (int i = 0; i < VERTEX_COUNT; i++) {
		for (int j = 0; j < VERTEX_COUNT; j++) {

			float jj = static_cast<float>(j);
			float ii = static_cast<float>(i);
			float vertexCount = static_cast<float>(VERTEX_COUNT - 1);

			position.x = jj / vertexCount * SIZE + x;
			if (i == 0 || j == 0)
			{
				position.y = LOWEST - 25.f;
			}
			else
			{
				position.y = LOWEST + static_cast<float>((rand() % static_cast<int>(-LOWEST)) * 2.0f) - 25.f;
			}
			int nothing = 0;
			
			position.z = ii / vertexCount * SIZE + z;
			positions.push_back(position);

			normal.x = 0;
			normal.y = 1;
			normal.z = 0;
			normals.push_back(normal);

			texture.x = jj / vertexCount;
			texture.y = ii / vertexCount;
			textureCoords.push_back(texture);
		}
	}	

	for (int gz = 0; gz < VERTEX_COUNT - 1; gz++) {
		for (int gx = 0; gx < VERTEX_COUNT - 1; gx++) {
			int topLeft = (gz*VERTEX_COUNT) + gx;
			int topRight = topLeft + 1;
			int bottomLeft = ((gz + 1)*VERTEX_COUNT) + gx;
			int bottomRight = bottomLeft + 1;

			index = topLeft;
			indices.push_back(index);

			index = bottomLeft;
			indices.push_back(index);

			index = topRight;
			indices.push_back(index);

			index = topRight;
			indices.push_back(index);

			index = bottomLeft;
			indices.push_back(index);

			index = bottomRight;
			indices.push_back(index);
		}
	}

	for (int j = 0; j < 4; j++)
	{
		for (std::size_t i = 1; i < positions.size()-1; i++)
		{
			positions[i].y = (positions[i - 1].y + positions[i + 1].y) / 2;
		}
		unsigned int offset = positions.size() / SIZE;
		for (size_t i = offset; i < positions.size() - offset; i++)
		{
			positions[i].y = ((positions[i + offset].y) + (positions[i - offset].y)) / 2;
		}
	}

	IndexedModel model;
	model.positions = positions;
	model.texCoords = textureCoords;
	model.normals = normals;
	model.indices = indices;

	model.CalcNormals();

	drawCount = indices.size();
	
	glGenVertexArrays(1, &vertexArrayObject);
	glBindVertexArray(vertexArrayObject);

	glGenBuffers(NUM_BUFFERS, vertexArrayBuffer);

	glBindBuffer(GL_ARRAY_BUFFER, vertexArrayBuffer[POSITION_VB]);
	glBufferData(GL_ARRAY_BUFFER, model.positions.size()*sizeof(model.positions[0]), &model.positions[0], GL_STATIC_DRAW);

	glEnableVertexAttribArray(0);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, 0);

	glBindBuffer(GL_ARRAY_BUFFER, vertexArrayBuffer[TEXCOORD_VB]);
	glBufferData(GL_ARRAY_BUFFER, model.positions.size()*sizeof(model.texCoords[0]), &model.texCoords[0], GL_STATIC_DRAW);

	glEnableVertexAttribArray(1);
	glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 0, 0);

	glBindBuffer(GL_ARRAY_BUFFER, vertexArrayBuffer[NORMAL_VB]);
	glBufferData(GL_ARRAY_BUFFER, model.normals.size()*sizeof(model.normals[0]), &model.normals[0], GL_STATIC_DRAW);

	glEnableVertexAttribArray(2);
	glVertexAttribPointer(2, 3, GL_FLOAT, GL_FALSE, 0, 0);

	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vertexArrayBuffer[INDEX_VB]);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, model.indices.size()*sizeof(model.indices[0]), &model.indices[0], GL_STATIC_DRAW);

	glBindVertexArray(0);
	std::cout << "Terrain generated." << std::endl;
}

void TerrainHandler::update()
{
	float rotation = parentEngine->getSkyboxHandler(1)->getRotation();
	rotation -= glm::half_pi<float>();
	float posX = cos(rotation) * this->sunDistance;
	float posY = sin(rotation) * this->sunDistance;
	this->sunPosition = glm::vec4(posX, posY, 0.f, 1.f);
	this->transform.getRot().y = rotation;
}

void TerrainHandler::draw(glm::mat4 & viewProjection)
{
	this->shader->bind();
	this->shader->loadTransform(transform, viewProjection);
	this->shader->loadMat4(U_VIEW, viewProjection);
	this->shader->loadVec4(U_LIGHT_POS, this->sunPosition);
	this->shader->loadInt(U_TEXTURE0, 0);
	this->shader->loadInt(U_TEXTURE1, 1);
	this->surface->bind(0);
	this->slopes->bind(1);

	//draw
	glBindVertexArray(vertexArrayObject);
	glDrawElements(GL_TRIANGLES, drawCount, GL_UNSIGNED_INT, 0);
	glBindVertexArray(0);
}
