#pragma once
#include <string>
#include <fstream>
#include <glm\glm.hpp>
#include <sstream>
#include <vector>
#include "ActorFactory.h"
#include "PhysicsHandler.h"
#ifdef __ANDROID__
#include <android\log.h>
#endif

/**
 * @class	LevelLoader
 *
 * @brief	A level loader. will probably be rewritten at some point
 * @details	The levelloader is not part of the engine, and is only used for demonstrationpurposes and 
 * 			calling the createActor method. any custom levelloader can create actors
 * 			
 */
class LevelLoader
{
public:
	LevelLoader();
	void setActorFactory(ActorFactory* actorFactory);
	void setPhysicsHandler(PhysicsHandler* physicsHandler);
	void loadLevel(int level);

#ifdef __ANDROID__
	void loadChunk(const std::string& filename, int chunkX, int chunkY, krem::android::AssetManager * assetmgr);
#else
	void loadChunk(const std::string& filename, int chunkX, int chunkY);
#endif
	void loadForeground();
	void loadBackground();
	void loadCollision();
	void loadProps();
	void loadMinotaur();
	void loadCyclop();

	void loadGroundPlayer();
	int getTilePos(int number, std::string position);
	
public:
	std::vector<glm::vec3> tilePositions;
	std::vector<glm::vec3> collisionTilePositions;

private:
	ActorFactory* actorFactory;
	PhysicsHandler* physicsHandler;
	std::string name;
	std::string aux;
	std::string tempString;
	int content = 0;

	int levelWidth = 0;
	int levelHeight = 0;
	int tileWidth = 0;
	int tileHeight = 0;
	int levelSize = 0;
	int chunkNumberX = 0;
	int chunkNumberY = 0;
	std::vector<CollisionData> collisionDataVector;

#ifdef __ANDROID__
	std::istringstream levelfile;
	std::vector<char> getLevelString(std::string path, krem::android::AssetManager * assetmgr);
#else
	std::ifstream levelfile;
#endif
};