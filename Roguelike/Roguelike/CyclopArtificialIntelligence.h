#pragma once
#include "IAIComponent.h"
#include "Timer.h"

class PhysicsComponent;
class ActorFactory;

class CyclopArtificialIntelligence : public IAIComponent
{
public:
	CyclopArtificialIntelligence(tinyxml2::XMLElement *xmlElement);
	void update(float deltaTime) override;
private:
	Actor* player;
	Timer throwCooldown;
	Timer attackCooldown;
	Timer walkCooldown;
	PhysicsComponent* physicsComponent;
	bool attacking;
	bool throwing;
	bool walking;
	ActorFactory* actorFactory;
};