#pragma once
#include <string>
#include <memory>
#include <map>
#include <vector>
#include "Transform.h"
#include "Mesh.h"
#include "Texture.h"
#include "Shader.h"
#include "tinyxml2-master\tinyxml2.h"
#include <glm\glm.hpp>
#include "Font.h"
class Engine;
class GuiState;

/** A panel. Can be used to display a panel in game. For example a panel behind buttons or really anything you just want to display as a square with a texture in gui */
class Panel
{
public:
	Panel(tinyxml2::XMLElement *xmlElement);
	~Panel();

	/**
	 * Draws the given view projection.
	 *
	 * @param [in,out]	viewProjection	The view projection.
	 */
	void draw(glm::mat4 &viewProjection);
	/** Updates this object. */
	void update();

	/**
	 * Initialises this object.
	 *
	 * @param [in,out]	parentEngine	If non-null, the parent engine.
	 */
	void init(Engine* parentEngine);

	/**
	 * Sets a parent./
	 *
	 * @param [in,out]	parentPtr	The parent pointer.
	 */
	void setParent(GuiState& parentPtr);

	/**
	 * Gets the parent of this item.
	 *
	 * @return	null if it fails, else the parent.
	 */

	void changeTexture(std::string texturePath);

	GuiState* getParent();
	std::string getName();
private:
	GuiState* parentGuiState;
	Engine* parentEngine;

	tinyxml2::XMLElement *originNode;

	std::string name;

	Transform transform;
	Mesh *mesh;
	Shader *shader;
	Texture *texture;
};

