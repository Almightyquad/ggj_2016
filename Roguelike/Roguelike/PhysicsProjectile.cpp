#include "PhysicsProjectile.h"
#include "Engine.h"
void PhysicsProjectile::onCollision(int fixtureUserData, Actor * collidedActor)
{
   	switch (fixtureUserData)
	{
	case CATEGORY_ENEMY:
		dynamic_cast<CombatComponent*>(parentActor->getComponent("CombatComponent"))->kill();
		parentActor->onHit();
		break;
	case CATEGORY_SCENERY:
		dynamic_cast<CombatComponent*>(parentActor->getComponent("CombatComponent"))->kill();
		parentActor->onHit();
		break;
	default:
		break;
	}
}
