#pragma once
#include <unordered_map>
#include <string>
#include <iostream>
#include <fstream>
#include <algorithm>
class ChunkGenerator
{
public:
	ChunkGenerator(int chunkSizeX, int chunkSizeY);
	~ChunkGenerator();
	void addElement(std::string name, int id, std::string layer);
	//the order goes y,x not the regular x,y
	void addPrefab(std::vector<std::vector<int>> prefab);
	bool loadPrefab(std::string filepath);
	std::vector<std::vector<int>> generateChunk();
	std::unordered_map<int, std::string> getElements(std::string layer);

private:
	int chunkSizeX;
	int chunkSizeY;
	std::vector<std::vector<std::vector<int>>> prefabVec;
	std::unordered_map<int, std::string> background;
	std::unordered_map<int, std::string> middleground;
	std::unordered_map<int, std::string> foreground;

	void findIdsInChunk(std::vector<std::vector<int>> chunkVec, int idToFind, std::vector<int> & vecPosX, std::vector<int> & vecPosY);
	int checkPrefabSpace(std::vector<int> vecPosX, std::vector<int> vecPosY, int & randPrefab);
	void checkElementMapForKey(std::unordered_map<int, std::string> & elementMap, std::string layer, int id, std::string name, std::string desiredLayer);
};

