#include "GuiHandler.h"
#include "Engine.h"
using namespace tinyxml2;

GuiHandler::GuiHandler():
	loaderFile("res/gui/guiloaderfile.xml")
{
}

GuiHandler::~GuiHandler()
{
}

void GuiHandler::loadStatesFromFiles()
{
	XMLDocument * xmlDoc = parentEngine->getXmlHandler()->loadXml(loaderFile);
	XMLElement * itemElement;
	itemElement = xmlDoc->FirstChildElement();
	itemElement = itemElement->FirstChildElement("Guistate");
	std::string temp = itemElement->Value();
	while (temp == "Guistate")
	{
		temp = itemElement->GetText();
		std::cout << temp;
		createState("res/gui/states/" + temp + ".xml");
		itemElement = itemElement->NextSiblingElement();
		if (itemElement != NULL)
		{
			temp = itemElement->Value();
		}
		else
		{
			temp = "end of Guistate";
		}
	}
}
//creates a new GUI state. I.e main menu, ingame, option screen by reading xml files
void GuiHandler::createState(std::string xmlPath)
{
	doc = parentEngine->getXmlHandler()->loadXml(xmlPath);
	std::string temp = xmlPath;
	if (doc == NULL)
	{
		std::cout << "Doc == NULL\n";
	}
	else 
	{
		XMLElement* rootElement = doc->FirstChildElement();
		std::unique_ptr<GuiState> newState = std::make_unique<GuiState>(rootElement);
		addChild(newState.get());
		newState->init(parentEngine);
		
		states.emplace_back(std::move(newState));
	}
}

void GuiHandler::draw(int gameState)
{
	//Enable this to get UI in perspective to look good on any monitor, no matter aspect ratio

	/*glm::mat4 projectionMatrix = glm::perspective(70.f, 16.f/9.f, 0.01f, 1000.f);
	glm::vec3 position = { 0.f, 0.f, 15.f };
	glm::vec3 forward = { 0, 0, -1 };
	glm::vec3 up = { 0, 1, 0 };
	glm::mat4 viewMatrix = glm::mat4(glm::lookAt(position, position + forward, up));*/

	glm::mat4 staticCamera =	//don't want UI to float in 3D space but locked to view projection
	{
	1.f, 0, 0, 0,
	0, 1.f, 0, 0,
	0, 0, -1.f, 0,
	0, 0, 0, 1.f
	};

	//staticCamera = projectionMatrix * viewMatrix;
	glDisable(GL_DEPTH);
	for (std::size_t i = 0; i < states.size(); i++)
	{
		if (i == parentEngine->getGameState())
		{
			states[i]->draw(staticCamera);
		}
	}
	glEnable(GL_DEPTH);
}

void GuiHandler::update()
{
	for (std::size_t i = 0; i < states.size(); i++)
	{
		if (i == parentEngine->getGameState())
		{
			states[i]->update();
		}
	}
	
}

void GuiHandler::displayContainer(std::string name)
{
	for (std::size_t i = 0; i < states.size(); i++)
	{
		states[i]->displayContainer(name);
	}
}

void GuiHandler::setContainerValue(std::string name, int box)
{
	for (std::size_t i = 0; i < states.size(); i++)
	{
		states[i]->setContainerValue(name, box);
	}
}

void GuiHandler::setTextureContainer(int slot, std::string name, std::string texturePath)
{
	for (std::size_t i = 0; i < states.size(); i++)
	{
		states[i]->setTextureContainer(slot, name, texturePath);
	}
}

void GuiHandler::setTextureContainerBox(std::string name, std::string texturePath)
{
	for (std::size_t i = 0; i < states.size(); i++)
	{
		states[i]->setTextureContainerBox(name, texturePath);
	}
}

void GuiHandler::setPanelTexture(std::string name, std::string texturePath)
{
	for (std::size_t i = 0; i < states.size(); i++)
	{
		states[i]->setTexturePanel(name, texturePath);
	}
}

void GuiHandler::setBarBackground(std::string name, std::string texturePath)
{
	for (std::size_t i = 0; i < states.size(); i++)
	{
		states[i]->setBarBackground(name, texturePath);
	}
}

void GuiHandler::setBarColor(std::string name, glm::vec4 changeColor)
{
	for (std::size_t i = 0; i < states.size(); i++)
	{
		states[i]->setBarColor(name, changeColor);
	}
}

void GuiHandler::setBarTexture(std::string name, std::string texturePath)
{
	for (std::size_t i = 0; i < states.size(); i++)
	{
		states[i]->setBarTexture(name, texturePath);
	}
}

void GuiHandler::setBarValue(std::string name, float size)
{
	for (std::size_t i = 0; i < states.size(); i++)
	{
		states[i]->setBarValue(name, size);
	}
}

void GuiHandler::setBarMaxValue(std::string name, float size)
{
	for (std::size_t i = 0; i < states.size(); i++)
	{
		states[i]->setBarMaxValue(name, size);
	}
}

bool GuiHandler::checkMouseClickLeft()
{
	for (std::size_t i = 0; i < states.size(); i++)
	{
		if (i == parentEngine->getGameState())
		{
			return states[parentEngine->getGameState()]->checkMouseClickLeft();
		}
	}
}

bool GuiHandler::checkMouseClickRight()
{
	for (std::size_t i = 0; i < states.size(); i++)
	{
		if (i == parentEngine->getGameState())
		{
			return states[parentEngine->getGameState()]->checkMouseClickRight();
		}
	}
	return false;
}

bool GuiHandler::checkMouseRelease()
{
	for (std::size_t i = 0; i < states.size(); i++)
	{
		if (i == parentEngine->getGameState())
		{
			return states[parentEngine->getGameState()]->checkMouseRelease();
		}
	}
	return false;
}

Bar * GuiHandler::getBar(std::string name)
{
	Bar *temp;
	for (std::size_t i = 0; i < states.size(); i++)
	{
		temp = states[i]->findBar(name);
		if (temp != nullptr)
		{
			return temp;
		}
	}
	return nullptr;
}

Button* GuiHandler::getButton(std::string name)
{
	Button *temp;
	for (std::size_t i = 0; i < states.size(); i++)
	{
		temp = states[i]->findButton(name);
		if (temp != nullptr)
		{
			return temp;
		}
	}
	return nullptr;
}

Slider* GuiHandler::getSlider(std::string name)
{
	Slider *temp;
	for (std::size_t i = 0; i < states.size(); i++)
	{
		temp = states[i]->findSlider(name);
		if (temp != nullptr)
		{
			return temp;
		}
	}
	return nullptr;
}

TextBox * GuiHandler::getTextBox(std::string name)
{
	TextBox *temp;
	for (std::size_t i = 0; i < states.size(); i++)
	{
		temp = states[i]->findTextBox(name);
		if (temp != nullptr)
		{
			return temp;
		}
	}
	return nullptr;
}

CheckBox * GuiHandler::getCheckBox(std::string name)
{
	CheckBox *temp;
	for (std::size_t i = 0; i < states.size(); i++)
	{
		temp = states[i]->findCheckBox(name);
		if (temp != nullptr)
		{
			return temp;
		}
	}
	return nullptr;
}

DropDown * GuiHandler::getDropDown(std::string name)
{
	DropDown *temp;
	for (std::size_t i = 0; i < states.size(); i++)
	{
		temp = states[i]->findDropDown(name);
		if (temp != nullptr)
		{
			return temp;
		}
	}
	return nullptr;
}

Container * GuiHandler::getContainer(std::string name)
{
	Container *temp;
	for (std::size_t i = 0; i < states.size(); i++)
	{
		temp = states[i]->findContainer(name);
		if (temp != nullptr)
		{
			return temp;
		}
	}
	return nullptr;
}

void GuiHandler::clear(int gameState)
{
	for (std::size_t i = 0; i < states.size(); i++)
	{
		if (i == gameState)
		{
			states[gameState]->clear();
		}
	}
}


int GuiHandler::getNextGuiId(void)
{
	lastGuiId++;
	return lastGuiId;
	
}

void GuiHandler::addChild(GuiState* guiState)
{
	guiState->setParent(*this);
}
