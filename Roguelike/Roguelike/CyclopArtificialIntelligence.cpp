#include "CyclopArtificialIntelligence.h"
#include "Engine.h"
#include "PhysicsComponent.h"

CyclopArtificialIntelligence::CyclopArtificialIntelligence(tinyxml2::XMLElement * xmlElement):
	IAIComponent(xmlElement),
	attackCooldown(3),
	throwCooldown(5),
	walkCooldown(3)
{
}

void CyclopArtificialIntelligence::update(float deltaTime)
{
	if (player == nullptr)
	{
		player = parentEngine->getPlayer();
		physicsComponent = dynamic_cast<PhysicsComponent*>(parentActor->getComponent("PhysicsComponent"));
		attackCooldown.start();
		walkCooldown.start();
		throwCooldown.start();
		actorFactory = parentEngine->getActorFactory();
	}
	float distanceToPlayer = glm::length<float>(player->getPosition() - parentActor->getPosition());
	if (distanceToPlayer < 3 && attackCooldown.hasEnded())
	{
		walking = false;
		throwing = false;
		attacking = true;
		attackCooldown.restart();
		walkCooldown.restart();
	}
	if (distanceToPlayer > 6 && distanceToPlayer < 9 && throwCooldown.hasEnded())
	{
		walking = false;
		throwing = true;
		attacking = false;
		throwCooldown.restart();
		walkCooldown.restart();
	}

	if (!walking && walkCooldown.hasEnded())
	{
		walking = true;
	}

	if ((player->getPosition().x - parentActor->getPosition().x) > 0)
	{
		if (throwing)
		{
			//physicsComponent->applyLinearImpulse({ 4.f, 1.5f });
			actorFactory->createActor("res/actors/cyclopRock.xml", glm::vec3(parentActor->getPosition().x + 0.5, parentActor->getPosition().y + 2, 0.f), { 7.5f,4.f,0.f });
			throwing = false;
			physicsComponent->setVelocity({ 0.f,0.f,0.f });
		}
		if (attacking)
		{
			attacking = false;
			physicsComponent->setVelocity({ 0.f,0.f,0.f });
		}
		if (walking)
		{
			physicsComponent->applySpeed({ 0.5f,0.f });
		}
	}
	else
	{
		if (throwing)
		{
			actorFactory->createActor("res/actors/cyclopRock.xml", glm::vec3(parentActor->getPosition().x - 0.5, parentActor->getPosition().y + 2, 0.f), { -7.5,4.f,0.f });
			throwing = false;
			physicsComponent->setVelocity({ 0.f,0.f,0.f });
		}
		if (attacking)
		{
			attacking = false;
			physicsComponent->setVelocity({ 0.f,0.f,0.f });
		}
		if (walking)
		{
			physicsComponent->applySpeed({ -0.5f,0.f });
		}
	}
}
