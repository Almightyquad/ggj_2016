#pragma once
#include "IPickUpComponent.h"

class PickUpGrenade final : public IPickUpComponent
{
public:
	PickUpGrenade(tinyxml2::XMLElement *xmlElement);
	virtual void onToss() override;
	virtual void onActivation() override;
	virtual void onDeactivation() override;
	virtual void fire(krem::Vector2f mousePos) override;

private:
	Timer grenadeTimer;
	Actor* grenadeActor;
};