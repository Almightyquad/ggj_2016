#pragma once
#include <string>
#include <memory>
#include <map>
#include <vector>
#include "Transform.h"
#include "Mesh.h"
#include "Texture.h"
#include "Shader.h"
#include "tinyxml2-master\tinyxml2.h"
#include <glm\glm.hpp>
#include "Font.h"
#include "Observable.h"
#include "Receiver.h"

class Engine;
class GuiState;

/** A container. Used to display a panel with slots inside. Can be used for either a inventory like container where you can move stuff around or a kind of selection screen */
class Container : public IObservable, public IReceiver
{
public:
	Container(tinyxml2::XMLElement *xmlElement);
	~Container();

	void setBoxTexture(std::string texturePath);

	/** Updates this object. */
	void update();

	/**
	 * Determines if mouse click left is inside the container or a container slot.
	 *
	 * @return	true if it succeeds, false if it fails.
	 */
	bool checkMouseClickLeft();

	/**
	 * Determines if mouse click right is inside a container slot.
	 *
	 * @return	true if it succeeds, false if it fails.
	 */
	bool checkMouseClickRight();

	/**
	 * runs if the container has movable slots inside. I.E swapping items in an inventory.
	 *
	 * @return	true if it succeeds, false if it fails.
	 */
	bool movableMouseClick();

	/**
	 * Determines if mouse release is inside the container or a slot.
	 *
	 * @return	true if it succeeds, false if it fails.
	 */
	bool checkMouseRelease();

	/**
	 * runs if the container has movable slots.
	 *
	 * @return	true if it succeeds, false if it fails.
	 */
	bool movableMouseRelease();

	/**
	 * Sort items in the container
	 *
	 * @param	item1	The first item.
	 * @param	item2	The second item.
	 */
	void sortItems(int item1, int item2);

	/**
	 * Swap items in the container
	 *
	 * @param	item1	The first item.
	 * @param	item2	The second item.
	 */
	void swapItems(int item1, int item2);

	void setSelectedBox(int box);
	
	/**
	 * Receives message. Used to receive messages from our messaging system.
	 *
	 * @param	message	The message.
	 */
	void receiveMessage(const Message &message);

	/**
	 * Draws the given view projection.
	 *
	 * @param [in,out]	viewProjection	The view projection.
	 */
	void draw(glm::mat4 &viewProjection);

	/**
	 * Initialises this object.
	 *
	 * @param [in,out]	parentEngine	If non-null, the parent engine.
	 */
	void init(Engine* parentEngine);

	/** Flip visible. Shows or hides the container**/
	void flipVisible();
	/** Clears this object to its blank/initial state. */
	void clear();

	void setTexture(int slot, std::string texturePath);

	/**
	 * Sets a parent.
	 *
	 * @param [in,out]	parentPtr	The parent pointer.
	 */
	void setParent(GuiState& parentPtr);

	/**
	 * Gets the parent of this item.
	 *
	 * @return	null if it fails, else the parent.
	 */
	GuiState* getParent();

	/**
	 * Gets the name.
	 *
	 * @return	The name.
	 */
	std::string getName();
private:
	GuiState* parentGuiState;
	Engine* parentEngine;

	tinyxml2::XMLElement *originNode;

	std::string name;

	bool visible;
	bool clickable;
	bool movableItems;
	bool destroyable;
	bool activatable;

	int selectedBox;
	int activatedBox;
	int numberOfActors;
	int numberOfPresetItems;
	int firstFreeSlot;
	int textureAtlasSizeX;
	int textureAtlasSizeY;
	int numberOfBoxes;
	int numberOfBoxesX;
	int numberOfBoxesY;

	float distanceBetweenBoxes;

	float borderTop;
	float borderBottom;
	float borderLeft;
	float borderRight;

	Transform transform;
	std::vector<Transform> transformBoxes;
	std::vector<Transform> transformContainedItems;
	Mesh *mesh;

	Shader *shader;
	Shader *shaderBoxes;

	Texture *textureContainer;
	Texture *textureBoxes;

	std::map<int, Texture*> containedItems;
};

