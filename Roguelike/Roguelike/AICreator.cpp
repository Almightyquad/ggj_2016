#include "AICreator.h"
#include "AIFactory.h"

AICreator::AICreator(const std::string & classname, AIFactory* aIFactory)
{
	aIFactory->registerIt(classname, this);
}
