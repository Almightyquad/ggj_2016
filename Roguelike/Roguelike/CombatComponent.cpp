#include "CombatComponent.h"
#include "Actor.h"
#include "Engine.h"

CombatComponent::CombatComponent(tinyxml2::XMLElement * xmlElement)
	: ICombatComponent(xmlElement),
	invincibilityTimer(0.5f)
{
	invincibilityTimer.start();
	timer.setDuration(1);
	maxHealth = 0;
	maxMana = 0;
	damage = 0;
	health = 0;
	mana = 0;
}

CombatComponent::~CombatComponent()
{
}

void CombatComponent::init(Engine * parentEngine)
{
	this->parentEngine = parentEngine;

	if (this->originNode->FirstChildElement("health"))
	{
		this->originNode->FirstChildElement("health")->QueryFloatText(&health);
		maxHealth = health;
	}
	else
	{
		health = 1;
		if (xmlDebug)
		{
			printf("Missing health. default value = %i\n", health);

		}
	}
	if (this->originNode->FirstChildElement("mana"))
	{
		this->originNode->FirstChildElement("mana")->QueryIntText(&mana);
		maxMana = mana;
	}
	else
	{
		if (xmlDebug)
		{
			printf("Missing mana\n");

		}
	}
	if (this->originNode->FirstChildElement("lifeTime"))
	{
		this->originNode->FirstChildElement("lifeTime")->QueryFloatText(&lifetime);
		timer.setDuration(static_cast<int>(lifetime));
		timer.start();
	}
	else 
	{
		timer.start();
		timer.pause();
		if (xmlDebug)
		{
			printf("Missing lifeTime\n");
		}
	}
	if (this->originNode->FirstChildElement("damage"))
	{
		this->originNode->FirstChildElement("damage")->QueryFloatText(&damage);
	}
	else if (xmlDebug)
	{
		printf("Missing damage");
	}
}

void CombatComponent::update(float deltaTime)
{
	if (timer.hasEnded() || health <= 0)
	{
		actorID temp = parentActor->getActorId();
		if (temp.actorName == "Player")
		{
			parentEngine->setGameState(STATE_GAMEOVER);
		}
		else
		{
			parentActor->onDeath();
			parentEngine->getActorFactory()->addToDeleteQueue(parentActor); //should rewrite this somehow
		}
	}
}

void CombatComponent::reduceHealth(float damage)
{
	if (invincibilityTimer.hasEnded())
	{
		health -= damage;
		if (health < 0)
		{
			health = 0;
		}
		invincibilityTimer.restart();
	}
	
}

void CombatComponent::reduceMana(int manaCost)
{
	mana -= manaCost;
	if (mana < 0)
	{
		mana = 0;
	}
}

void CombatComponent::increaseHealth(float healthValue)
{
	health += healthValue;
	if (health > maxHealth)
	{
		health = maxHealth;
	}
}

void CombatComponent::increaseMana(int manaValue)
{
	mana += manaValue;
	if (mana > maxMana)
	{
		mana = maxMana;
	}
}

int CombatComponent::getMana()
{
	return mana;
}

int CombatComponent::getHealth()
{
	return health;
}

int CombatComponent::getDamage()
{
	return damage;
}

void CombatComponent::kill()
{
	health = 0;
	parentActor->onDeath();
	parentEngine->getActorFactory()->addToDeleteQueue(parentActor); //should rewrite this somehow
}
