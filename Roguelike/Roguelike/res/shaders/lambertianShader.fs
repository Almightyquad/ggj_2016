#version 330 core

in vec2 texCoord0;
in vec3 normal0;

out vec4 color;

uniform sampler2D texture0;
uniform vec4 lightPosition;

void main()
{
	color = texture2D(texture0, texCoord0) * clamp(dot(-lightPosition.rgb, normal0), 0.5, lightPosition.a);
}