#version 330 core

layout(location = 0) in vec3 vertex;
layout(location = 1) in vec3 positions;
layout(location = 2) in vec4 colors;
layout(location = 3) in vec2 texCoords;

out vec2 texCoord;
out vec4 color;

uniform mat4 transform;

void main()
{
	color = colors;
	texCoord = texCoords;
	gl_Position = transform * vec4(vertex + positions, 1.0);
}