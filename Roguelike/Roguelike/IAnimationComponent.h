#pragma once
#include "ActorComponent.h"
#include "tinyxml2-master\tinyxml2.h"
#include <unordered_map>
#include "Animation.h"

class IAnimationComponent : public ActorComponent
{
public:
	IAnimationComponent(tinyxml2::XMLElement *xmlElement)
		: ActorComponent(xmlElement) {};
	virtual ~IAnimationComponent() {};

protected:
	//struct Animation
	//{
	//	int firstFrame = -1;
	//	int amountOfFrames = -1;
	//	float animationSpeed = -1;
	//};
	

	std::unordered_map<int, Animation> animations;
};
