#include "NetworkServerHandler.h"
#include "Engine.h"
#include "Actor.h"
#include "Message.h"

NetworkServerHandler::NetworkServerHandler()
{
}

NetworkServerHandler::~NetworkServerHandler()
{
	for (auto& it : clients)
	{
		SDLNet_TCP_Close(it->socket);
	}
	//SDLNet_TCP_Close(socket);
	SDLNet_Quit();
}

void NetworkServerHandler::init()
{
	socketSet = SDLNet_AllocSocketSet(31);
	if (SDLNet_Init() < 0)
	{
		printf("SDLNET_init:%s\n", SDLNet_GetError());
	}
	
	if (SDLNet_ResolveHost(&serverIP, NULL, 2000) < 0)
	{
		printf("SDLNET_ResolveHost:%s\n", SDLNet_GetError());
	}
	
	//Opens a connection which will act as a listener
	if (!(socket = SDLNet_TCP_Open(&serverIP))) 
	{
		printf("SDLNET_TCP_Open:%s\n", SDLNet_GetError());
	}
	//oldActors = parentEngine->getActorFactory()->getActors();
}

void NetworkServerHandler::initClients()
{
	SDLNet_TCP_Close(socket);
	for (auto& it : clients)
	{
		unsigned int randomSeed = parentEngine->getSeed();
		if (SDLNet_TCP_Send(it->socket, &randomSeed, sizeof(unsigned int)) < sizeof(unsigned int))
		{
			printf("SDLNet_TCP_Send Start Game: %s\n", SDLNet_GetError());
		}
		else if (networkDebug)
		{
			printf("succeeded sending random seed from host \n");
		}

		//receive selectedCharacter id based on selection from client
		int selectedCharacter;
		if ((SDLNet_TCP_Recv(it->socket, &selectedCharacter, sizeof(int))) <= 0)
		{
			printf("SDLNet_TCP_Recv game start error: %s\n", SDLNet_GetError());
		}
		else
		{
			std::string characterXml = "res/actors/player" + std::to_string(selectedCharacter) + ".xml";
			it->actor = parentEngine->getActorFactory()->createActor(characterXml, glm::vec3(5.f, -3.f, 0.f));
		}

		//send new actorID to create actor on client
		if (SDLNet_TCP_Send(it->socket, &it->actor->getActorId(), sizeof(actorID)) < sizeof(actorID))
		{
			printf("SDLNet_TCP_Send actorID: %s\n", SDLNet_GetError());
		}
		else if (networkDebug)
		{
			printf("succeeded sending -player actorID- packet \n");
		}
	}
}

void NetworkServerHandler::listen()
{
	if (newClient == nullptr)
	{
		newClient = std::make_unique<Client>();
	}

	if (newClient->socket = SDLNet_TCP_Accept(socket))
	{
		if ((newClient->IP = SDLNet_TCP_GetPeerAddress(newClient->socket)))
		{
			numberOfClients++;
			printf("Host connected: %x %d\n", SDLNet_Read32(&newClient->IP->host), SDLNet_Read16(&newClient->IP->port));

			//send random seed for world generation
			int numused = SDLNet_TCP_AddSocket(socketSet, newClient->socket);

			clients.emplace_back(std::move(newClient));
		}
	}
}

void NetworkServerHandler::sendPacket(int gameState)
{
	if (networkDebug)
	{
		printf("starting to send packet from server\n");
	}
		switch (gameState)
	{
	case STATE_HOSTING:
		sendHosting();
		return;
	case STATE_RUNNING:
		sendRunning();
		break;
	default:
		printf("invalid gamestate for network play");
		break;
	}
}


void NetworkServerHandler::receivePacket(int gameState)
{	
	if (networkDebug)
	{
		printf("starting to receive packet on server\n");
	}
	
	switch (gameState)
	{
	case STATE_HOSTING:
		receiveHosting();
		break;
	case STATE_RUNNING:
		receiveRunning();
		break;
	default:
		printf("invalid gamestate for network play");
		break;
	}
}

void NetworkServerHandler::receiveHosting()
{
	//actorID actorId;
	//for (auto& it : clients)
	//{
	//	if ((SDLNet_TCP_Recv(socket, &actorId, sizeof(actorID))) <= 0)
	//	{
	//		printf("SDLNet_TCP_Recv game start error: %s\n", SDLNet_GetError());
	//	}
	//	else
	//	{
	//		it->actor = parentEngine->getActorFactory()->createActor(actorId.actorName, glm::vec3(5.f, -3.f, 0.f));
	//	}
	//}
}

void NetworkServerHandler::receiveRunning()
{
	InputValues receivedInput;
	if (SDLNet_CheckSockets(socketSet, 1))
	{
		for (auto& it : clients)
		{
			int size = SDLNet_TCP_Recv(it->socket, &receivedInput, sizeof(InputValues));
			if (size > 0)
			{
				IInputComponent* actorPhysics = dynamic_cast<IInputComponent*>(it->actor->getComponent("InputComponent"));
				//actorPhysics->receiveNetworkInput(receivedInput);//fix when we fix network
			}

			if (networkDebugTwo)
			{
				//I think there's an issue with inputcomponent where some shit isn't reset, which is why these outputs are wrong in certain cases
				printf("received packet I think\n:");
				printf(receivedInput.w ? "w: true\n" : "w: false\n");
				printf(receivedInput.s ? "s: true\n" : "s: false\n");
				printf(receivedInput.a ? "a: true\n" : "a: false\n");
				printf(receivedInput.d ? "d: true\n" : "d: false\n");
				printf(receivedInput.q ? "q: true\n" : "q: false\n");
				printf(receivedInput.space ? "space: true\n" : "space: false\n");
				printf(receivedInput.mouse ? "mouse: true\n" : "mouse: false\n");
				printf("mousepos: %f, %f", receivedInput.mousePosition.x, receivedInput.mousePosition.y);
			}
		}
	}
}

void NetworkServerHandler::sendHosting()
{
}

void NetworkServerHandler::sendRunning()
{
	std::vector<NetworkActorData> serverData;
	std::vector<NetworkActorData> itemData;
	int numberOfActors;
	int sentData;
	int datasize;

	oldActors = parentEngine->getActorFactory()->getActors();
	for (auto it : oldActors)
	{
		//if (it->isActive())
		{
			NetworkActorData newNetworkData;
			packActorId(&newNetworkData, it);
			newNetworkData.actorPosition = { static_cast<PhysicsComponent*>(it->getComponent("PhysicsComponent"))->getPosition().x,
												static_cast<PhysicsComponent*>(it->getComponent("PhysicsComponent"))->getPosition().y };
			newNetworkData.actorVelocity = { static_cast<PhysicsComponent*>(it->getComponent("PhysicsComponent"))->getVelocity().x,
												static_cast<PhysicsComponent*>(it->getComponent("PhysicsComponent"))->getVelocity().y };
			serverData.emplace_back(newNetworkData);
		}
	}

	numberOfActors = serverData.size();

	datasize = sizeof(NetworkActorData);
	for (auto& itClients : clients)
	{
		sentData = SDLNet_TCP_Send(itClients->socket, &numberOfActors, sizeof(int));
		if (sentData < sizeof(int))
		{
			printf("SDLNet_TCP_Send1: %s\n", SDLNet_GetError());
		}
		else if (networkDebug)
		{
			printf("succeeded sending packet1 nr: \n");
		}

		for (auto& itActors : serverData)
		{
			sentData = SDLNet_TCP_Send(itClients->socket, &itActors, datasize);
			if (sentData < datasize)
			{
				printf("SDLNet_TCP_Send2: %s\n", SDLNet_GetError());
			}
			else if (networkDebug)
			{
				printf("succeeded sending packet2 nr: \n");
			}
		}
		
		//std::map<int, Actor*> clientItems = dynamic_cast<InventoryComponent*>(itClients->actor->getComponent("InventoryComponent"))->getItems();
		//int numberOfItems = clientItems.size();
		//sentData = SDLNet_TCP_Send(itClients->socket, &numberOfItems, sizeof(int));
		//if (sentData < sizeof(int))
		//{
		//	printf("SDLNet_TCP_Send2: %s\n", SDLNet_GetError());
		//}
		//else if (networkDebug)
		//{
		//	printf("succeeded sending packet3 nr: \n");
		//}
		/*for (auto itItems : clientItems)
		{
			NetworkActorData newItemData;
			packActorId(&newItemData, itItems.second);
			itemData.emplace_back(newItemData);
		}
		for (auto& itItemsToSend : itemData)
		{
			sentData = SDLNet_TCP_Send(itClients->socket, &itItemsToSend, datasize);
			if (sentData < datasize)
			{
				printf("SDLNet_TCP_Send4: %s\n", SDLNet_GetError());
			}
			else if (networkDebug)
			{
				printf("succeeded sending packet4 nr: \n");
			}
		}*/
	}
}

void NetworkServerHandler::packActorId(NetworkActorData* newNetworkData, Actor * actorToSend)
{
	std::string actorIDname = actorToSend->getActorId().actorName;
	for (std::size_t i = 0; i < actorIDname.size(); i++)
	{
		newNetworkData->actorIdName[i] = actorIDname[i];
	}
	newNetworkData->actorIdNameLength = actorIDname.size();

	newNetworkData->actorIdNumber = actorToSend->getActorId().actorId;
	if (actorToSend->getCategory() != "")
	{
		std::string category = actorToSend->getCategory();
		for (std::size_t i = 0; i < category.size(); i++)
		{
			newNetworkData->category[i] = category[i];
		}
		newNetworkData->categoryLength = category.size();

	}
	else
	{
		newNetworkData->categoryLength = 0;
	}
}
