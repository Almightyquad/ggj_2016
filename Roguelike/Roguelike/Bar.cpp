#include "Bar.h"
#include "Engine.h"
#include "GuiState.h"
#include "Message.h"


Bar::Bar(tinyxml2::XMLElement *xmlElement)
{
	this->originNode = xmlElement;
	name = "";
	color = { 1.f, 1.f, 1.f, 1.f };
	maxValue = 0;
	currentValue = 0;
}

Bar::~Bar()
{
}

void Bar::update()
{
	float tempScale = (currentValue / maxValue);

	float tempPos; 

	//checks if the bar is at 100% or more. If it is the bar shouldnt draw outside its limits
	if (horizontal)
	{
		tempPos = (width * tempScale / 2.f);
		if (tempScale <= 1.0)
		{
			barTransform.setPos(glm::vec3(defaultBarTransform.getPos().x + tempPos - width / 2.f, barTransform.getPos().y, 1.0f));
			barTransform.setScale(glm::vec3(defaultBarTransform.getScale().x * tempScale, barTransform.getScale().y, 1.0f));
		}
		else
		{
			barTransform = defaultBarTransform;
			barTransform.setPos(defaultBarTransform.getPos());
		}
	}
	else
	{
		tempPos = (height * tempScale / 2.f);

		if (tempScale <= 1.0)
		{
			barTransform.setPos(glm::vec3(barTransform.getPos().x , defaultBarTransform.getPos().y + tempPos - height / 2.f, 1.0f));
			barTransform.setScale(glm::vec3(barTransform.getScale().x , defaultBarTransform.getScale().y* tempScale, 1.0f));
		}
		else
		{
			barTransform = defaultBarTransform;
			barTransform.setPos(defaultBarTransform.getPos());
		}
	}
}

void Bar::setBackgroundTexture(std::string texturePath)
{
	backgroundTexture = parentEngine->getTextureHandler()->loadTexture(texturePath);
}

void Bar::setColor(glm::vec4 colorChange)
{
	color = colorChange;
}

void Bar::setBarTexture(std::string texturePath)
{
	barTexture = parentEngine->getTextureHandler()->loadTexture(texturePath);
}


void Bar::setCurrentValue(float size)
{
	currentValue = size;
}

void Bar::setMaxValue(float size)
{
	maxValue = size;
	currentValue = maxValue;
}

void Bar::draw(glm::mat4 &viewProjection)
{
	if (textureOnBar)
	{
		shader->bind();
		shader->loadTransform(barTransform, viewProjection);
		shader->loadInt(U_TEXTURE0, 0);
		shader->loadFloat(U_SCALE, 1.5f);
		barTexture->bind(0);
		mesh->draw();
	}
	else
	{
		shaderBar->bind();
		shaderBar->loadTransform(barTransform, viewProjection);
		shaderBar->loadVec4(U_COLOR, color);
		mesh->draw();
	}
	
	shader->bind();
	shader->loadTransform(backgroundTransform, viewProjection);
	shader->loadInt(U_TEXTURE0, 0);
	shader->loadFloat(U_SCALE, 1.5f);
	backgroundTexture->bind(0);
	mesh->draw();
}

void Bar::init(Engine* parentEngine)
{
	this->parentEngine = parentEngine;
	float tempPosX;
	float tempPosY;
	

	std::string pathTemp;

	if (this->originNode->FirstChildElement("name"))
	{
		name = this->originNode->FirstChildElement("name")->GetText();
	}
	else if (xmlDebug)
	{
		printf("Missing name\n");
	}

	if (this->originNode->FirstChildElement("horizontal"))
	{
		this->originNode->FirstChildElement("horizontal")->QueryBoolText(&horizontal);
	}
	else if (xmlDebug)
	{
		printf("Missing orientation\n");
	}

	if (this->originNode->FirstChildElement("positionX"))
	{
		this->originNode->FirstChildElement("positionX")->QueryFloatText(&tempPosX);
	}
	else if (xmlDebug)
	{
		printf("Missing positionX\n");
	}

	if (this->originNode->FirstChildElement("positionY"))
	{
		this->originNode->FirstChildElement("positionY")->QueryFloatText(&tempPosY);
	}
	else if (xmlDebug)
	{
		printf("Missing positionY\n");
	}

	if (this->originNode->FirstChildElement("widthInPixels"))
	{
		this->originNode->FirstChildElement("widthInPixels")->QueryFloatText(&width);
	}
	else if (xmlDebug)
	{
		printf("Missing widthInPixels\n");
	}

	if (this->originNode->FirstChildElement("heightInPixels"))
	{
		this->originNode->FirstChildElement("heightInPixels")->QueryFloatText(&height);
	}
	else if (xmlDebug)
	{
		printf("Missing heightInPixels\n");
	}

	if (this->originNode->FirstChildElement("meshVerticesPath"))
	{
		pathTemp = this->originNode->FirstChildElement("meshVerticesPath")->GetText();
		this->mesh = parentEngine->getMeshHandler()->loadModel(pathTemp);
	}
	else if (xmlDebug)
	{
		printf("Missing meshVerticesPath\n");
	}

	if (this->originNode->FirstChildElement("texturePath"))
	{
		pathTemp = this->originNode->FirstChildElement("texturePath")->GetText();
		this->backgroundTexture = parentEngine->getTextureHandler()->loadTexture(pathTemp);
	}
	else if (xmlDebug)
	{
		printf("Missing texturePath\n");
	}

	if (this->originNode->FirstChildElement("shaderPath"))
	{
		pathTemp = this->originNode->FirstChildElement("shaderPath")->GetText();
		this->shader = parentEngine->getShaderHandler()->loadShader(pathTemp);
	}
	else if (xmlDebug)
	{
		printf("Missing shaderPath\n");
	}

	if (this->originNode->FirstChildElement("textureBarPath"))
	{
		textureOnBar = true;
		pathTemp = this->originNode->FirstChildElement("textureBarPath")->GetText();
		this->barTexture = parentEngine->getTextureHandler()->loadTexture(pathTemp);
	}
	else if (xmlDebug)
	{
		std::cout << "Missing textureBarPath\n";
	}
	else
	{
		if (this->originNode->FirstChildElement("colorR"))
		{
			this->originNode->FirstChildElement("colorR")->QueryFloatText(&color.r);
		}
		else if (xmlDebug)
		{
			std::cout << "Missing colorR\n";
		}

		if (this->originNode->FirstChildElement("colorG"))
		{
			this->originNode->FirstChildElement("colorG")->QueryFloatText(&color.g);
		}
		else if (xmlDebug)
		{
			std::cout << "Missing colorG\n";
		}

		if (this->originNode->FirstChildElement("colorB"))
		{
			this->originNode->FirstChildElement("colorB")->QueryFloatText(&color.b);
		}
		else if (xmlDebug)
		{
			std::cout << "Missing colorB\n";
		}
	}

	this->shaderBar = parentEngine->getShaderHandler()->loadShader("res/shaders/basicColorShader");

	backgroundTransform.setPos(glm::vec3(tempPosX, tempPosY, 1.0f));
	backgroundTransform.setScale(glm::vec3(width, height, 1.f));

	
	if (horizontal)
	{
		barTransform.setScale(glm::vec3(backgroundTransform.getScale().x * 0.92f, backgroundTransform.getScale().y * 0.85f, 1.0f));
		barTransform.setPos(glm::vec3(backgroundTransform.getPos().x - (backgroundTransform.getScale().x - barTransform.getScale().x) / 2.f + 0.01125f, backgroundTransform.getPos().y, backgroundTransform.getPos().z));
	}
	else
	{
		barTransform.setScale(glm::vec3(backgroundTransform.getScale().x * 0.92f, backgroundTransform.getScale().y, 1.0f));
		barTransform.setPos(glm::vec3(backgroundTransform.getPos().x, backgroundTransform.getPos().y - (backgroundTransform.getScale().y - barTransform.getScale().y) / 2.f + 0.005f, backgroundTransform.getPos().z));
	}
	
	
	defaultBarTransform = barTransform;
 }

std::string Bar::getName()
{
	return name;
}

void Bar::setParent(GuiState& parentPtr)
{
	this->parentGuiState = &parentPtr;
}

GuiState* Bar::getParent()
{
	return parentGuiState;
}
