#include "SavedFrames.h"



SavedFrames::SavedFrames()
{
}


SavedFrames::~SavedFrames()
{

}

void SavedFrames::setPosition(glm::vec3 position)
{
	positionVector.emplace_back(position);
}

void SavedFrames::setRotation(float rotation)
{
	rotationVector.emplace_back(rotation);
}

void SavedFrames::setVelocity(glm::vec2 velocity)
{
	velocityVector.emplace_back(velocity);
}

void SavedFrames::setFrameSpawned(int frameSpawned)
{
	//checks if the object has existed long enough to not be rewinded
	if (frameSpawned <= 0)
	{
		this->frameSpawned = -1;
	} //needs to move the frameSpawned according to the vector
	else
	{
		this->frameSpawned = frameSpawned;
	}
}

void SavedFrames::MoveFrameSpawned()
{
	if (frameSpawned < 0)
	{
		frameSpawned = -1;
	}
	frameSpawned -= 1;
}

void SavedFrames::removeTimeFrame(int timeFrame)
{
	if (timeFrame == 0)
	{
		positionVector.erase(positionVector.begin());
		rotationVector.erase(rotationVector.begin());
		velocityVector.erase(velocityVector.begin());
	}
	else
	{
		positionVector.pop_back();
		rotationVector.pop_back();
		velocityVector.pop_back();
	}
}

glm::vec3 SavedFrames::getPosition(int timeFrame)
{
	return positionVector.back();
}

float SavedFrames::getRotation(int timeFrame)
{
	return rotationVector.back();
}

glm::vec2 SavedFrames::getVelocity(int timeFrame)
{
	return velocityVector.back();
}

int SavedFrames::getFrameSpawned()
{
	return frameSpawned;
}
