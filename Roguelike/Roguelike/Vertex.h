#pragma once
#include <glm\glm.hpp>

class Vertex
{
public:

	/**
	 * @fn	Vertex::Vertex(const glm::vec3& pos, const glm::vec2& texCoord, const glm::vec3& normal = glm::vec3(0, 0, 0))
	 *
	 * @brief	Constructor.
	 *
	 * @param	pos			The position.
	 * @param	texCoord	The texture coordinate.
	 * @param	normal  	The normal.
	 */
	Vertex(const glm::vec3& pos, const glm::vec2& texCoord, const glm::vec3& normal = glm::vec3(0, 0, 0))
	{
		this->pos = pos;
		this->texCoord = texCoord;
	}
	inline glm::vec3* getPos()
	{
		return &pos;
	}
	inline glm::vec2* getTexCoord()
	{
		return &texCoord;
	}

	inline glm::vec3* getNormal()
	{
		return &normal;
	}
protected:
private:
	glm::vec3 pos;
	glm::vec2 texCoord;
	glm::vec3 normal;
};