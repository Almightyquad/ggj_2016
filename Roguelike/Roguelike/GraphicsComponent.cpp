#include "GraphicsComponent.h"
#include "Actor.h"
#include "Engine.h"
#include "Message.h"

GraphicsComponent::GraphicsComponent(tinyxml2::XMLElement *xmlElement) 
	: IGraphicsComponent(xmlElement)
{

}

//GraphicsComponent::GraphicsComponent(const GraphicsComponent& newComponent)
//{
//	*this = newComponent;
//}

GraphicsComponent::~GraphicsComponent()
{

}

void GraphicsComponent::init(Engine* parentEngine)
{
	this->parentEngine = parentEngine;
	float sizeX = 1.f;
	float sizeY = 1.f;
	float sizeZ = 1.f;
	std::string shaderID = "res/shaders/basicShader";


	if (this->originNode->FirstChildElement("category"))
	{
		this->category = this->originNode->FirstChildElement("category")->GetText();
	}
	else if (xmlDebug)
	{
		printf("Missing sizeX\n");
	}

	if (this->originNode->FirstChildElement("sizeX"))
	{
		this->originNode->FirstChildElement("sizeX")->QueryFloatText(&sizeX);
	}
	else if (xmlDebug)
	{
		printf("Missing sizeX\n");
	}

	if (this->originNode->FirstChildElement("sizeY"))
	{
		this->originNode->FirstChildElement("sizeY")->QueryFloatText(&sizeY);
	}
	else if (xmlDebug)
	{
		printf("Missing sizeY\n");
	}

	if (this->originNode->FirstChildElement("sizeZ"))
	{
		this->originNode->FirstChildElement("sizeZ")->QueryFloatText(&sizeZ);
	}
	else if (xmlDebug)
	{
		printf("Missing sizeZ\n");
	}

	if (this->originNode->FirstChildElement("xFrames"))
	{
		this->originNode->FirstChildElement("xFrames")->QueryIntText(&this->xFrames);
	}
	else if (xmlDebug)
	{
		printf("Missing xFrames\n");
	}
	
	if (this->originNode->FirstChildElement("yFrames"))
	{
		this->originNode->FirstChildElement("yFrames")->QueryIntText(&this->yFrames);
	}
	else
	{
		yFrames = xFrames;
		if (xmlDebug)
		{
			printf("Missing yFrames. yFrames set to xFrames\n");
		}
	}

	if (this->originNode->FirstChildElement("animated"))
	{
		this->originNode->FirstChildElement("animated")->QueryBoolText(&this->animated);
	}
	else if (xmlDebug)
	{
		printf("Missing animated(bool)\n");
	}

	if (this->originNode->FirstChildElement("meshVerticesPath"))
	{
		this->meshPath = this->originNode->FirstChildElement("meshVerticesPath")->GetText();
	}
	else if (xmlDebug)
	{
		printf("Missing meshVerticesPath\n");
	}
	if (this->originNode->FirstChildElement("texturePath"))
	{
		this->texturePath = this->originNode->FirstChildElement("texturePath")->GetText();
	}
	else if (xmlDebug)
	{
		printf("Missing texturePath\n");
	}

	if (this->originNode->FirstChildElement("shader"))
	{
		shaderID = this->originNode->FirstChildElement("shader")->GetText();
	}
	else if (xmlDebug)
	{
		printf("Missing shader\n");
	}

	this->mesh = this->parentEngine->getMeshHandler()->loadModel(this->meshPath);
	this->texture = this->parentEngine->getTextureHandler()->loadTexture(this->texturePath);
	this->shader = this->parentEngine->getShaderHandler()->loadShader(shaderID);

	this->transform.getScale().x = sizeX;
	this->transform.getScale().y = sizeY;
	this->transform.getScale().z = sizeZ;
}

void GraphicsComponent::draw(const glm::mat4& viewProjection)
{
	glm::vec3 camPos = parentEngine->getGraphicsHandler()->getCamera()->getPosition();
	int offset = 50;	//blocks to draw away from camera. fix this somewhat flexible
	bool inside = true;

	//AABB to reduce amount of draw calls
	if (transform.getPos().x > camPos.x + offset)
	{
		inside = false;
	}
	else if (transform.getPos().x < camPos.x - offset)
	{
		inside = false;
	}
	else if (transform.getPos().y > camPos.y + offset)
	{
		inside = false;
	}
	else if (transform.getPos().y < camPos.y - offset)
	{
		inside = false;
	}

	if (inside)
	{
		shader->bind();
		shader->loadVec2(U_SIZE, glm::vec2(xFrames, yFrames));

		if (animated)
		{
			shader->loadInt(U_SPRITE_NO, anim.getFrame());
		}
		else
		{
			shader->loadInt(U_SPRITE_NO, (static_cast<int>(tilePosition.x + (tilePosition.y*xFrames))));
		}

		shader->loadMat4(U_TRANSFORM, (viewProjection * modelMatrix));
		texture->bind(0);
		mesh->draw();
	}
}

void GraphicsComponent::update(float deltaTime)
{
	if (!initB)
	{
		this->postMessage(Message(this, static_cast<int>(IDLE)));
		initB = true;
	}
	this->anim.update(deltaTime);
}

void GraphicsComponent::setPosition(glm::vec3 position)
{
	this->transform.setPos(position);
	this->modelMatrix = transform.getModel();
}

void GraphicsComponent::setScale(glm::vec3 scale)
{
	this->transform.setScale(scale);
	this->modelMatrix = transform.getModel();
}

void GraphicsComponent::setTilePosition(glm::vec2 tilePosition)
{
	this->tilePosition = tilePosition;
}

void GraphicsComponent::receiveMessage(const Message & message)
{
	if (message.getSenderType() == typeid(PhysicsComponent))
	{
		b2Body* body = message.getVariable(0);

		if (body->GetLinearVelocity().x < -0.1f)
		{
			this->postMessage(Message(this, static_cast<int>(WALKING)));	//use walking animation
			flipped = true;
		}

		if (body->GetLinearVelocity().x > 0.1f)
		{
			this->postMessage(Message(this, static_cast<int>(WALKING)));	//use walking animation
			flipped = false;
		}

		if (body->GetLinearVelocity().y < -0.1f)
		{
			this->postMessage(Message(this, static_cast<int>(FALLING)));	//use falling animation
		}

		if (body->GetLinearVelocity().y > 0.1f)
		{
			this->postMessage(Message(this, static_cast<int>(JUMPING)));	//use jumping animation
		}


		if (body->GetLinearVelocity().x == 0 && body->GetLinearVelocity().y == 0)
		{
			this->postMessage(Message(this, static_cast<int>(IDLE)));		//use idle animation
		}

		transform.setPos(glm::vec3{ body->GetPosition().x, body->GetPosition().y, transform.getPos().z});	

		
		if (animated && flipped)
		{
			transform.getRot().y = glm::pi<float>();		//flip cube to display the texture the oppsite direction (using the backfacing cube at 180 degrees)
			transform.getRot().z = glm::half_pi<float>();	//flip the remaining 45 degrees to display the correct way
			//transform.getRot().z += body->GetAngle();
		}
		else
		{
			transform.getRot().y = 0.f;	//flip cube back to original direction
			transform.getRot().z = body->GetAngle(); //"hmtnja, sikkert" - Jonas 2016
		}
		
		

		//if (body->GetLinearVelocity().y != 0)
		//{
		//	float newAngle = atan2(body->GetLinearVelocity().y, body->GetLinearVelocity().x);	//rotate projectile to velocity angle
		//	transform.getRot().z = newAngle;
		//}
		modelMatrix = transform.getModel();
	}

	if (parentActor->getComponent("PhysicsComponent") == nullptr && parentActor->getComponent("AnimationComponent") != nullptr)
	{
				//use idle animation
	}

	if (message.getSenderType() == typeid(AnimationComponent))
	{
		int start = message.getVariable(0);		//frame to animating from
		int amount = message.getVariable(1);	//total frames to animate
		float speed = message.getVariable(2);	//frames per second
		anim.config(speed, amount, start);		//configurate animation to use new variables
	}
}

void GraphicsComponent::setTexture(Texture * texture)
{
	this->texture = texture;
}

void GraphicsComponent::setRotation(float angle)
{
	transform.getRot().z = angle;
}

void GraphicsComponent::setFrames(glm::vec2 xy)
{
	this->xFrames = xy.x;
	this->yFrames = xy.y;
}

glm::vec3 GraphicsComponent::getPosition()
{
	return this->transform.getPos();
}

Texture* GraphicsComponent::getTexture()
{
	return texture;
}