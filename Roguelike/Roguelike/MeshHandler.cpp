#include "MeshHandler.h"
#include "Engine.h"
MeshHandler::MeshHandler()
{
}


MeshHandler::~MeshHandler()
{
	meshes.clear();
}

Mesh* MeshHandler::loadModel(const std::string & filePath)
{
	std::unordered_map<std::string, std::unique_ptr<Mesh>>::iterator it = meshes.find(filePath);
	if (it == meshes.end())											//check if mesh already exists in map
	{
		std::cout << "Mesh: " << filePath << std::endl;
#ifdef __ANDROID__
		krem::android::AssetManager * mgr = parentEngine->getAssetMgr();
		this->meshes[filePath] = std::make_unique<Mesh>(filePath, mgr);
#else
		this->meshes[filePath] = std::make_unique<Mesh>(filePath);	//add new mesh to map
#endif
	}
	return meshes[filePath].get();
}