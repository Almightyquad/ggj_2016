#pragma once
#include "IInputComponent.h"
#include "Timer.h"

class Actor;
class ActorFactory;

class PlayerGodInput : public IInputComponent
{
public:
	PlayerGodInput(tinyxml2::XMLElement * xmlElement);
	~PlayerGodInput();
	void update(float deltatime) override;
	void receiveMessage(const Message & message) override;
	void setGroundActor(Actor* actor);
	void swapCharacters();
	void checkAbilityCast();
	void castGoodAbilityOne();
	void castGoodAbilityTwo();
	void castEvilAbilityOne();
	void castEvilAbilityTwo();

private:
	Timer abilityOneTimer;
	Timer abilityTwoTimer;
	Timer freezeInterval;

	glm::vec2 movement;
	bool subscribedToEvents;
	Actor* groundActor;
	bool goodPlayerPlaying;
	//input for good player
	bool wDown;
	bool sDown;
	bool aDown;
	bool dDown;
	bool eDown;
	bool spaceDown;
	bool oneDown;
	bool twoDown;
	bool threeDown;
	bool fourDown;

	//input for evil player
	bool upDown;
	bool downDown;
	bool leftDown;
	bool rightDown;
	bool numZeroDown;
	bool numOneDown;
	bool numSevenDown;
	bool numEightDown;
	bool numNineDown;
	bool numFourDown;
	int maxJoystickValue;
	int freezeCount;
	float speed;

	SDL_JoystickID stickID = -1;
	struct ControllerInput
	{
		bool upButtonDown = false;
		bool downButtonDown = false;
		bool leftBumperDown = false;
		bool rightBumperDown = false;
		krem::Vector2f axis = { 0.f, 0.f };
	};
	std::vector<ControllerInput> inputs;

	ActorFactory* parentActorFactory;
};