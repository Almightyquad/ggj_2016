#include "InputPlayer.h"
#include "Engine.h"
#include "Message.h"
#include "Actor.h"

InputPlayer::InputPlayer(tinyxml2::XMLElement * xmlElement)
	: IInputComponent(xmlElement),
	flipInventory(false)
{
	input.i = false;
}

void InputPlayer::update(float deltatime)
{
	int message = 0;
	if (input.a && !input.d)
	{
		message = -1;
	}
	if (!input.a && input.d)
	{
		message = 1;
	}

	std::string tempName = dynamic_cast<InventoryComponent*>(parentActor->getComponent("InventoryComponent"))->getContainer();
	if (tempName != "")
	{
		if (flipInventory)
		{
			parentEngine->getGuiHandler()->displayContainer(tempName);
			flipInventory = false;
		}
	}

	this->postMessage(Message(this, "keyboardEvent", message, input.space));
	inputChanged = false;
}

void InputPlayer::receiveMessage(const Message & message)
{
	int nothing = 3;
	glm::vec3 temp;
	if (message.getSenderType() == typeid(EventHandler))
	{
		int number = message.getVariable(0);
		inputChanged = true;
		switch (number)
		{
		case SDLK_w:
			input.w = message.getVariable(1);
			break;
		case SDLK_s:
			input.s = message.getVariable(1);
			break;
		case SDLK_a:
			input.a = message.getVariable(1);
			break;
		case SDLK_d:
			input.d = message.getVariable(1);
			break;
		case SDLK_SPACE:
			input.space = message.getVariable(1);
			break;
		case SDLK_i:
			if ((bool)message.getVariable(1) != input.i && !flipInventory)
			{
				flipInventory = true;
			}
			break;
		case SDLK_q:	//timerewinder stuffs
			input.q = message.getVariable(1);
			printf("yo. I'm a timerewinderino\n");
			break;
		default:
			break;
		}
	}
	//if (message.getSenderType() == typeid(NetworkServerHandler))
	//{
	//	if (static_cast<const char*>(message.getVariable(0)) == "remoteInput")
	//	{
	//		inputChanged = true;

	//		wDown = message.getVariable(1);
	//		sDown = message.getVariable(2);
	//		aDown = message.getVariable(3);
	//		dDown = message.getVariable(4);
	//		qDown = message.getVariable(5);
	//		spaceDown = message.getVariable(6);
	//		mouseDown = message.getVariable(7);
	//	}
	//}
}

void InputPlayer::receiveNetworkInput(InputValues newInput)
{
	inputChanged = true;
	input = newInput;
}

InputValues InputPlayer::getInputValues()
{
	return input;
}
