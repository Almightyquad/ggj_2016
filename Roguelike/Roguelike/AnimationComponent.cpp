#include "AnimationComponent.h"
#include "Actor.h"
#include "Message.h"
#include "common.h"
#include "Engine.h"

AnimationComponent::AnimationComponent(tinyxml2::XMLElement * xmlElement)
	: IAnimationComponent(xmlElement)
{

}

AnimationComponent::~AnimationComponent()
{
	animations.clear();
}

void AnimationComponent::init(Engine* parentEngine)
{
	Animation anim;
	if (this->originNode->FirstChildElement("idle"))
	{
		loadFromXML(anim, "idle");
		animations[IDLE] = anim;
	}
	if (this->originNode->FirstChildElement("walking"))
	{
		loadFromXML(anim, "walking");
		animations[WALKING] = anim;
	}
	if (this->originNode->FirstChildElement("death"))
	{
		loadFromXML(anim, "death");
		animations[DEATH] = anim;
	}
	if (this->originNode->FirstChildElement("jumping"))
	{
		loadFromXML(anim, "jumping");
		animations[JUMPING] = anim;
	}
	if (this->originNode->FirstChildElement("falling"))
	{
		loadFromXML(anim, "falling");
		animations[FALLING] = anim;
	}
}

void AnimationComponent::loadFromXML(Animation &anim, char* firstChild)
{
	if (this->originNode->FirstChildElement(firstChild)->FirstChildElement("firstFrame"))
	{
		this->originNode->FirstChildElement(firstChild)->FirstChildElement("firstFrame")->QueryIntText(&anim.firstFrame);
	}
	else if (xmlDebug)
	{
		std::cout << "Missing firstFrame\n";
	}

	if (this->originNode->FirstChildElement(firstChild)->FirstChildElement("amountOfFrames"))
	{
		this->originNode->FirstChildElement(firstChild)->FirstChildElement("amountOfFrames")->QueryIntText(&anim.amountOfFrames);
	}
	else if (xmlDebug)
	{
		std::cout << "Missing amontOfFrames\n";
	}

	if (this->originNode->FirstChildElement(firstChild)->FirstChildElement("animationSpeed"))
	{
		this->originNode->FirstChildElement(firstChild)->FirstChildElement("animationSpeed")->QueryFloatText(&anim.animationSpeed);
	}
	else if (xmlDebug)
	{
		std::cout << "Missing animationSpeed\n";
	}
}

void AnimationComponent::receiveMessage(const Message & message)
{
	if (message.getSenderType() == typeid(GraphicsComponent))
	{
		int state = message.getVariable(0);	//state can be stuff like IDLE, WALKING, JUMPING, etc.
		if (animations.count(state))
		{
			this->postMessage(Message(this, animations[state].firstFrame, animations[state].amountOfFrames, animations[state].animationSpeed));
		}
	}
}

Animation* AnimationComponent::getAnim(int STATE)
{
	auto it = IAnimationComponent::animations.find(STATE);
	if (it != animations.end())
	{
		Animation* obj = &it->second;
		return obj;
	}
}
