#include "GuiState.h"
#include "Engine.h"
#include "GuiHandler.h"


GuiState::GuiState(tinyxml2::XMLElement *xmlElement)
{
	this->originNode = xmlElement;
}

GuiState::~GuiState()
{
}

void GuiState::init(Engine* parentEngine)
{
	this->parentEngine = parentEngine;
	tinyxml2::XMLElement *itemElement;
	std::string temp = "";

	if (originNode->FirstChildElement("GuiItems")->FirstChildElement("Button"))
	{
		
			itemElement = originNode->FirstChildElement("GuiItems")->FirstChildElement("Button");
			temp = originNode->FirstChildElement("GuiItems")->FirstChildElement("Button")->Value();
			while (temp == "Button")
			{
				std::unique_ptr<Button> newButton = std::make_unique<Button>(itemElement);
				addChild(newButton.get());
				newButton->init(this->parentEngine);

				buttons.emplace_back(std::move(newButton));

				itemElement = itemElement->NextSiblingElement();
				if (itemElement != NULL)
				{
					temp = itemElement->Value();
				}
				else
				{
					temp = "end of buttons";
				}
			}
	}
	else if (xmlDebug)
	{
		printf("Missing button\n");
	}

	if (originNode->FirstChildElement("GuiItems")->FirstChildElement("Slider"))
	{
		itemElement = originNode->FirstChildElement("GuiItems")->FirstChildElement("Slider");
		temp = originNode->FirstChildElement("GuiItems")->FirstChildElement("Slider")->Value();
		while (temp == "Slider")
		{
			std::unique_ptr<Slider> newSlider = std::make_unique<Slider>(itemElement);
			addChild(newSlider.get());
			newSlider->init(this->parentEngine);

			sliders.emplace_back(std::move(newSlider));

			itemElement = itemElement->NextSiblingElement();
			if (itemElement != NULL)
			{
				temp = itemElement->Value();
			}
			else
			{
				temp = "end of sliders";
			}
			
		}
	}
	else if (xmlDebug)
	{
		printf("Missing sliders\n");
	}

	if (originNode->FirstChildElement("GuiItems")->FirstChildElement("Panel"))
	{
		itemElement = originNode->FirstChildElement("GuiItems")->FirstChildElement("Panel");
		temp = originNode->FirstChildElement("GuiItems")->FirstChildElement("Panel")->Value();
		while (temp == "Panel")
		{
			std::unique_ptr<Panel> newPanel = std::make_unique<Panel>(itemElement);
			addChild(newPanel.get());
			newPanel->init(this->parentEngine);

			panels.emplace_back(std::move(newPanel));

			itemElement = itemElement->NextSiblingElement();

			if (itemElement != NULL)
			{
				temp = itemElement->Value();
			}
			else
			{
				temp = "end of panels";
			}

		}
	}
	else if (xmlDebug)
	{
		printf("Missing panels\n");
	}

	if (originNode->FirstChildElement("GuiItems")->FirstChildElement("Textbox"))
	{
		itemElement = originNode->FirstChildElement("GuiItems")->FirstChildElement("Textbox");
		temp = originNode->FirstChildElement("GuiItems")->FirstChildElement("Textbox")->Value();
		while (temp == "Textbox")
		{
			std::unique_ptr<TextBox> newTextBox = std::make_unique<TextBox>(itemElement);
			addChild(newTextBox.get());
			newTextBox->init(this->parentEngine);

			textBoxes.emplace_back(std::move(newTextBox));

			itemElement = itemElement->NextSiblingElement();
			if (itemElement != NULL)
			{
				temp = itemElement->Value();
			}
			else
			{
				temp = "end of textboxes";
			}

		}
	}
	else if (xmlDebug)
	{
		printf("Missing textboxes\n");
	}

	if (originNode->FirstChildElement("GuiItems")->FirstChildElement("Checkbox"))
	{
		itemElement = originNode->FirstChildElement("GuiItems")->FirstChildElement("Checkbox");
		temp = originNode->FirstChildElement("GuiItems")->FirstChildElement("Checkbox")->Value();
		while (temp == "Checkbox")
		{
			std::unique_ptr<CheckBox> newCheckBox = std::make_unique<CheckBox>(itemElement);
			addChild(newCheckBox.get());
			newCheckBox->init(this->parentEngine);

			checkBoxes.emplace_back(std::move(newCheckBox));

			itemElement = itemElement->NextSiblingElement();
			if (itemElement != NULL)
			{
				temp = itemElement->Value();
			}
			else
			{
				temp = "end of checkboxes";
			}

		}
	}
	else if (xmlDebug)
	{
		printf("Missing checkboxes\n");
	}

	if (originNode->FirstChildElement("GuiItems")->FirstChildElement("Dropdown"))
	{
		itemElement = originNode->FirstChildElement("GuiItems")->FirstChildElement("Dropdown");
		temp = originNode->FirstChildElement("GuiItems")->FirstChildElement("Dropdown")->Value();
		while (temp == "Dropdown")
		{
			std::unique_ptr<DropDown> newDropDown = std::make_unique<DropDown>(itemElement);
			addChild(newDropDown.get());
			newDropDown->init(this->parentEngine);

			dropDowns.emplace_back(std::move(newDropDown));

			itemElement = itemElement->NextSiblingElement();
			if (itemElement != NULL)
			{
				temp = itemElement->Value();
			}
			else
			{
				temp = "end of dropdowns";
			}

		}
	}
	else if (xmlDebug)
	{
		printf("Missing dropdowns\n");
	}

	if (originNode->FirstChildElement("GuiItems")->FirstChildElement("Container"))
	{
		itemElement = originNode->FirstChildElement("GuiItems")->FirstChildElement("Container");
		temp = originNode->FirstChildElement("GuiItems")->FirstChildElement("Container")->Value();
		while (temp == "Container")
		{
			std::unique_ptr<Container> newContainer = std::make_unique<Container>(itemElement);
			addChild(newContainer.get());
			newContainer->init(this->parentEngine);

			containers.emplace_back(std::move(newContainer));

			itemElement = itemElement->NextSiblingElement();
			if (itemElement != NULL)
			{
				temp = itemElement->Value();
			}
			else
			{
				temp = "end of containers";
			}

		}
	}
	else if (xmlDebug)
	{
		printf("Missing containers\n");
	}

	if (originNode->FirstChildElement("GuiItems")->FirstChildElement("Bar"))
	{
		itemElement = originNode->FirstChildElement("GuiItems")->FirstChildElement("Bar");
		temp = originNode->FirstChildElement("GuiItems")->FirstChildElement("Bar")->Value();
		while (temp == "Bar")
		{
			std::unique_ptr<Bar> newBar = std::make_unique<Bar>(itemElement);
			addChild(newBar.get());
			newBar->init(this->parentEngine);

			bars.emplace_back(std::move(newBar));

			itemElement = itemElement->NextSiblingElement();
			if (itemElement != NULL)
			{
				temp = itemElement->Value();
			}
			else
			{
				temp = "end of Bar";
			}

		}
	}
	else if (xmlDebug)
	{
		printf("Missing Bar\n");
	}
}

//rewrite all functions that looks like this to use let duplicated code
void GuiState::draw(glm::mat4 & viewProjection)
{
	for (auto& it : dropDowns)
	{
		it->draw(viewProjection);
	}
	for (auto& it : bars)
	{
		it->draw(viewProjection);
	}
	for (auto& it : buttons)
	{
		it->draw(viewProjection);
	}
	for (auto& it : sliders)
	{
		it->draw(viewProjection);
	}
	for (auto& it : textBoxes)
	{
		it->draw(viewProjection);
	}
	for (auto& it : checkBoxes)
	{
		it->draw(viewProjection);
	}
	for (auto& it : panels)
	{
		it->draw(viewProjection);
	}
	for (auto& it : containers)
	{
		it->draw(viewProjection);
	}
}

void GuiState::update()
{
	for (auto& it : buttons)
	{
		it->update();
	}
	for (auto& it : bars)
	{
		it->update();
	}
	for (auto& it : sliders)
	{
		it->update();
	}
	for (auto& it : textBoxes)
	{
		it->update();
	}
	for (auto& it : checkBoxes)
	{
		it->update();
	}
	for (auto& it : dropDowns)
	{
		it->update();
	}
	for (auto& it : containers)
	{
		it->update();
	}
}

void GuiState::displayContainer(std::string name)
{
	for (auto& it : containers)
	{
		if (it->getName() == name)
		{
			it->flipVisible();
		}
	}
}

void GuiState::setContainerValue(std::string name, int box)
{
	for (auto& it : containers)
	{
		if (it->getName() == name)
		{
			it->setSelectedBox(box);
		}
	}
}

void GuiState::setTextureContainerBox(std::string name, std::string texturePath)
{
	for (auto& it : containers)
	{
		if (it->getName() == name)
		{
			it->setBoxTexture(texturePath);
		}
	}
}

void GuiState::setTextureContainer(int slot, std::string name, std::string texturePath)
{
	for (auto& it : containers)
	{
		if (it->getName() == name)
		{
			it->setTexture(slot, texturePath);
		}
	}
}

void GuiState::setTexturePanel(std::string name, std::string texturePath)
{
	for (auto& it : panels)
	{
		if (it->getName() == name)
		{
			it->changeTexture(texturePath);
		}
	}
}

void GuiState::setBarBackground(std::string name, std::string texturePath)
{
	for (auto& it : bars)
	{
		if (it->getName() == name)
		{
			it->setBackgroundTexture(texturePath);
		}
	}
}

void GuiState::setBarColor(std::string name, glm::vec4 colorChange)
{
	for (auto& it : bars)
	{
		if (it->getName() == name)
		{
			it->setColor(colorChange);
		}
	}
}

void GuiState::setBarTexture(std::string name, std::string texturePath)
{
	for (auto& it : bars)
	{
		if (it->getName() == name)
		{
			it->setBarTexture(texturePath);
		}
	}
}

void GuiState::setBarValue(std::string name, float size)
{
	for (auto& it : bars)
	{
		if (it->getName() == name)
		{
			it->setCurrentValue(size);
		}
	}
}

void GuiState::setBarMaxValue(std::string name, float size)
{
	for (auto& it : bars)
	{
		if (it->getName() == name)
		{
			it->setMaxValue(size);
		}
	}
}

bool GuiState::checkMouseClickLeft()
{
	for (auto& it : dropDowns)
	{
		if (it->checkMouseClickLeft())
			return true;
	}
	for (auto& it : buttons)
	{
		if (it->checkMouseClickLeft())
			return true;
	}
	for (auto& it : sliders)
	{
		if (it->checkMouseClickLeft())
			return true;
	}
	for (auto& it : checkBoxes)
	{
		if (it->checkMouseClickLeft())
			return true;
	}
	for (auto& it : containers)
	{
		if (it->checkMouseClickLeft())
			return true;
	}

	bool tempFocus = false;

	for (auto& it : textBoxes)
	{
		
		if (it->onFocus())
		{
			tempFocus = true;
		}			
	}
	if (tempFocus)
	{
		return true;
	}
	
	return false;
}

bool GuiState::checkMouseClickRight()
{
	for (auto& it : dropDowns)
	{
		if (it->checkMouseClickRight())
			return true;
	}
	for (auto& it : buttons)
	{
		if (it->checkMouseClickRight())
			return true;
	}
	for (auto& it : sliders)
	{
		if (it->checkMouseClickRight())
			return true;
	}
	for (auto& it : checkBoxes)
	{
		if (it->checkMouseClickRight())
			return true;
	}
	for (auto& it : containers)
	{
		if (it->checkMouseClickRight())
			return true;
	}
	return false;
}

bool GuiState::checkMouseRelease()
{
	for (auto& it : dropDowns)
	{
		if (it->checkMouseRelease())
			return true;
	}
	for (auto& it : buttons)
	{
		if (it->checkMouseRelease())
			return true;
	}
	for (auto& it : sliders)
	{
		if (it->checkMouseRelease())
			return true;
	}
	for (auto& it : checkBoxes)
	{
		if (it->checkMouseRelease())
			return true;
	}
	for (auto& it : containers)
	{
		if (it->checkMouseRelease())
			return true;
	}
	return false;
}

Bar * GuiState::findBar(std::string name)
{
	for (auto& it : bars)
	{
		if (it->getName() == name)
		{
			return it.get();
		}
	}

	return nullptr;
}

Button* GuiState::findButton(std::string name)
{
	
	for (auto& it : buttons)
	{
		if (it->getName() == name)
		{
			return it.get();
		}
	}
	
	return nullptr;
}

Slider* GuiState::findSlider(std::string name)
{
	
	for (auto& it : sliders)
	{
		if (it->getName() == name)
		{
			return it.get();
		}
	}
	
	return nullptr;
}

TextBox * GuiState::findTextBox(std::string name)
{
	
	for (auto& it : textBoxes)
	{
		if (it->getName() == name)
		{
			return it.get();
		}
	}
	
	return nullptr;
}

CheckBox * GuiState::findCheckBox(std::string name)
{
	for (auto& it : checkBoxes)
	{
		if (it->getName() == name)
		{
			return it.get();
		}
	}

	return nullptr;
}

DropDown * GuiState::findDropDown(std::string name)
{
	for (auto& it : dropDowns)
	{
		if (it->getName() == name)
		{
			return it.get();
		}
	}

	return nullptr;
}

Container * GuiState::findContainer(std::string name)
{
	for (auto& it : containers)
	{
		if (it->getName() == name)
		{
			return it.get();
		}
	}

	return nullptr;
}

void GuiState::clear()
{
	for (auto& it : containers)
	{
		it->clear();
	}
}

void GuiState::setParent(GuiHandler& parentPtr)
{
	this->parentGuiHandler = &parentPtr;
}

GuiHandler* GuiState::getParent()
{
	return this->parentGuiHandler;
}

void GuiState::addChild(Bar * bar)
{
	bar->setParent(*this);
}

void GuiState::addChild(Button* button)
{

	button->setParent(*this);
}

void GuiState::addChild(Slider* slider)
{

	slider->setParent(*this);
}

void GuiState::addChild(Panel* panel)
{

	panel->setParent(*this);
}

void GuiState::addChild(TextBox* textBox)
{

	textBox->setParent(*this);
}

void GuiState::addChild(CheckBox * checkBox)
{
	checkBox->setParent(*this);
}

void GuiState::addChild(DropDown * dropDown)
{
	dropDown->setParent(*this);
}

void GuiState::addChild(Container * container)
{
	container->setParent(*this);
}




