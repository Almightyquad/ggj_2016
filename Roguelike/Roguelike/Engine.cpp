#include "Engine.h"
#include "Message.h"
#include "Actor.h"

//doing another test with branching

void Engine::initMainmenu()
{
	hosting = false;
	joining = false;
	audioHandler->loadMusic("res/sounds/maintheme.mp3");
	audioHandler->playMusic("res/sounds/maintheme.mp3");
}

void Engine::updateMainmenu()
{
	while (gameState == STATE_MAINMENU)
	{
		updateMenuGraphics();
		drawMenuGraphics();
		genericUpdate();
		genericDraw();
	}
	return;
}

void Engine::initSettings()
{

}

void Engine::updateSettings()
{
	while (gameState == STATE_SETTINGS)
	{
		updateMenuGraphics();
		drawMenuGraphics();
		genericUpdate();
		genericDraw();
	}
	return;
}

void Engine::initLevelSelect()
{

}

void Engine::updateLevelSelect()
{
	while (gameState == STATE_LEVELSELECT)
	{
		updateMenuGraphics();
		drawMenuGraphics();
		genericUpdate();
		genericDraw();
	}
	return;
}

void Engine::initHosting()
{
#ifdef __ANDROID__
#else
	srand(randomSeed);
	networkServerHandler = std::make_unique<NetworkServerHandler>();
	hosting = true;
	addChild(networkServerHandler.get());
	networkServerHandler->init();
#endif
}

void Engine::updateHosting()
{
#ifdef __ANDROID__
#else
	std::thread listener(&Engine::connectionlistener, this);
	while (gameState == STATE_HOSTING)
	{
		updateMenuGraphics();
		drawMenuGraphics();
		genericUpdate();
		if (netUpdateTimer.hasEnded())
		{
			/*networkServerHandler->receivePacket(gameState);
			networkServerHandler->sendPacket(gameState);*/
			netUpdateTimer.restart();
		}
		genericDraw();
	}
	networkServerHandler->initClients();
//	listener.join();
	//this allows users to continue joining even after the game has "started". not sure how to do this in a good way 
	listener.detach();
	return;
#endif
}

void Engine::initJoining()
{
#ifdef __ANDROID__
#else
	networkClientHandler = std::make_unique<NetworkClientHandler>();
	joining = true;
	addChild(networkClientHandler.get());
	std::cout << "\n*DOES NOTHING ATM*Enter remote IP ( 127.0.0.1  for local connections ) : ";
	//std::cin >> IP;
	std::cout << "\n*DOES NOTHING ATM*Enter remote port : ";
	//std::cin >> remotePort;
	networkClientHandler->init();
#endif
}

void Engine::updateJoining()
{
#ifdef __ANDROID__
#else
	while (gameState == STATE_JOINING)
	{
		updateMenuGraphics();
		drawMenuGraphics();
		genericUpdate();
		if (netUpdateTimer.hasEnded())
		{
			networkClientHandler->sendPacket(gameState);
			networkClientHandler->receivePacket(gameState);
			netUpdateTimer.restart();
		}
		genericDraw();
	}
#endif
}



void Engine::initRunning()
{

	//TEMP: DELETE
	Actor* preGenerator;
	preGenerator = actorFactory->createActor("res/actors/freeze.xml", glm::vec3(0.f, 0.f, 0.f));
	actorFactory->addToDeleteQueue(preGenerator);
	preGenerator = actorFactory->createActor("res/actors/meteor.xml", glm::vec3(0.f, 0.f, 0.f));
	actorFactory->addToDeleteQueue(preGenerator);
	preGenerator = actorFactory->createActor("res/actors/spikes.xml", glm::vec3(0.f, 0.f, 0.f));
	actorFactory->addToDeleteQueue(preGenerator);
	preGenerator = actorFactory->createActor("res/actors/halo.xml", glm::vec3(0.f, 0.f, 0.f));
	actorFactory->addToDeleteQueue(preGenerator);
	preGenerator = actorFactory->createActor("res/actors/shield.xml", glm::vec3(0.f, 0.f, 0.f));
	actorFactory->addToDeleteQueue(preGenerator);
	preGenerator = actorFactory->createActor("res/actors/flameBlast.xml", glm::vec3(0.f, 0.f, 0.f));
	actorFactory->addToDeleteQueue(preGenerator);
	
	textureHandler->loadTexture("res/textures/characters/GroundPlayer/PwsE_01.png");
	textureHandler->loadTexture("res/textures/characters/GroundPlayer/PisG_01.png");
	textureHandler->loadTexture("res/textures/characters/GroundPlayer/PwsG_01.png");
	textureHandler->loadTexture("res/textures/characters/GroundPlayer/PisE_01.png");
	textureHandler->loadTexture("res/textures/characters/GroundPlayer/PGsG_01.png");
	textureHandler->loadTexture("res/textures/characters/GroundPlayer/PssG_01.png");
	textureHandler->loadTexture("res/textures/characters/GroundPlayer/PssE_01.png");
	textureHandler->loadTexture("res/textures/characters/GroundPlayer/frozenGood.png");
	textureHandler->loadTexture("res/textures/characters/GroundPlayer/frozenBad.png");
	textureHandler->loadTexture("res/textures/characters/GroundPlayer/PssE_01.png");


	
	textureHandler->loadTexture("res/gui/textures/barBackgroundBad.png");
	textureHandler->loadTexture("res/gui/textures/goodGodPortrait.png");
	textureHandler->loadTexture("res/gui/textures/spellFrameGood.png");
	textureHandler->loadTexture("res/gui/textures/spellFrameBad.png");
	textureHandler->loadTexture("res/gui/textures/DurationBackgroundGood.png");

	textureHandler->loadTexture("res/gui/textures/heal.png");
	textureHandler->loadTexture("res/gui/textures/freeze.png");
	textureHandler->loadTexture("res/gui/textures/gloryThrow.png");
	textureHandler->loadTexture("res/gui/textures/shield.png");
	textureHandler->loadTexture("res/gui/textures/meteor.png");
	textureHandler->loadTexture("res/gui/textures/groundSpike.png");
	textureHandler->loadTexture("res/gui/textures/flameBlast.png");
	textureHandler->loadTexture("res/gui/textures/selfDestruct.png");
	
	levelLoaded = true;
	eventHandler->setInputStatus(false);
	printf("seed: is %i\n", randomSeed);
	srand(randomSeed);

	audioHandler->loadMusic("res/sounds/gameMusic.wav");
	audioHandler->loadSoundEffect("res/sounds/pop.wav");
	audioHandler->loadSoundEffect("res/sounds/erase.wav");
	//audioHandler->playMusic("res/sounds/gameMusic.wav");
	levelLoader->setActorFactory(actorFactory.get());
	levelLoader->setPhysicsHandler(physicsHandler.get());
	//levelLoader->loadLevel(selectedLevel);
	//IInputComponent* playerInput;
	//InventoryComponent* playerInventory;
	if (!joining) //== if singleplayer or hosting
	{
		setPlayer(*actorFactory->createActor(playerString, glm::vec3(2.f, 0.f, 0.f))); //game needs atleast one player
#ifndef __ANDROID__
		if (hosting)
		{
			//dnetworkServerHandler->sendPacket(gameState);
		}
#endif
	}
#ifdef __ANDROID__
#else
	if (joining)
	{
		actorID playerID = networkClientHandler->getPlayerActorID();
		networkClientHandler->receivePacket(STATE_JOINING); //TODO: fix this when rewriting network
		setPlayer(*actorFactory->getActor(playerID));
	}
#endif
	//playerInput = dynamic_cast<IInputComponent*>(player->getComponent("InputComponent"));
	//playerInventory = dynamic_cast<InventoryComponent*>(player->getComponent("InventoryComponent"));

	//guiHandler->setBarMaxValue("healthBar", static_cast<float>(dynamic_cast<CombatComponent*>(player->getComponent("CombatComponent"))->getHealth()));
	//guiHandler->setBarMaxValue("manaBar", static_cast<float>(dynamic_cast<CombatComponent*>(player->getComponent("CombatComponent"))->getMana()));
	//SUBSCRIBE PLAYER TO EVENTHANDLER FOR DIRECT INPUTHANDLING
	//eventHandler->addSubscriber(playerInput);
	//guiHandler->getContainer("inventory")->addSubscriber(playerInventory);
	if (physicsDebug)
	{
		physicsHandler->dump();
	}

	chunkHandler.setActorFactory(actorFactory.get());
	chunkHandler.setPhysicsHandler(physicsHandler.get());
	std::vector<std::string> prefabVec;
	int numberOfPrefabs = 1;
	for (int i = 0; i < numberOfPrefabs; i++)
	{
		prefabVec.emplace_back("res/chunk/prefab/fab" + std::to_string(i + 1) + ".txt");
	}
	chunkHandler.loadPrefabList(prefabVec);
	for (int i = 0; i < 5; i++)
	{
		chunkHandler.loadChunks();
	}

	enemyVec.emplace_back("minotaur");
}

void Engine::updateRunning()
{
	while (gameState == STATE_RUNNING)
	{
		if (enemiesKilled >= killsNeededToWin)
		{
			std::cout << "You have killed " << enemiesKilled << " enemies." << std::endl;
			std::cout << "You had to kill " << killsNeededToWin << " enemies." << std::endl;
		}
		genericUpdate();
#ifdef __ANDROID__
#else
		if (netUpdateTimer.hasEnded())
		{
			if (hosting)
			{
				networkServerHandler->sendPacket(gameState);
			}
			if (joining)
			{
				networkClientHandler->sendPacket(gameState);
			}
			netUpdateTimer.restart();
		}
		if (hosting)
		{
			networkServerHandler->receivePacket(gameState);
		}
		if (joining)
		{
			networkClientHandler->receivePacket(gameState);
		}		
#endif
		glm::mat4 viewProjection = graphicsHandler->getCamera()->getViewProjection();
		this->physicsHandler->update(this->deltaTime);
		this->actorFactory->updateAll(this->deltaTime);	
		for (size_t i = 0; i < skyboxHandlers.size(); i++)
		{
			float speed = dynamic_cast<PhysicsComponent*>(this->player->getComponent("PhysicsComponent"))->getVelocity().x * 0.1f;
			this->skyboxHandlers[i]->update(viewProjection, this->deltaTime, (speed * static_cast<float>(i) * deltaTime));
		}
		weatherParticles->update(this->deltaTime);
		particleSystem->update(this->deltaTime);

		//this->timeRewinder->update(); //doesn't work
		glm::vec3 camera = player->getPosition();
		//TODO do this smarter/more flexible
		/*if (camera.x < 11.6f) camera.x = 11.6f;
		if (camera.x > 81.4f) camera.x = 81.4f;
		if (camera.y < -8.15f) camera.y = -8.15f;*/

		glm::vec2 mousePos = eventHandler->getMousePos();
		/*float moveSpeed = 5.f * this->deltaTime;
		if (mousePos.x <= 0)
		{
			player->movePosition(glm::vec3(-moveSpeed, 0.f, 0.f));
		}
		if (mousePos.y <= 0)
		{
			player->movePosition(glm::vec3(0.f, moveSpeed, 0.f));
		}
		if (mousePos.x >= (graphicsHandler->getWindowResolution().x - 1.f))
		{
			player->movePosition(glm::vec3(moveSpeed, 0.f, 0.f));
		}
		if (mousePos.y >= (graphicsHandler->getWindowResolution().y - 1.f))
		{
			player->movePosition(glm::vec3(0.0f, -moveSpeed, 0.f));
		}*/
		//graphicsHandler->getCamera()->setPosition(camera);
		graphicsHandler->setCameraPosition(glm::vec3{ camera.x, -3.15f, graphicsHandler->getViewDistance() });
		for (size_t i = 0; i < skyboxHandlers.size(); i++)
		{
			this->skyboxHandlers[i]->draw(viewProjection);	//updates and draws
		}
		
		this->actorFactory->draw();

#ifdef __ANDROID__
#else
		//this->lightHandler->update(viewProjection);
#endif
		this->weatherParticles->draw(viewProjection);
		this->particleSystem->draw(viewProjection);
		if (!joining)
		{
		}
		
		enemySpawnTimer += deltaTime;
		glm::vec2 tempPos = { 0,0 };
		while (tempPos.x < 5 && tempPos.x > -5)
		{
			tempPos.x = player->getPosition().x + (-10 + (rand() % 20));
		}
		tempPos.y = player->getPosition().y + 5;
		if (enemySpawnTimer > enemySpawnRate)
		{
			randomSpawn->spawnRandomEnemy(enemyVec[rand() % static_cast<int>(enemyVec.size())], tempPos);
			enemySpawnTimer = 0;
		}
		std::vector<std::vector<Actor*>> tempActors = chunkHandler.getChunkActors();
		//Theres something strange going on here.
		if (tempActors[tempActors.size() - 2].back()->getPosition().x < player->getPosition().x)
		{
			chunkHandler.loadChunks();
			for (auto it : tempActors.front())
			{
				actorFactory->addToDeleteQueue(it);
			}
			chunkHandler.deleteChunkActors();
		}

		////// test light
		/*Transform transform;
		transform.setPos(glm::vec3(5, -5, 0));
		transform.setScale(glm::vec3(5, 5, 1));
		Mesh *mesh = getMeshHandler()->loadModel("res/models/quad.obj");
		Shader *shader = getShaderHandler()->loadShader("res/shaders/basicShader");
		shader->loadTransform(transform, viewProjection);
		mesh->draw();*/
		genericDraw();
		//if (dynamic_cast<CombatComponent*>(player->getComponent("CombatComponent"))->getHealth() <= 0)
		//{
		//	setGameState(STATE_GAMEOVER);
		//}
	}
	//eventHandler->removeSubscriber(dynamic_cast<IInputComponent*>(player->getComponent("InputComponent")));
	guiHandler->clear(STATE_RUNNING);
//	eventHandler->removeSubscriber(dynamic_cast<InputComponent*>(player->getComponent("InputComponent")));
	actorFactory->clearFactory();
	physicsHandler->clear();
	ChunkHandler temp;
	chunkHandler = temp;
	
	return;
}

void Engine::initGameover()
{
	
}

void Engine::updateGameover()
{
	while (gameState == STATE_GAMEOVER)
	{
		updateMenuGraphics();
		drawMenuGraphics();
		genericUpdate();
		genericDraw();
	}
	return;
}

void Engine::initQuitting()
{
	physicsHandler->clear();
}

void Engine::updateQuitting()
{
//stuffsibuffsi
}



Engine::Engine()
	:
	deltaTime(0),
	previousTime(0),
	netUpdateTimer(1.f / 30.f), //sets network to update every 1.f/x.f second (or x times a second). NOTE: this feels unprecise. should get rid of sdl timers anyways 1
	oneSecondTimer(1),
	joining(false),
	hosting(false),
	enemiesKilled(0),
	killsNeededToWin(20),
	playerString("res/actors/groundPlayer.xml")
{
	randomSeed = static_cast<unsigned int>(time(NULL));
	srand(randomSeed);
	running = true;
	
	actorFactory = std::make_unique<ActorFactory>();
	graphicsHandler = std::make_unique<GraphicsHandler>("Good god bad god");
	physicsHandler = std::make_unique<PhysicsHandler>();
	audioHandler = std::make_unique<AudioHandler>();
	textureHandler = std::make_unique<TextureHandler>();
	meshHandler = std::make_unique<MeshHandler>();
	guiHandler = std::make_unique<GuiHandler>();
	shaderHandler = std::make_unique<ShaderHandler>();
	eventHandler = std::make_unique<EventHandler>();
	//timeRewinder = std::make_unique<TimeRewinder>();
	fontHandler = std::make_unique<FontHandler>();
	terrainHandler = std::make_unique<TerrainHandler>();
	randomSpawn = std::make_unique<RandomSpawn>();
	for (size_t i = 0; i < 4; i++)
	{
		skyboxHandlers.emplace_back(std::make_unique<SkyboxHandler>());
	}
	
	xmlHandler = std::make_unique<XmlHandler>();

	levelLoader = std::make_unique<LevelLoader>();
#ifdef __ANDROID__
	sdlAndroidActivity = std::make_unique<krem::app::SdlAndroidActivity>();
	sdlAndroidActivity->initialize();
	javaEnv = sdlAndroidActivity->getJavaEnvironment();
	javaAssetManager = sdlAndroidActivity->getJavaAssetManager();
	assetMgr = std::make_unique<krem::android::AssetManager>(javaEnv, javaAssetManager);
#else
	//lightHandler = std::make_unique<LightHandler>();
#endif
	weatherParticles = std::make_unique<WeatherParticles>();
	particleSystem = std::make_unique<ParticleSystem>();

	enemySpawnTimer = 0;
	enemySpawnRate = 10;

	gameState = STATE_MAINMENU;
}

Engine::~Engine()
{
}

void Engine::init()
{
	addChild(fontHandler.get());
	addChild(shaderHandler.get());
	addChild(meshHandler.get());
	addChild(actorFactory.get());
	addChild(guiHandler.get());
	addChild(eventHandler.get());
	//addChild(timeRewinder.get());
	addChild(terrainHandler.get());
	addChild(randomSpawn.get());
	for (size_t i = 0; i < skyboxHandlers.size(); i++)
	{
		addChild(skyboxHandlers[i].get());
	}
	
	addChild(textureHandler.get());
	addChild(xmlHandler.get());
#ifdef __ANDROID__
#else
	//addChild(lightHandler.get());
#endif
	eventHandler->init();
	guiHandler->loadStatesFromFiles();
	terrainHandler->init();
	terrainHandler->generateTerrain(-0.5f, -0.5f);
	for (size_t i = 0; i < skyboxHandlers.size(); i++)
	{
	skyboxHandlers[i]->init();
	skyboxHandlers[i]->loadTexture("res/textures/parallax" + std::to_string(i) + "/", SKYBOX_DAY);
	skyboxHandlers[i]->loadTexture("res/textures/parallax" + std::to_string(i) + "/", SKYBOX_NIGHT);
	//skyboxHandlers[i]->loadTexture("res/textures/skybox/", SKYBOX_DAY);
	//skyboxHandlers[i]->loadTexture("res/textures/nightbox/", SKYBOX_NIGHT);
	}


	weatherParticles->init(this, 0.12f, this->textureHandler->loadTexture("res/textures/snowflake.png"), 0.35f, true);
	weatherParticles->createPoint(0, glm::vec3(0.f, 0.f, 0.f), 1);
	particleSystem->init(this, 0.02f, this->textureHandler->loadTexture("res/textures/snowball.png"), 0.50f, true);

	//lightHandler->init();
	//lightHandler->addLight(glm::vec3(0.f, 0.f, 0.f), 15.f);

	//timeRewinder->init();
	this->deltaTime = 0.f;

	previousTime = SDL_GetTicks();

	addListeners();
}
void Engine::run()
{
	gameLoop();
}

void Engine::gameLoop()
{
	SDL_StartTextInput();
	while (gameState != STATE_QUITTING)
	{
		switch (gameState)
		{
		case STATE_MAINMENU:
			initMainmenu();
			updateMainmenu();
			break;
		case STATE_LEVELSELECT:
			initLevelSelect();
			updateLevelSelect();
			break;
		case STATE_HOSTING:
			initHosting();
			updateHosting();
			break;
		case STATE_JOINING:
			initJoining();
			updateJoining();
			break;
		case STATE_RUNNING:
			initRunning();
			if (levelLoaded)
			{
				updateRunning();
			}
			break;
		case STATE_GAMEOVER:
			initGameover();
			updateGameover();
			break;
		case STATE_SETTINGS:
			initSettings();
			updateSettings();
			break;
		default:
			break;
		}
	}
	//one function should be enough
	initQuitting();
	//updateQuitting();
	
	physicsHandler.reset(nullptr);
	SDL_StopTextInput();
}


void Engine::genericUpdate()
{
	
	eventHandler->handleEvents();
	deltaTime = (SDL_GetTicks() - previousTime) / 1000.f; //1000 = ms per second
	previousTime = SDL_GetTicks();
	guiHandler->update();
	
	if (debug)
	{
		if (oneSecondTimer.hasEnded())
		{
			printf("FPS: %i\n", fps);
			fps = 0;
			oneSecondTimer.restart();
		}
		else
		{
			fps++;
		}
	}
}

void Engine::genericDraw()
{
	guiHandler->draw(gameState);
	graphicsHandler->swapBuffers();
	graphicsHandler->clearWindow();
}

ActorFactory* Engine::getActorFactory() const
{
	return actorFactory.get();
}

PhysicsHandler* Engine::getPhysicsHandler() const
{
	return physicsHandler.get();
}

AudioHandler* Engine::getAudioHandler() const
{
	return audioHandler.get();
}

GraphicsHandler* Engine::getGraphicsHandler() const
{
	return graphicsHandler.get();
}

TextureHandler* Engine::getTextureHandler() const
{
	return textureHandler.get();
}

MeshHandler* Engine::getMeshHandler() const
{
	return meshHandler.get();
}

GuiHandler* Engine::getGuiHandler() const
{
	return guiHandler.get();
}

ShaderHandler* Engine::getShaderHandler() const
{
	return shaderHandler.get();
}

EventHandler* Engine::getEventHandler() const
{
	return eventHandler.get();
}

TimeRewinder* Engine::getTimeKeeper() const
{
	return timeRewinder.get();
}

FontHandler * Engine::getFontHandler() const
{
	return fontHandler.get();
}
TerrainHandler * Engine::getTerrainHandler() const
{
	return terrainHandler.get();
}
SkyboxHandler * Engine::getSkyboxHandler(const int NUMBER) const
{
	return skyboxHandlers[NUMBER].get();
}
ParticleSystem * Engine::getParticleSystem() const
{
	return particleSystem.get();
}
#ifdef __ANDROID__
#else
NetworkClientHandler * Engine::getClientHandler() const
{
	return networkClientHandler.get();
}

NetworkServerHandler * Engine::getServerHandler() const
{
	return networkServerHandler.get();
}
#endif


LevelLoader * Engine::getLevelLoader() const
{
	return levelLoader.get();
}

XmlHandler * Engine::getXmlHandler() const
{
	return xmlHandler.get();
}

RandomSpawn * Engine::getRandomSpawn() const
{
	return randomSpawn.get();
}

#ifdef __ANDROID__
krem::android::AssetManager * Engine::getAssetMgr() const
{
	return assetMgr.get();
}
#endif
template<typename T>
void Engine::addChild(T t)
{
	t->setParent(*this);
}

void Engine::setPlayer(Actor& newplayer)
{
	Actor* tempactor = actorFactory->getActor(newplayer.getActorId());
	this->player = tempactor;
	//this->player = &newplayer;
}

Actor * Engine::getPlayer() const
{
	return this->player;
}

int Engine::getSelectedTool() const
{
	return selectedTool;
}

int Engine::getSelectedLevel() const
{
	return selectedLevel;
}

void Engine::setGameState(int gameState)
{
	this->gameState = gameState;
}

int Engine::getGameState()
{
	return gameState;
}

void Engine::setSeed(unsigned int seed)
{
	randomSeed = seed;
	srand(randomSeed);
}

unsigned int Engine::getSeed()
{
	return randomSeed;
}

float Engine::getDeltaTime()
{
	return deltaTime;
}

void Engine::connectionlistener()
{
#ifdef __ANDROID__
#else
	//while(STATE_HOSTING)
	while (1) //fix this when states are chikorita
	{
		networkServerHandler->listen();
	}
#endif
}
//TODO: Decouple this and have all the listeners be in a logical place instead of just smashing them in the engine
void Engine::addListeners()
{
	//main menu screen
	if (guiHandler->getButton("mainsingleplayer") != nullptr)
	{
		guiHandler->getButton("mainsingleplayer")->addSubscriber(this);
	}
	if (guiHandler->getButton("mainsettings") != nullptr)
	{
		guiHandler->getButton("mainsettings")->addSubscriber(this);
	}
	if (guiHandler->getButton("mainexitgame") != nullptr)
	{
		guiHandler->getButton("mainexitgame")->addSubscriber(this);
	}

	//setting screen
	if (guiHandler->getButton("settingback") != nullptr)
	{
		guiHandler->getButton("settingback")->addSubscriber(this);
	}
	if (guiHandler->getSlider("settingmastervolume") != nullptr)
	{
		guiHandler->getSlider("settingmastervolume")->addSubscriber(this);
	}
	if (guiHandler->getSlider("settingmusic") != nullptr)
	{
		guiHandler->getSlider("settingmusic")->addSubscriber(this);
	}
	if (guiHandler->getSlider("settingsoundeffects") != nullptr)
	{
		guiHandler->getSlider("settingsoundeffects")->addSubscriber(this);
	}
	
	if (guiHandler->getCheckBox("settingvsync") != nullptr)
	{
		guiHandler->getCheckBox("settingvsync")->addSubscriber(this);
	}

	if (guiHandler->getDropDown("settingresolution") != nullptr)
	{
		guiHandler->getDropDown("settingresolution")->addSubscriber(this);
	}
	if (guiHandler->getDropDown("settingdisplay") != nullptr)
	{
		guiHandler->getDropDown("settingdisplay")->addSubscriber(this);
	}

	//level select
	if (guiHandler->getButton("selectBack") != nullptr)
	{
		guiHandler->getButton("selectBack")->addSubscriber(this);
	}
	if (guiHandler->getButton("selectPlay") != nullptr)
	{
		guiHandler->getButton("selectPlay")->addSubscriber(this);
	}
	if (guiHandler->getContainer("selectLevel") != nullptr)
	{
		guiHandler->getContainer("selectLevel")->addSubscriber(this);
	}

	//running screen
	if (guiHandler->getButton("runningpause") != nullptr)
	{
		guiHandler->getButton("runningpause")->addSubscriber(this);
	}

	if (guiHandler->getContainer("actionBarHuman") != nullptr)
	{
		guiHandler->getContainer("actionBarHuman")->addSubscriber(this);
	}

	//game over screen
	if (guiHandler->getButton("gameoverplayagain") != nullptr)
	{
		guiHandler->getButton("gameoverplayagain")->addSubscriber(this);
	}
	if (guiHandler->getButton("gameovermainmenu") != nullptr)
	{
		guiHandler->getButton("gameovermainmenu")->addSubscriber(this);
	}

	getEventHandler()->addSubscriber(this);
}

//TODO: same as function above. Remove the shit out of this so it is in a better place
// All of these are messaged received from different gui elements 
void Engine::receiveMessage(const Message & message)
{
	if (gameState == STATE_MAINMENU || gameState == STATE_GAMEOVER)
	{
		if (static_cast<const char*>(message.getVariable(0)) == "keyUp")
		{
			int number = message.getVariable(1);
			switch (number)
			{
			case SDLK_SPACE:
				gameState = STATE_RUNNING;
				break;
			case SDLK_ESCAPE:
				gameState = STATE_QUITTING;
				break;
			}
		}
	

	if (static_cast<const char*>(message.getVariable(0)) == "joystickButtonDown")
	{
		SDL_JoyButtonEvent buttonDown = message.getVariable(1);
		switch (buttonDown.button)
		{
		case 0: //dpad up

			break;
		case 1: //dpad left

			break;
		case 2: //dpad down

			break;
		case 3: //dpad right

			break;
		case 4: //start
			gameState = STATE_RUNNING;
			break;
		case 5: //back or select
			gameState = STATE_QUITTING;
			break;
		case 6: //left tumbstick pressed

			break;
		case 7: //right tumbstick pressed

			break;
		case 8: //left bumper or l1

			break;
		case 9: //right bumper or r1

			break;
		case 10: //a or x
		
			break;
		case 11: //b or circle

			break;
		case 12: //x or square

			break;
		case 13: //y or triangle
		
			break;
		}
	}
}


	if (message.getSenderType() == typeid(Button))
	{
		const char * tempMessage = message.getVariable(0);
		if (strcmp(tempMessage, "mainsingleplayer") == 0)
		{
			gameState = STATE_RUNNING;
		}
		if (strcmp(tempMessage, "mainsettings") == 0)
		{
			gameState = STATE_SETTINGS;
		}
		if (strcmp(tempMessage, "mainexitgame") == 0)
		{
			gameState = STATE_QUITTING;
		}
		if (strcmp(tempMessage, "settingback") == 0)
		{
			gameState = STATE_MAINMENU;
		}
		if (strcmp(tempMessage, "runningpause") == 0)
		{
			gameState = STATE_MAINMENU;
		}
		if (strcmp(tempMessage, "gameoverplayagain") == 0)
		{
			gameState = STATE_RUNNING;
		}
		if (strcmp(tempMessage, "gameovermainmenu") == 0)
		{
			gameState = STATE_MAINMENU;
		}
		if (strcmp(tempMessage, "selectPlay") == 0)
		{
			gameState = STATE_RUNNING;
		}
		if (strcmp(tempMessage, "selectBack") == 0)
		{
			gameState = STATE_MAINMENU;
			guiHandler->setPanelTexture("asdfa", "2341234");
		}
	}
	if (message.getSenderType() == typeid(CheckBox))
	{
		const char * tempMessage = message.getVariable(0);
		bool tempStatus = message.getVariable(1);
		if (strcmp(tempMessage, "settingvsync") == 0)
		{
			if (tempStatus)
			{
				SDL_GL_SetSwapInterval(1);
			}
			else
			{
				SDL_GL_SetSwapInterval(0);
			}
		}
	}
	if (message.getSenderType() == typeid(DropDown))
	{
		const char * tempMessage = message.getVariable(0);
		const char * temp = message.getVariable(1);
		if (strcmp(tempMessage, "settingresolution") == 0)
		{
			
			glm::vec2 resolution = graphicsHandler->getWindowResolution();
			//glViewport(0, 0, 640, 360);
		}
		if (strcmp(tempMessage, "settingdisplay") == 0)
		{
			if (strcmp(temp, "Fullscreen") == 0)
			{
				//graphicsHandler->updateWindow(SDL_WINDOW_FULLSCREEN);
			}
			else if (strcmp(temp, "Windowed") == 0)
			{
				graphicsHandler->updateWindow(0);
			}
			else if (strcmp(temp, "Borderless") == 0)
			{
				graphicsHandler->updateWindow(SDL_WINDOW_BORDERLESS);
			}
		}
	}
	if (message.getSenderType() == typeid(Slider))
	{
		const char * tempMessage = message.getVariable(0);
		int temp = message.getVariable(1);
		if (strcmp(tempMessage, "settingmastervolume") == 0)
		{
			getAudioHandler()->setMasterVolume(temp);
		}
		if (strcmp(tempMessage, "settingmusic") == 0)
		{
			getAudioHandler()->setMusicVolume(temp);
		}
		if (strcmp(tempMessage, "settingsoundeffects") == 0)
		{
			getAudioHandler()->setSoundVolume(temp);
		}

	}
	if (message.getSenderType() == typeid(Container))
	{
		const char * tempMessage = message.getVariable(0);

		if (strcmp(tempMessage, "selectLevel") == 0)
		{
			tempMessage = message.getVariable(1);
			if (strcmp(tempMessage, "boxSelected") == 0)
			{
				selectedLevel = message.getVariable(2);
			}
		}

		if (strcmp(tempMessage, "actionBarHuman") == 0)
		{
			tempMessage = message.getVariable(1);
			if (strcmp(tempMessage, "boxSelected") == 0)
			{
				selectedTool = message.getVariable(2);
			}
		}
	}
}

void Engine::increaseEnemiesKilled()
{
	enemiesKilled++;
}

bool Engine::getJoining()
{
	return joining;
}

void Engine::drawMenuGraphics()
{
	glm::mat4 viewProjection = graphicsHandler->getCamera()->getViewProjection();
	for (size_t i = 0; i < skyboxHandlers.size(); i++)
	{
		this->skyboxHandlers[i]->draw(viewProjection);
	}
	
	this->terrainHandler->draw(viewProjection);
	this->weatherParticles->draw(viewProjection);
}

void Engine::updateMenuGraphics()
{
	glm::mat4 viewProjection = graphicsHandler->getCamera()->getViewProjection();
	for (size_t i = 0; i < skyboxHandlers.size(); i++)
	{
		this->skyboxHandlers[i]->update(viewProjection, this->deltaTime, 0.002f * static_cast<float>(i));
	}
	
	this->terrainHandler->update();
	this->weatherParticles->update(this->deltaTime);
}


