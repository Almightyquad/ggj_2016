#pragma once
#include <string>
#include <memory>
#include <map>
#include <vector>
#include "Transform.h"
#include "Mesh.h"
#include "Texture.h"
#include "Shader.h"
#include "tinyxml2-master\tinyxml2.h"
#include <glm\glm.hpp>
#include "Font.h"
#include "Observable.h"

class Engine;
class GuiState;

/** A button. Used to create buttons that returns true or false if they are clicked or not */
class Button : public IObservable
{
public:
	Button(tinyxml2::XMLElement *xmlElement);
	~Button();

	/** Updates this object. */
	void update();

	/**
	 * Determines if mouse click left is inside the button.
	 *
	 * @return	true if it succeeds, false if it fails.
	 */
	bool checkMouseClickLeft();

	/**
	 * Determines if mouse click right is inside the button.
	 *
	 * @return	true if it succeeds, false if it fails.
	 */
	bool checkMouseClickRight();

	/**
	 * Determines if mouse release is inside the button.
	 *
	 * @return	true if it succeeds, false if it fails.
	 */
	bool checkMouseRelease();

	/**
	 * Draws the given view projection.
	 *
	 * @param [in,out]	viewProjection	The view projection.
	 */
	void draw(glm::mat4 &viewProjection);

	/**
	 * Initialises this object.
	 *
	 * @param [in,out]	parentEngine	If non-null, the parent engine.
	 */
	void init(Engine* parentEngine);

	/**
	 * Sets a parent.
	 *
	 * @param [in,out]	parentPtr	The parent pointer.
	 */
	void setParent(GuiState& parentPtr);

	/**
	 * Gets the parent of this item.
	 *
	 * @return	null if it fails, else the parent.
	 */
	GuiState* getParent();

	/**
	 * Gets the name.
	 *
	 * @return	The name.
	 */
	std::string getName();
private:
	GuiState* parentGuiState;
	Engine* parentEngine;

	std::string name;
	std::string text;

	bool clicked;
	bool fontDisplay;

	int currentTexture;
	int textureAtlasSize;

	tinyxml2::XMLElement *originNode;

	Transform transform;
	Font * font;
	Mesh *mesh;
	Shader *shader;
	Texture *texture;
};

