#pragma once
#include "ParticleSystem.h"

class WeatherParticles : public ParticleSystem
{
public:
	WeatherParticles()
		: ParticleSystem() {};

	/**
	 * @fn	void WeatherParticles::update(float deltaTime) override;
	 *
	 * @brief	Updates with the given deltaTime.
	 *
	 * @param	deltaTime	The delta time.
	 */
	void update(float deltaTime) override;
};