#include "AIFactory.h"

std::unique_ptr<IAIComponent> AIFactory::create(const std::string & itemName, tinyxml2::XMLElement * xmlElement)
{
	auto it = table.find(itemName);
	if (it != table.end())
	{
		return it->second->create(xmlElement);
	}
	else
	{
		return nullptr;
	}
}

void AIFactory::registerIt(const std::string & itemName, AICreator* aICreator)
{
	table[itemName] = aICreator;
}